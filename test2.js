var bla = [
    {
        name: "HN-Atmungsaktivität",
        tag_id: 101,
        attribute_id: 194,
        options: []
    },
    {
        name: "HN-Waschkennzeichen",
        tag_id: 1,
        attribute_id: 157,
        options: [
          {
            option_id: 1,
            name: "HN-Handwäsche",
            attribute_value_id: 3720,
            attribute_id: 157
          },
          {
            option_id: 2,
            name: "HN-30°C Maschinenwäsche, Schongang",
            attribute_value_id: 3721,
            attribute_id: 157
          },
        ]
    }
]

var tag_element = bla[1];
var attribute_value = getAttributeValueByOptionId(tag_element, 2);
console.log(attribute_value);


function getAttributeValueByOptionId(tag_element, option_id) {
    if(tag_element.options.length) {
        for (const element of tag_element.options) {
            if(element.option_id == option_id) {
                return element;
            }
        }
    }
    return false;
}