const express = require('express');
const app = express();
const request = require('request');
const amqp = require('amqplib/callback_api');
const readline = require('readline');
const { WebClient } = require('@slack/web-api');
var bodyParser = require('body-parser');
const send = require('send');
app.use(express.json());
app.use(bodyParser.json());
require('dotenv').config();
let token;
let username = process.env.PLENTY_USERNAME;
let password = process.env.PLENTY_PASSWORD;
const data = JSON.stringify({
    username: username,
    password: password
});

function getToken() {
    var header = {
        'Content-Type': 'application/json',
        'Content-Length': data.length
    }
    var options = {
        url: process.env.PLENTY_URL + "/rest/login",
        port: 443,
        path: '/rest/login',
        method: 'POST',
        body: data,
        headers: header
    }
    return httpRequest(options, true, 'POST');
}

(async () => {
    try {
        console.log("getting token...");
        var a = await getToken();
        var out = JSON.parse(a);
        if ("error" in out) {
            throw new Error("Error in getToken");
        }
        console.log("done");

        token = out["access_token"];
        console.log(token);
        var header = {
            'Content-Type': 'application/json; charset=utf8',
            'Authorization': 'Bearer ' + token
        }
        var options = {
            url: process.env.PLENTY_URL + "/rest/items/3967/variations/1069/stock",
            port: 443,
            method: 'GET',
            headers: header
        } 
        var out = await httpRequest(options, true, 'GET');
        //console.log(out);
        out = JSON.parse(out);
        for (let index = 0; index < out.length; index++) {
            const element = out[index];
            console.log(element.physicalStock);  
        }

    } catch (e) {
        console.error(e);
    } finally {
        //console.log('We do cleanup here');
    }
})();


async function httpRequest(params, postData, type = 'GET') {
    try {
        return new Promise(function (resolve, reject) {
            if (type === 'POST') {
                request.post(params, (err, res, body) => {
                    if (err) {
                        var msg = "Error in POST httpRequest " + err;
                        thechannel.sendToQueue(queue, Buffer.from(msg));
                        return reject(new Error('statusCode=' + res));
                    }
                    if (body) {
                        resolve(body);
                    }
                });
            }
            else if (type === 'PUT') {
                //console.log("in put");
                request.put(params, (err, res, body) => {
                    if (err) {
                        var msg = "Error in PUT httpRequest " + err;
                        thechannel.sendToQueue(queue, Buffer.from(msg));
                        return console.log(err);
                    }
                    if (body) {
                        //console.log(res);
                        //console.log(body);
                        resolve(body);
                        return body;
                        /* console.log("BODY: ");
                        console.log(body); */
                    }
                    
                });
            }
            else if (type === 'DELETE') {
                //console.log("in put");
                request.delete(params, (err, res, body) => {
                    if (err) {
                        var msg = "Error in DELETE httpRequest " + err;
                        thechannel.sendToQueue(queue, Buffer.from(msg));
                        return console.log(err);
                    }
                    if (body) {
                        
                        resolve(body);
                        return body;
                        
                    }
                    
                });
            }
            else if (type === 'GET') {
                request.get(params, (err, res, body) => {
                    if (err) {
                        var msg = "Error in GET httpRequest " + err;
                        //thechannel.sendToQueue(queue, Buffer.from(msg));
                        console.log(err);
                        return reject(new Error(err + ' statusCode=' + res));
                    }
                    if (body) {
                        resolve(body);
                    }
                });
            }
        });
    }
    catch (e) {
        var msg = "Error in httpRequest " + e;
        thechannel.sendToQueue(queue, Buffer.from(msg));
        console.error(e);
    } finally {
        //console.log('We do cleanup here');
    }
}