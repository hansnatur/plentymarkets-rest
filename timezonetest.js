process.env.TZ = 'Europe/Berlin';

function getGermanTimestamp() {
    // Aktuelles Datum und Uhrzeitobjekt erstellen
    var jetzt = new Date();

    // Datum formatieren (TT.MM.JJJJ)
    var tag = jetzt.getDate();
    var monat = jetzt.getMonth() + 1; // Monate sind nullbasiert, daher +1
    var jahr = jetzt.getFullYear();

    // Uhrzeit formatieren (HH:MM:SS)
    var stunden = jetzt.getHours();
    var minuten = jetzt.getMinutes();
    var sekunden = jetzt.getSeconds();

    // Führende Nullen hinzufügen, wenn nötig
    tag = (tag < 10) ? '0' + tag : tag;
    monat = (monat < 10) ? '0' + monat : monat;
    stunden = (stunden < 10) ? '0' + stunden : stunden;
    minuten = (minuten < 10) ? '0' + minuten : minuten;
    sekunden = (sekunden < 10) ? '0' + sekunden : sekunden;

    // Das Datum und die Uhrzeit im deutschen Format zusammenstellen
    return tag + '.' + monat + '.' + jahr + ' ' + stunden + ':' + minuten + ':' + sekunden;
}

console.log("time: " + getGermanTimestamp());