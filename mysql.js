const mysql = require('mysql2');
const {query} = require("express");
const console = require("node:console");
require('dotenv').config();

// Create a connection to the MySQL database
/* const connection = mysql.createConnection({
  host: 'mysql', // This should match the service name defined in your docker-compose.yml file
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PW,
  database: process.env.MYSQL_DB
}); */


// Create a connection pool
const pool = mysql.createPool({
    host: 'mysql',
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PW,
    database: process.env.MYSQL_DB,
    waitForConnections: true,
    connectionLimit: 10,
    queueLimit: 0
});

// Function to perform a query
function queryDatabase(query, params) {
    return new Promise((resolve, reject) => {
        pool.query(query, params, (error, results) => {
            if (error) {
                return reject(error);
            }
            resolve(results);
        });
    });
}

// Example usage
queryDatabase('SHOW TABLES')
    .then(results => {
        console.log(results);
    })
    .catch(error => {
        console.error('Error executing query:', error);
    });


async function startMysqlConnection() {
    // Connect to the database
    connection.connect((err) => {
    if (err) {
      console.error('Error connecting to MySQL database:', err);
      return;
    }
    console.log('Connected to MySQL database');
  });
}

function endMysqlConnection() {
    // Close the connection
    connection.end();
}


/* // // Perform a sample query
connection.query('SHOW TABLES', (error, results, fields) => {
  if (error) throw error;
  console.log(results);
}); */

async function insertIntoCategoryMapping(item) {
    const data = {
        plenty_category_id: item.plenty_category_id,
        sage_id: item.sage_id,
        name: item.name,
        sage_parent_id: item.sage_parent_id,
        plenty_parent_id: item.plenty_parent_id
    };

    const query = 'INSERT INTO category_mapping (plenty_category_id, sage_id, name, sage_parent_id, plenty_parent_id) VALUES (?, ?, ?, ?, ?)';

    try {
        const [results] = await pool.promise().query(query, [
            data.plenty_category_id,
            data.sage_id,
            data.name,
            data.sage_parent_id,
            data.plenty_parent_id
        ]);
        console.log('Data inserted successfully to DB', results);
    } catch (error) {
        console.error('Error inserting at insertIntoCategoryMapping data:', error);
    }
}

/* async function insertIntoCategoryMapping(item) {
    const data = {
        plenty_category_id: item.plenty_category_id,
        sage_id: item.sage_id,
        name: item.name,
        sage_parent_id: item.sage_parent_id,
        plenty_parent_id: item.plenty_parent_id
      };
    // Convert data to JSON string
    const jsonData = JSON.stringify(data);
    const query = 'INSERT INTO category_mapping (plenty_category_id, sage_id, name, sage_parent_id, plenty_parent_id) VALUES (?, ?, ?, ?, ?)';
    connection.query(query, [data.plenty_category_id, data.sage_id, data.name, data.sage_parent_id, data.plenty_parent_id], (error, results, fields) => {
        if (error) {
          console.error('Error inserting data:', error);
          return;
        }
        console.log('Data inserted successfully to DB');
      });
      
} */

async function insertIntoTagMapping(item) {
    // Convert data to JSON string
    const query = 'INSERT INTO tag_mapping (sage_tag_id, plenty_tag_id, name) VALUES (?, ?, ?)';
    
    try {
        const [results] = await pool.promise().query(query, [
            item[0].sage_tag_id,
            item[0].plenty_tag_id,
            item[0].name
        ]);
        console.log('Data inserted successfully to DB', results);
    } catch (error) {
        console.error('Error inserting at insertIntoCategoryMapping data:', error);
    }

    /* connection.query(query, [item[0].sage_tag_id, item[0].plenty_tag_id, item[0].name], (error, results, fields) => {
        if (error) {
            console.error('Error inserting data:', error);
            return;
        }
        console.log('Data inserted successfully to DB');
    }); */

}

async function insertIntoTagValueMapping(item) {
    // Convert data to JSON string
    const query = 'INSERT INTO tag_value_mapping (sage_option_id, plenty_selection_id, sage_group_id, plenty_tag_id, name) VALUES (?, ?, ?, ?, ?)';


    try {
        const [results] = await pool.promise().query(query, [
            item[0].sage_option_id,
            item[0].plenty_selection_id,
            item[0].sage_group_id,
            item[0].plenty_tag_id,
            item[0].name 
        ]);
        console.log('Data inserted successfully to DB', results);
    } catch (error) {
        console.error('Error inserting at insertIntoCategoryMapping data:', error);
    }

   /*  connection.query(query, [item[0].sage_option_id, item[0].plenty_selection_id, item[0].sage_group_id, item[0].plenty_tag_id, item[0].name ], (error, results, fields) => {
        if (error) {
            console.error('Error inserting data:', error);
            return;
        }
        console.log('Data inserted successfully to DB');
    }); */

}

async function updateTagMapping(item){
    const query = 'UPDATE tag_mapping SET name = ?, last_updated = CURRENT_TIMESTAMP() WHERE plenty_tag_id = ?';

    try {
        const [results] = await pool.promise().query(query, [
            item[0].name,
            item[0].plenty_tag_id
        ]);
        console.log('Data inserted successfully to DB', results);
    } catch (error) {
        console.error('Error inserting at insertIntoCategoryMapping data:', error);
    }

    /* connection.query(query, [item[0].name, item[0].plenty_tag_id], (error, results, fields) => {
        if (error) {
            console.error('Error inserting data:', error);
            return;
        }
        console.log('Tag Group successfully updated in Tag DB');
    }); */
}
async function updateTagValueMapping(item){
    const query = 'UPDATE tag_value_mapping SET name = ?, last_updated = CURRENT_TIMESTAMP() WHERE plenty_selection_id = ?';

    try {
        const [results] = await pool.promise().query(query, [
            item[0].name, item[0].plenty_selection_id
        ]);
        console.log('Data inserted successfully to DB', results);
    } catch (error) {
        console.error('Error inserting at insertIntoCategoryMapping data:', error);
    }

    /* connection.query(query, [item[0].name, item[0].plenty_selection_id], (error, results, fields) => {
        if (error) {
            console.error('Error inserting data:', error);
            return;
        }
        console.log('Tag successfully updated in Tag DB');
    }); */
}

async function insertIntoImageMapping(obj, table = "image_mapping") {
    const data = {
        sageImageId: obj.sage_image_id,
        plentyImageId: obj.plenty_image_id,
        sageSku: obj.sage_sku
    }
    const jsonData = JSON.stringify(data);
    const query = 'INSERT INTO '+table+' (sage_image_id, plenty_image_id, sku) VALUES (?, ?, ?)';
    try {
        const [results] = await pool.promise().query(query, [
            data.sageImageId, data.plentyImageId, data.sageSku
        ]);
        console.log('Data inserted successfully to DB', results);
    } catch (error) {
        console.error('Error inserting at insertIntoCategoryMapping data:', error);
    }

    /* connection.query(query, [data.sageImageId, data.plentyImageId, data.sageSku], (error, results, fields) => {
        if (error) {
            console.error('Error inserting data:', error);
            return;
        }
    }); */
}

async function updatePriceOfItem(sku, price = null, is_special = null, special_price = null, table) { 
  const data = {
    sku: sku,
    price, price,
    is_special: is_special,
    special_price: special_price
  }
  const query = 'UPDATE '+table+' SET price = ?, is_specialprice = ?, special_price = ? WHERE sku = ?';
  try {
        const [results] = await pool.promise().query(query, [
            data.price, data.is_special, data.special_price, data.sku
        ]);
        console.log('Data inserted successfully to DB', results);
    } catch (error) {
        console.error('Error inserting at insertIntoCategoryMapping data:', error);
    }
  /* connection.query(query, [data.price, data.is_special, data.special_price, data.sku], (error, results, fields) => {
    if (error) {
      console.error('Error updating data:', error);
      return;
    }
    console.log('Data updated successfully');
  }); */
}



async function updateItemMappingItem(item, table = "item_mapping") {
  console.log("in update function");
  console.log(`updating in table ${table}`);
  console.log(item);
  const data = {
      plenty_id: item.id,
      plenty_item_id: item.itemId,
      name: item.name,
      sage_id: item.sage_id,
      father_id: item.father_id,
      father_sku: item.father_sku,
      sku: item.sku,
      categories: item.categories,
      variation_attributes: item.variation_attributes,
    };
  // Convert categories and variation_attributes to JSON strings
  const categoriesJson = JSON.stringify(data.categories);
  const variationAttributesJson = JSON.stringify(data.variation_attributes);
  // Convert data to JSON string
  const jsonData = JSON.stringify(data);
  const query = `UPDATE ${table} SET plenty_id = ?, plenty_item_id = ?, name = ?, sage_id = ?, father_id = ?, father_sku = ?, sku = ?, categories = ?, variation_attributes = ? WHERE plenty_id = ?`;
  console.log("query: " + query);
  try {
        const [results] = await pool.promise().query(query, [
        data.plenty_id, data.plenty_item_id, data.name, data.sage_id, data.father_id, data.father_sku, data.sku, categoriesJson, variationAttributesJson, data.plenty_id
    ]);
    console.log(query);
    console.log('Data inserted successfully to DB', results);
    } catch (error) {
        console.error('Error inserting at insertIntoCategoryMapping data:', error);
    }
}

async function updateImageMapping(obj){
    const data = {
        sageImageId: obj.sage_image_id,
        plentyImageId: obj.plenty_image_id
    }
    const jsonData = JSON.stringify(data);
    //change query to update image_mappings plenty_image_id value
    const query = 'UPDATE image_mapping SET plenty_image_id = ? WHERE sage_image_id = ?';
    try {
        const [results] = await pool.promise().query(query, [
        data.plentyImageId, data.sageImageId
    ]);
    console.log('Data inserted successfully to DB', results);
    } catch (error) {
        console.error('Error inserting at insertIntoCategoryMapping data:', error);
    }
    /* connection.query(query, [data.plentyImageId, data.sageImageId], (error, results, fields) => {
        if (error) {
            console.error('Error inserting data:', error);
            return;
        }
        console.log('Data inserted successfully to Image DB');
    }); */
}



async function logFail(text) {
    console.log("in logFail function");
    const data = {
      log: text
    };
    const query = 'INSERT INTO fail_log (log) VALUES (?)';
    try {
        const [results] = await pool.promise().query(query, [
        data.log
    ]);
    console.log('Data inserted successfully to DB', results);
    } catch (error) {
        console.error('Error inserting at insertIntoCategoryMapping data:', error);
    }
    /* connection.query(query, [data.log], (error, results, fields) => {
        if (error) {
          console.error('Error inserting data:', error);
          return;
        }
        console.log('Data inserted successfully');
      }); */
}

async function insertIntoDisabledAmazon(item) {
    console.log("in insert disabled function");
    const data = {
        sku: item.Sku,
        sage_id: item.Id,
        father_id: item.FatherId,
        father_sku: item.FatherSku,
      };
    const query = 'INSERT INTO disabled_items_amazon (sku, sage_id, father_id, father_sku) VALUES (?, ?, ?, ?)';
    try {
      const [results] = await pool.promise().query(query, [
          data.sku, data.sage_id, data.father_id, data.father_sku
      ]);
      console.log('Data inserted successfully to DB', results);
      } catch (error) {
          console.error('Error inserting at insertIntoCategoryMapping data:', error);
      }
    /* connection.query(query, [data.sku, data.sage_id, data.father_id, data.father_sku], (error, results, fields) => {
        if (error) {
          console.error('Error inserting data:', error);
          return;
        }
        console.log('Data inserted successfully');
      }); */
  }

async function insertIntoDisabled(item) {
  console.log("in insert disabled function");
  const data = {
      sku: item.Sku,
      sage_id: item.Id,
      father_id: item.FatherId,
      father_sku: item.FatherSku,
    };
  const query = 'INSERT INTO disabled_items (sku, sage_id, father_id, father_sku) VALUES (?, ?, ?, ?)';
  try {
    const [results] = await pool.promise().query(query, [
        data.sku, data.sage_id, data.father_id, data.father_sku
    ]);
    console.log('Data inserted successfully to DB', results);
    } catch (error) {
        console.error('Error inserting at insertIntoCategoryMapping data:', error);
    }
  /* connection.query(query, [data.sku, data.sage_id, data.father_id, data.father_sku], (error, results, fields) => {
      if (error) {
        console.error('Error inserting data:', error);
        return;
      }
      console.log('Data inserted successfully');
    }); */
}


async function insertIntoItemMappingInactive(item) {
    console.log("in insert function for inactive");
    const data = {
        plenty_id: item.id,
        plenty_item_id: item.itemId,
        plenty_mainVariationId: item.plenty_mainVariationId,
        is_main: item.is_main,
        name: item.name,
        sage_id: item.sage_id,
        father_id: item.father_id,
        father_sku: item.father_sku,
        sku: item.sku,
        categories: item.categories,
        variation_attributes: item.variation_attributes,
      };
    // Convert categories and variation_attributes to JSON strings
    const categoriesJson = JSON.stringify(data.categories);
    const variationAttributesJson = JSON.stringify(data.variation_attributes);
    // Convert data to JSON string
    const jsonData = JSON.stringify(data);
    const query = 'INSERT INTO item_mapping_orphan (plenty_id, plenty_item_id, plenty_mainVariationId, is_main, name, sage_id, father_id, father_sku, sku, categories, variation_attributes) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
    try {
        const [results] = await pool.promise().query(query, [
            data.plenty_id, data.plenty_item_id, data.plenty_mainVariationId, data.is_main, data.name, data.sage_id, data.father_id, data.father_sku, data.sku, categoriesJson, variationAttributesJson
        ]);
        console.log('Data inserted successfully to DB', results);
        } catch (error) {
            console.error('Error inserting at insertIntoItemMappingAmazon data:', error);
        }
    /* connection.query(query, [data.plenty_id, data.plenty_item_id, data.name, data.sage_id, data.father_id, data.father_sku, data.sku, categoriesJson, variationAttributesJson], (error, results, fields) => {
        if (error) {
          console.error('Error inserting data:', error);
          return;
        }
        console.log('Data inserted successfully');
      }); */
}


async function insertIntoItemMappingAmazon(item) {
    console.log("in insert function for amazon");
    const data = {
        plenty_id: item.id,
        plenty_item_id: item.itemId,
        plenty_mainVariationId: item.plenty_mainVariationId,
        name: item.name,
        sage_id: item.sage_id,
        father_id: item.father_id,
        father_sku: item.father_sku,
        sku: item.sku,
        categories: item.categories,
        variation_attributes: item.variation_attributes,
      };
    // Convert categories and variation_attributes to JSON strings
    const categoriesJson = JSON.stringify(data.categories);
    const variationAttributesJson = JSON.stringify(data.variation_attributes);
    // Convert data to JSON string
    const jsonData = JSON.stringify(data);
    const query = 'INSERT INTO item_mapping_amazon (plenty_id, plenty_item_id, plenty_mainVariationId, name, sage_id, father_id, father_sku, sku, categories, variation_attributes) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
    try {
        const [results] = await pool.promise().query(query, [
            data.plenty_id, data.plenty_item_id, data.plenty_mainVariationId, data.name, data.sage_id, data.father_id, data.father_sku, data.sku, categoriesJson, variationAttributesJson
        ]);
        console.log('Data inserted successfully to DB', results);
        } catch (error) {
            console.error('Error inserting at insertIntoItemMappingAmazon data:', error);
        }
    /* connection.query(query, [data.plenty_id, data.plenty_item_id, data.name, data.sage_id, data.father_id, data.father_sku, data.sku, categoriesJson, variationAttributesJson], (error, results, fields) => {
        if (error) {
          console.error('Error inserting data:', error);
          return;
        }
        console.log('Data inserted successfully');
      }); */
}

async function insertIntoItemMapping(item, table = "item_mapping") {
    console.log("in insert function table = " +table);
    const data = {
        plenty_id: item.id,
        plenty_item_id: item.itemId,
        name: item.name,
        sage_id: item.sage_id,
        father_id: item.father_id,
        father_sku: item.father_sku,
        sku: item.sku,
        categories: item.categories,
        variation_attributes: item.variation_attributes,
      };
    // Convert categories and variation_attributes to JSON strings
    const categoriesJson = JSON.stringify(data.categories);
    const variationAttributesJson = JSON.stringify(data.variation_attributes);
    // Convert data to JSON string
    const jsonData = JSON.stringify(data);
    const query = 'INSERT INTO '+table+' (plenty_id, plenty_item_id, name, sage_id, father_id, father_sku, sku, categories, variation_attributes) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)';
    try {
        const [results] = await pool.promise().query(query, [
            data.plenty_id, data.plenty_item_id, data.name, data.sage_id, data.father_id, data.father_sku, data.sku, categoriesJson, variationAttributesJson
        ]);
        console.log('Data inserted successfully to DB', results);
        } catch (error) {
            console.error('Error inserting at insertIntoCategoryMapping data:', error);
        }
    /* connection.query(query, [data.plenty_id, data.plenty_item_id, data.name, data.sage_id, data.father_id, data.father_sku, data.sku, categoriesJson, variationAttributesJson], (error, results, fields) => {
        if (error) {
          console.error('Error inserting data:', error);
          return;
        }
        console.log('Data inserted successfully');
      }); */
}

async function insertIntoVariationnameAttributesMapping(item) {
  console.log("inserting into variationname attributes mapping...");
    const data = {
        name: item.name,
        attributevalue_id: item.id,
    };
    // Convert data to JSON string
    const jsonData = JSON.stringify(data);
    const query = 'INSERT INTO variationname_attributes (attributevalue_id, name) VALUES (?, ?)';
    try {
        const [results] = await pool.promise().query(query, [
            data.attributevalue_id, data.name
        ]);
        console.log('Data inserted successfully to DB', results);
        } catch (error) {
            console.error('Error inserting at insertIntoCategoryMapping data:', error);
        }
    /* connection.query(query, [data.attributevalue_id, data.name], (error, results, fields) => {
        if (error) {
          console.error('Error inserting data:', error);
          return;
        }
        console.log('Data inserted successfully');
      }); */
}

async function insertIntoAttributesMapping(item, table) {
    console.log("inserting into variationname attributes mapping...");
      const data = {
          name: item.name,
          attributevalue_id: item.id,
      };
      // Convert data to JSON string
      const jsonData = JSON.stringify(data);
      const query = 'INSERT INTO '+table+' (attributevalue_id, name) VALUES (?, ?)';
      try {
          const [results] = await pool.promise().query(query, [
              data.attributevalue_id, data.name
          ]);
          console.log('Data inserted successfully to DB', results);
          } catch (error) {
              console.error('Error inserting at insertIntoCategoryMapping data:', error);
          }
      /* connection.query(query, [data.attributevalue_id, data.name], (error, results, fields) => {
          if (error) {
            console.error('Error inserting data:', error);
            return;
          }
          console.log('Data inserted successfully');
        }); */
  }


async function getTable(table) {
  return new Promise(async (resolve, reject) => {
      const query = 'SELECT * FROM ??';
      try {
        const [results] = await pool.promise().query(query, [
            table
        ]);
        resolve(results);
        //console.log('Data inserted successfully to DB', results);
        } catch (error) {
            console.error('Error inserting at insertIntoCategoryMapping data:', error);
            reject(error);
        }
      /* connection.query(query, [table], (error, results, fields) => {
          if (error) {
              console.error('Error querying database:', error);
              reject(error);
          } else {
              resolve(results);
          }
      }); */
  });
}

async function updateTimestamp(table, key, column) {
    const sql = 'UPDATE ?? SET last_updated = CURRENT_TIMESTAMP() WHERE ?? = ?';
  
    try {
      const [results] = await pool.promise().query(sql, [table, column, key]);
      return results;
      // console.log('Data updated successfully in DB', results);
    } catch (error) {
      console.error('Error updating timestamp in DB:', error);
      throw error;
    }
  }



async function insertIntoTagMapping(property_group_id, sage_id, name) {
  const data = {
      property_group_id: property_group_id,
      sage_id: sage_id,
      name: name
    };
  const query = 'INSERT INTO property_group_mapping (property_group_id, sage_id, name) VALUES (?, ?, ?)';
  try {
    const [results] = pool.promise().query(query, [
        data.property_group_id, data.sage_id, data.name
    ]);
    return results;
    //console.log('Data inserted successfully to DB', results);
    } catch (error) {
        console.error('Error inserting at insertIntoCategoryMapping data:', error);
        
    }
  /* connection.query(query, [data.property_group_id, data.sage_id, data.name], (error, results, fields) => {
      if (error) {
        console.error('Error inserting data:', error);
        return;
      }
      console.log('Data inserted successfully');
    }); */
}

async function insertIntoTags(tag_id, name, color) {
  
}

async function getSpecificItems() {
  console.log("in getSpecificItems");
  return new Promise((resolve, reject) => {
    const query = "SELECT * FROM `item_mapping` WHERE `last_updated` < '2024-05-27 00:00:00' ORDER BY `item_mapping`.`plenty_item_id`";
    /* connection.query(query, (error, results, fields) => {
        if (error) {
            console.error('Error querying database:', error);
            reject(error);
        } else {
            resolve(results);
        }
    }); */
});
}

async function getItemInDB(table, search, column) {

    const query = 'SELECT * FROM ?? WHERE ?? = ?';

    try {
        const [results] = await pool.promise().query(query, [
            table,
            column,
            search
        ]);
        console.log('getItemInDB successfull');
        return results;
    } catch (error) {
        console.error('Error querying database:', error);
    }


    /* return new Promise((resolve, reject) => {
        const query = 'SELECT * FROM ?? WHERE ?? = ?';
        connection.query(query, [table, column, search], (error, results, fields) => {
            if (error) {
                console.error('Error querying database:', error);
                reject(error);
            } else {
                resolve(results);
            }
        });
    }); */
}
async function getAllItemsInDB(table){

    const query = 'SELECT * FROM ??';
    try {
        const [results] = await pool.promise().query(query, [
            table
        ]);
        console.log('getItemInDB successfull');
        return results;
    } catch (error) {
        console.error('Error querying database:', error);
    }

    /* connection.query(query, [table], (error, results, fields) => {
        if (error) {
            console.error('Error querying database:', error);
            reject(error);
        } else {
            resolve(results);
        }
        }); */

}

async function getImageInDB(table, columnOne, columnTwo, imageId, sku) {
    return new Promise(async (resolve, reject) => {
        const query = 'SELECT * FROM ?? WHERE ?? = ? AND ?? = ?';
        try {
            const [results] = await pool.promise().query(query, [
                table, columnOne, imageId, columnTwo, sku
            ]);
            console.log('getItemInDB successfull');
            resolve(results);
        } catch (error) {
            reject(error);
            console.error('Error querying database:', error);
        }
    
        /* connection.query(query, [table, columnOne, imageId, columnTwo, sku], (error, results, fields) => {
            if (error) {
                console.error('Error querying database:', error);
                reject(error);
            } else {
                resolve(results);
            }
        }); */
    });
}
async function deleteImageFromDB(table, columnOne, columnTwo, imageId, sku){
    const query = 'DELETE FROM ?? WHERE ?? = ? AND ?? = ?';
    try {
        const [results] = await pool.promise().query(query, [
            table, columnOne, imageId, columnTwo, sku
        ]);
        console.log('getItemInDB successfull');
        return results;
    } catch (error) {
        console.error('Error querying database:', error);
    }
    /* connection.query(query, [table, columnOne, imageId, columnTwo, sku], (error, results, fields) => {
        if (error) {
            console.error('Error deleting data:', error);
            return;
        }
    }); */
}



async function emptyTable(table){
    const query = 'DELETE FROM ??';
    try {
        const [results] = await pool.promise().query(query, [
            table
        ]);
        console.log('getItemInDB successfull');
        return results;
    } catch (error) {
        console.error('Error querying database:', error);
    }
    /* connection.query(query, [table], (error, results, fields) => {
        if (error) {
            console.error('Error deleting data:', error);
            return;
        }
    }); */
}

async function deleteItemFromDB(table, search, column){
    const query = 'DELETE FROM ?? WHERE ?? = ?';
    try {
        const [results] = await pool.promise().query(query, [
            table, column, search
        ]);
        console.log('getItemInDB successfull');
        return results;
    } catch (error) {
        console.error('Error querying database:', error);
    }
    /* connection.query(query, [table, column, search], (error, results, fields) => {
        if (error) {
            console.error('Error deleting data:', error);
            return;
        }
    }); */
}

module.exports = { insertIntoItemMapping,updateTagMapping, getAllItemsInDB, updateTagValueMapping, insertIntoTagValueMapping, insertIntoTagMapping, insertIntoCategoryMapping, emptyTable, insertIntoImageMapping, updateImageMapping, deleteItemFromDB, startMysqlConnection, endMysqlConnection,
  insertIntoVariationnameAttributesMapping, getItemInDB, getImageInDB, deleteImageFromDB, logFail, 
  getTable, insertIntoDisabled, insertIntoDisabledAmazon, updateTimestamp, updateItemMappingItem, getSpecificItems, updatePriceOfItem, insertIntoItemMappingAmazon, insertIntoItemMappingInactive, insertIntoAttributesMapping };

