const amqp = require('amqplib/callback_api');
const mysql = require('./mysql');
const express = require('express');
const axios = require('axios');
const { htmlToText } = require('html-to-text');
const fs = require('fs').promises;
const path = require('path');
const app = express();
const request = require('request');
const { WebClient } = require('@slack/web-api');
var bodyParser = require('body-parser');
const e = require('express');
const console = require("node:console");

const categoriesInPlenty = require("mysql/lib/protocol/BufferList");
const { options } = require("axios");
const tagClassesInSage = require("mysql/lib/protocol/BufferList");
const { table } = require('console');
//const { isSet } = require('util/types');
app.use(express.json());
app.use(bodyParser.json());
require('dotenv').config();
const queueName = process.env.RABBITMQ_QUEUE;
const server = process.env.AMQP_URL;
let username = process.env.PLENTY_USERNAME;
let password = process.env.PLENTY_PASSWORD;
const sms_username = process.env.SMS_LIVE_USERNAME;
const sms_password = process.env.SMS_LIVE_PASSWORD;
const sms_live_url = process.env.SMS_PLENTY_URL;
let token = null;
const main_id = 136;
var sage_main_id = 946;
const n24_property_group_id = 151;
const main_channel = "naturbettwaren24";
const n24_shipping_profile = 7;
const sage_url = process.env.SAGE_URL;
const category_table_name = 'category_mapping';
const item_table_name = 'item_mapping';
const amazon_item_table_name = 'item_mapping_amazon';
const sms_item_table_name = 'item_mapping_sms_live';
const image_table_name = 'image_mapping';
const image_table_name_sms_live = 'image_mapping_sms_live'
const tag_value_table_name = 'tag_value_mapping';
const tag_table_name = 'tag_mapping';
const variation_table_name = 'variationname_attributes';
const disabled_table_name = 'disabled_items';
const uncategorized_id = 433;
const variation_attribute_groupid = 195;
const tree_category_url = sage_url + "/PimCategories/tree/byChannel/";
const special_price_plenty_id = 3;
const plenty_id = process.env.PLENTY_ID;
const n24_plenty_id = process.env.N24_PLENTY_ID;
const sms_plenty_id = process.env.SMS_PLENTY_ID_LIVE;
const data = {
    username: username,
    password: password
};
const slack_token = process.env.SLACK;
const slack = new WebClient(slack_token);
//let id_mapping = [];
let id_mapping_filename = "id_mapping";


const MAX_RETRIES = 10;
let retryCount = 0;

async function start() {
    amqp.connect(server + "?heartbeat=3600", function (err, conn) {
        if (err) {
            console.error("[AMQP]", err.message);
            retryCount++;
            if (retryCount <= MAX_RETRIES) {
                return setTimeout(start, 1000 * retryCount); // Exponential backoff
            } else {
                console.error("[AMQP] Max retries reached. Exiting.");
                process.exit(1);
            }
        }
        retryCount = 0;
        conn.on("error", function (err) {
            if (err.message !== "Connection closing") {
                console.error("[AMQP] conn error", err.message);
            }
        });
        conn.on("close", function () {
            console.error("[AMQP] reconnecting");
            return setTimeout(start, 1000);
        });

        console.log("[AMQP] connected");
        amqpConn = conn;

        whenConnected();
    });
}

async function whenConnected() {
    startPublisher();
    await startWorker();
}

var pubChannel = null;
var offlinePubQueue = [];
function startPublisher() {
    amqpConn.createConfirmChannel(function (err, ch) {
        if (closeOnErr(err)) return;
        ch.on("error", function (err) {
            console.error("[AMQP] channel error", err.message);
        });
        ch.on("close", function () {
            console.log("[AMQP] channel closed");
        });

        pubChannel = ch;
        while (true) {
            var m = offlinePubQueue.shift();
            if (!m) break;
            publish(m[0], m[1], m[2]);
        }
    });
}

// method to publish a message, will queue messages internally if the connection is down and resend later
function publish(exchange, routingKey, content) {
    try {
        pubChannel.publish(exchange, routingKey, content, { persistent: true },
            function (err, ok) {
                if (err) {
                    console.error("[AMQP] publish", err);
                    offlinePubQueue.push([exchange, routingKey, content]);
                    pubChannel.connection.close();
                }
            });
    } catch (e) {
        console.error("[AMQP] publish", e.message);
        offlinePubQueue.push([exchange, routingKey, content]);
    }
}

// A worker that acks messages only if processed succesfully
async function startWorker() {
    await amqpConn.createChannel(async function (err, ch) {
        if (closeOnErr(err)) return;
        ch.on("error", function (err) {
            console.error("[AMQP] channel error", err.message);
        });
        ch.on("close", function () {
            console.log("[AMQP] channel closed");
        });
        ch.prefetch(1);
        await ch.assertQueue(queueName, { durable: true }, async function (err, _ok) {
            if (closeOnErr(err)) return;
            console.log("Worker is started");
            consumeMessages(ch); // Start the consumption loop
        });
    });
}

function consumeMessages(ch) {
    ch.consume(queueName, async function processMsg(msg) {
        if (msg !== null) {
            try {
                let payload = msg.content.toString();
                console.log(payload);
                await handleMessage(payload); // Wait for handleMessage to complete
                ch.ack(msg);
                 // Acknowledge the message after processing

                // Introduce a delay before processing the next message
                await new Promise(resolve => setTimeout(resolve, 5000));

            } catch (err) {
                console.error(err);
                ch.reject(msg, true); // Requeue the message if not processed successfully
            }
        }
    }, { noAck: false });
}

async function work(msg, cb) {
    //console.log("in worker");
    console.log("Got msg", msg.content.toString());
    cb(true);
}

function closeOnErr(err) {
    if (!err) return false;
    console.error("[AMQP] error", err);
    amqpConn.close();
    return true;
}

/* setInterval(function() {
    publish("", queueName, Buffer.from("Buffering..."));
}, 5000); */

start();

// sends a slack message with a text to channel 'plentymarket'
async function sendMessage(slacktext) {
    if (slacktext.length > 0) {
        try {
            const result = await slack.chat.postMessage({
                channel: '#plentymarket', // Ersetze 'channel-name' durch den Namen des gewünschten Channels
                text: slacktext,
            });
        } catch (error) {
            console.error('Fehler beim Senden der Nachricht:', error);
        }
    }
}

function getGermanTimestamp() {
    // Aktuelles Datum und Uhrzeitobjekt erstellen
    var jetzt = new Date();

    // Datum formatieren (TT.MM.JJJJ)
    var tag = jetzt.getDate();
    var monat = jetzt.getMonth() + 1; // Monate sind nullbasiert, daher +1
    var jahr = jetzt.getFullYear();

    // Uhrzeit formatieren (HH:MM:SS)
    var stunden = jetzt.getHours();
    var minuten = jetzt.getMinutes();
    var sekunden = jetzt.getSeconds();

    // Führende Nullen hinzufügen, wenn nötig
    tag = (tag < 10) ? '0' + tag : tag;
    monat = (monat < 10) ? '0' + monat : monat;
    stunden = (stunden < 10) ? '0' + stunden : stunden;
    minuten = (minuten < 10) ? '0' + minuten : minuten;
    sekunden = (sekunden < 10) ? '0' + sekunden : sekunden;

    // Das Datum und die Uhrzeit im deutschen Format zusammenstellen
    return tag + '_' + monat + '_' + jahr + '_' + stunden + ':' + minuten + ':' + sekunden;
}

function getGerTimestampForDB() {
    // Aktuelles Datum und Uhrzeitobjekt erstellen
    var jetzt = new Date();

    // Datum formatieren (TT.MM.JJJJ)
    var tag = jetzt.getDate();
    var monat = jetzt.getMonth() + 1; // Monate sind nullbasiert, daher +1
    var jahr = jetzt.getFullYear();

    // Uhrzeit formatieren (HH:MM:SS)
    var stunden = jetzt.getHours();
    var minuten = jetzt.getMinutes();
    var sekunden = jetzt.getSeconds();

    // Führende Nullen hinzufügen, wenn nötig
    tag = (tag < 10) ? '0' + tag : tag;
    monat = (monat < 10) ? '0' + monat : monat;
    stunden = (stunden < 10) ? '0' + stunden : stunden;
    minuten = (minuten < 10) ? '0' + minuten : minuten;
    sekunden = (sekunden < 10) ? '0' + sekunden : sekunden;

    // Das Datum und die Uhrzeit im deutschen Format zusammenstellen
    return jahr + '-' + monat + '-' + tag + ' ' + stunden + ':' + minuten + ':' + sekunden;
}

async function checkFileExists(filePath) {
    try {
        await fs.access(filePath, fs.constants.F_OK);
        //console.log(`${filePath} exists`);
    } catch (err) {
        console.error(`${filePath} does not exist`);
    }
}

async function postRequest(params) {
    //console.log(params);
    try {
        return new Promise(function (resolve, reject) {
            request.post(params, (err, res, body) => {
                //console.log(res);
                if (err) {
                    console.log(err);
                    return reject(new Error('statusCode=' + res));
                }
                if (body) {
                    //console.log(body);
                    resolve(body);
                }
            });
        });
    }
    catch (e) {
        await sendMessage(getGermanTimestamp() + msg);
        console.error(e);
    } finally {
        //console.log('We do cleanup here');
    }
}
async function getRequest(params) {
    try {
        return new Promise(function (resolve, reject) {
            request.get(params, (err, res, body) => {
                if (err) {
                    console.log(err);
                    return reject(new Error('statusCode=' + res));
                }
                if (body) {
                    resolve(body);
                }
            });
        });
    }
    catch (e) {
        await sendMessage(getGermanTimestamp() + msg);
        console.error(e);
    } finally {
        //console.log('We do cleanup here');
    }
}

async function findPlentyItemBySKU(sku, plenty_items) {
    for (let index = 0; index < plenty_items.length; index++) {
        let element = plenty_items[index];
        //console.log("external id: " + element.externalId);
        if (element.externalId == sku) {
            return element;
        }
    }
    return false;
}

async function getAllOrphans() {
    try {
        token = await getToken();
        var plenty_items = [];
        var first_page_for_total_count = await getInactiveItemsFromPlenty(1);
        var pages = first_page_for_total_count.lastPageNumber;
        for (let index = 1; index <= pages; index++) {
            //for (let index = 1; index <= 1; index++) {
            console.log(index + '/' + pages);
            var items = await getInactiveItemsFromPlenty(index);
            delay();
            for (let a = 0; a < items.entries.length; a++) {
                const element = items.entries[a];
                plenty_items.push(element);
            }
        }
        console.log(plenty_items.length);
        return plenty_items;
    } catch (error) {
        console.log(error);
    }
}

async function getAllItemsFromPlentymarkets() {
    try {
        token = await getToken();
        var plenty_items = [];
        var url = process.env.PLENTY_URL + "/rest/items/variations";
        var resp1;
        try {
            var header = {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
            const response = await axios.get(url, { headers: header });
            //console.log('Request successful. Response:', response.data.id);
            resp1 = response.data;
        }
        catch (error) {
            console.log(error);
            //console.error('Error in the request:', error.message);
            //throw error;
        }
        var pages = resp1.lastPageNumber;
        console.log("pages: " + pages);
        for (let index = 1; index <= pages; index++) {
            //for (let index = 1; index <= 3; index++) {
            console.log(index + '/' + pages);
            var url = process.env.PLENTY_URL + "/rest/items/variations?page=" + index;
            try {
                var header = {
                    'Authorization': 'Bearer ' + token,
                    'Content-Type': 'application/json'
                }
                var response = await axios.get(url, { headers: header });
                //console.log('Request successful. Response:', response.data.id);

                //console.log(response.data.entries);
                //console.log(response.data.entries.length);
                for (let a = 0; a < response.data.entries.length; a++) {
                    //for (let a = 0; a < 3; a++) {
                    const element = response.data.entries[a];
                    plenty_items.push(element);
                }
                await delay();
            }
            catch (error) {
                console.log(error);
            }

        }
        fs.appendFile("test.txt", JSON.stringify(plenty_items), function (err) {
            if (err) {
                return console.log(err);
            }
            console.log("The file was saved!");
        });
        console.log(plenty_items.length);

    } catch (error) {
        console.log(error);
    }
}

async function getAllItemsFromAmazon() {
    try {
        token = await getToken();
        var plenty_items = [];
        var first_page_for_total_count = await getItemsFromPlenty(1, plenty_id);
        var pages = first_page_for_total_count.lastPageNumber;
        for (let index = 1; index <= pages; index++) {
            //for (let index = 1; index <= 1; index++) {
            console.log(index + '/' + pages);
            var items = await getItemsFromPlenty(index, plenty_id);
            delay();
            for (let a = 0; a < items.entries.length; a++) {
                //for (let a = 0; a < 10; a++) {
                const element = items.entries[a];
                plenty_items.push(element);
            }
        }
        console.log(plenty_items.length);
        return plenty_items;
    } catch (error) {
        console.log(error);
    }
}

async function getAllInactiveItemsFromPlenty() {
    token = await getToken();
    var plenty_items = [];
    var first_page_for_total_count = await getItemsFromPlenty(1, false);
    var pages = first_page_for_total_count.lastPageNumber;
    for (let index = 1; index <= pages; index++) {
        var items = await getItemsFromPlenty(index, false);
        delay();
        for (let a = 0; a < items.entries.length; a++) {
            const element = items.entries[a];
            plenty_items.push(element);
        }
    }
    console.log(plenty_items.length);
    return plenty_items;
}

async function getAllItemsFromPlenty(plenty_id = process.env.N24_PLENTY_ID, api_url = process.env.PLENTY_URL) {
    var plenty_items = [];
    if(plenty_id) {
        var first_page_for_total_count = await getItemsFromPlenty(1, plenty_id, api_url);
        var pages = first_page_for_total_count.lastPageNumber;
        for (let index = 1; index <= pages; index++) {
            console.log(index+"/"+pages);
            var items = await getItemsFromPlenty(index, plenty_id, api_url);
            delay();
            for (let a = 0; a < items.entries.length; a++) {
                const element = items.entries[a];
                plenty_items.push(element);
            }
        }
    }
    else {
        var first_page_for_total_count = await getItemsFromPlenty(1, false);
        var pages = first_page_for_total_count.lastPageNumber;
        for (let index = 1; index <= pages; index++) {
            console.log(index+"/"+pages);
            var items = await getItemsFromPlenty(index, false);
            delay();
            for (let a = 0; a < items.entries.length; a++) {
                const element = items.entries[a];
                plenty_items.push(element);
            }
        }
    }
    console.log(plenty_items.length);
    return plenty_items;
}

async function getAllItemsFromSage(channel) {
    let url = sage_url + "/SkuList/Channel/" + channel;
    let header = {
        'Content-Type': 'application/json; charset=utf8'
    };
    var options = {
        url: url,
        json: true,
        method: "GET",
        headers: header
    }
    console.log("getting items from sage...");
    return await getRequest(options);
}

async function getSingleArticleFromSage(sku, channel) {
    let url = sage_url + '/PimItem/Sku/' + sku + '/Channel/' + channel;
    //console.log("url: "+url);
    let header = {
        'Content-Type': 'application/json; charset=utf8'
    };
    var options = {
        url: url,
        json: true,
        method: "GET",
        headers: header
    }
    return getRequest(options);
}

async function getSingleCategoryFromPlenty(category_id) {
    var header = {
        'Content-Type': 'application/json; charset=utf8',
        'Authorization': 'Bearer ' + token
    }
    console.log("getting category from plenty id: " + category_id);
    let url = process.env.PLENTY_URL + '/rest/categories/' + category_id;
    console.log("url: " + url);
    var options = {
        url: url,
        json: true,
        method: "GET",
        headers: header
    }
    return await getRequest(options);
}

async function updateFatherArticle(item, plenty_category_id, attributes, db_item, plenty_id_for_updatemain = n24_plenty_id, table_name = item_table_name) {
    var productItem = item.GetPimItemBySkuAndChannelResult.Data.ProductItem;
    if (item.GetPimItemBySkuAndChannelResult.Data.PackageItems.length) {
        var packageItem = item.GetPimItemBySkuAndChannelResult.Data.PackageItems[0];
    }
    else {
        var packageItem = false;
    }

    let response = await updateMainItem(productItem, plenty_category_id, packageItem, attributes, db_item, plenty_id_for_updatemain, table_name);
    //console.log(response);
    return response;
}

async function createFatherArticleInPlenty(item, plenty_category_id, attributes, mandanten_id = n24_plenty_id, api_url = process.env.PLENTY_URL) {
    var productItem = item.GetPimItemBySkuAndChannelResult.Data.ProductItem;
    if (item.GetPimItemBySkuAndChannelResult.Data.PackageItems.length) {
        var packageItem = item.GetPimItemBySkuAndChannelResult.Data.PackageItems[0];
    }
    else {
        var packageItem = false;
    }

    let response = await createMainItem(productItem, plenty_category_id, packageItem, attributes, mandanten_id, api_url);
    //console.log(response);
    return response;
}
async function updateChildrenArticle(item, plenty_category_id, attributes, db_item, plenty_id_for_update = n24_plenty_id, table_name = item_table_name) {
    var productItem = item.GetPimItemBySkuAndChannelResult.Data.ProductItem;
    if (item.GetPimItemBySkuAndChannelResult.Data.PackageItems.length) {
        var packageItem = item.GetPimItemBySkuAndChannelResult.Data.PackageItems[0];
    }
    else {
        var packageItem = false;
    }

    let response = await updateVariationItem(productItem, plenty_category_id, packageItem, attributes, db_item, plenty_id_for_update, table_name);
    //console.log(response);
    return response;
}

async function createChildrenArticleInPlentyDB(item, plenty_categories, father, attributes, mandanten_id = n24_plenty_id, name = item.VariantName, url = process.env.PLENTY_URL) {
    var productItem = item.GetPimItemBySkuAndChannelResult.Data.ProductItem;
    if (item.GetPimItemBySkuAndChannelResult.Data.PackageItems.length) {
        var packageItem = item.GetPimItemBySkuAndChannelResult.Data.PackageItems[0];
    }
    else {
        var packageItem = false;
    }
    //let response = await createMainItem(productItem, plenty_category_id, packageItem);
    let response = await createVariationThroughDB(productItem, plenty_categories, father, attributes, packageItem, mandanten_id, name, url)

    //console.log(response);
    return response;
}

async function createChildrenArticleInPlenty(item, plenty_categories, father, attributes) {
    var productItem = item.GetPimItemBySkuAndChannelResult.Data.ProductItem;
    if (item.GetPimItemBySkuAndChannelResult.Data.PackageItems.length) {
        var packageItem = item.GetPimItemBySkuAndChannelResult.Data.PackageItems[0];
    }
    else {
        var packageItem = false;
    }
    //let response = await createMainItem(productItem, plenty_category_id, packageItem);
    let response = await createVariation(productItem, plenty_categories, father, attributes, packageItem)
    //console.log(response);
    return response;
}


async function getTreeCategoriesFromSage(channel) {
    let url = tree_category_url + channel;
    let header = {
        'Content-Type': 'application/json; charset=utf8'
    };
    var options = {
        url: url,
        json: true,
        method: "GET",
        headers: header
    }
    console.log("getting categories in tree from sage...");
    return getRequest(options);
}

async function getCategoryFromPlentyByPlentyId(page = 1, plenty_id = n24_plenty_id) {
    var url = process.env.PLENTY_URL + "/rest/categories?plentyId=" + plenty_id + "&page=" + page + "&type=item";
    try {
        var header = {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        }
        const response = await axios.get(url, { headers: header });
        //console.log('Request successful. Response:', response.data.id);
        return response.data;
    }
    catch (error) {
        console.log(error);
        //console.error('Error in the request:', error.message);
        //throw error;
    }
}

async function getAllCategoryFromPlentyByPlentyId(plenty_id) {
    try {
        var categories = [];
        var first_page_for_total_count = await getCategoryFromPlentyByPlentyId(1, plenty_id);
        var pages = first_page_for_total_count.lastPageNumber;
        for (let index = 1; index <= pages; index++) {
            var items = await getCategoryFromPlentyByPlentyId(index);
            delay();
            for (let a = 0; a < items.entries.length; a++) {
                const element = items.entries[a];
                categories.push(element);
            }
        }
        return categories;
    } catch (error) {
        console.log(error);
    }
}

async function getCategoryFromPlenty(id) {
    var header = {
        'Content-Type': 'application/json; charset=utf8',
        'Authorization': 'Bearer ' + token
    }
    var options = {
        url: process.env.PLENTY_URL + '/rest/categories/?parentId=' + id,
        port: 443,
        path: '/rest/categories/' + id,
        method: 'GET',
        headers: header
    }
    console.log("getting category from plenty id: " + id);
    return getRequest(options);
}

async function getN24TagsFromPlenty() {
    var header = {
        'Content-Type': 'application/json; charset=utf8',
        'Authorization': 'Bearer ' + token
    }
    var options = {
        url: process.env.PLENTY_URL + '/rest/v2/properties?group=151&itemsPerPage=5000',
        port: 443,
        path: '/rest/v2/properties?group=151',
        method: 'GET',
        headers: header
    }
    return getRequest(options);
}

async function getN24TagValuesFromPlenty(tagId) {
    var header = {
        'Content-Type': 'application/json; charset=utf8',
        'Authorization': 'Bearer ' + token
    }
    var options = {
        url: process.env.PLENTY_URL + '/rest/v2/properties/' + tagId + '/selections?group=151&itemsPerPage=5000',
        port: 443,
        path: '/rest/v2/properties?group=151',
        method: 'GET',
        headers: header
    }
    console.log("tag value request");
    return getRequest(options);
}

async function deleteCategoryFromPlenty(id) {
    var header = {
        'Content-Type': 'application/json; charset=utf8',
        'Authorization': 'Bearer ' + token
    }
    var options = {
        url: process.env.PLENTY_URL + '/rest/categories/' + id,
        port: 443,
        path: '/rest/categories/' + id,
        method: 'DELETE',
        headers: header
    }
    console.log("deleting category from plenty id: " + id);
    request(options, function (error, response, body) {
        if (!error && response.statusCode === 200) {
            //console.log('Request successful. Response:', body);
        } else {
            console.error('Error in the request:', error);
            console.error('Response:', body);
        }
    });
}

async function getAllAttributes(pagenumber = 0) {
    if (pagenumber > 0) {
        var url = process.env.PLENTY_URL + '/rest/items/attributes?page=' + pagenumber;
    }
    else {
        var url = process.env.PLENTY_URL + '/rest/items/attributes';
    }
    var header = {
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json; charset=utf8'
    }
    var options = {
        url: url,
        json: true,
        method: "GET",
        headers: header
    }
    console.log("getting attributes from plenty...");
    return await getRequest(options);
}

async function getAllAttributeValues(id, pagenumber = 0) {
    if (pagenumber > 0) {
        var url = process.env.PLENTY_URL + '/rest/items/attributes/' + id + '/values?page=' + pagenumber;
    }
    else {
        var url = process.env.PLENTY_URL + '/rest/items/attributes/' + id + '/values';
    }
    var header = {
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json; charset=utf8'
    }
    var options = {
        url: url,
        json: true,
        method: "GET",
        headers: header
    }
    console.log("getting attributes from plenty...");
    return await getRequest(options);
}

async function exportAttributesSage() {
    var deleted = [];
    var non_public = [];
    var valid = [];
    var initObj = {};
    await exportData(initObj, "deleted_tags");
    await exportData(initObj, "non_public_tags");
    await exportData(initObj, "valid_tags");
    token = await getToken();
    var sageAttr = await getAllTagsFromSage();
    sageAttr = sageAttr.GetTagItemsListResult.Data;
    for (let index = 0; index < sageAttr.length; index++) {
        const element = sageAttr[index];
        let main_attr_payload = {
            id: element.Id,
            name: element.Name
        }
        valid.push(main_attr_payload);
        let options = element.Options;
        for (let y = 0; y < options.length; y++) {
            const item = options[y];
            if (item.IsDeleted) {
                deleted.push(item);
            }
            if (!item.IsPublic) {
                non_public.push(item);
            }
            if (!item.IsDeleted && item.IsPublic) {
                valid.push(item);
            }
        }
    }
    await exportData(deleted, "deleted_tags");
    await exportData(non_public, "non_public_tags");
    await exportData(valid, "valid_tags");
}
async function exportAttributes() {
    var attribute_values = [];
    var main_attributes = [];
    var initObj = {};
    token = await getToken();
    await exportData(initObj, "all_attribute_values");
    var attributes = await getAllAttributes();
    var number_of_pages = attributes.lastPageNumber;
    for (let index = 1; index <= number_of_pages; index++) {
        attributes = await getAllAttributes(index);
        for (let x = 0; x < attributes.entries.length; x++) {
            var element = attributes.entries[x];
            main_attributes.push(element);
        }
    }
    console.log("length: " + main_attributes.length);
    for (let index = 0; index < main_attributes.length; index++) {
        const element = main_attributes[index];
        console.log(element.backendName);
        var attri_values = await getAllAttributeValues(element.id);
        var number_of_pages = attri_values.lastPageNumber;
        if (number_of_pages > 1) {
            for (let index = 1; index <= number_of_pages; index++) {
                var attri_values = await getAllAttributeValues(element.id, index);
                for (let index = 0; index < attri_values.entries.length; index++) {
                    const element = attri_values.entries[index];
                    attribute_values.push(element);
                }
            }
        }
        else {
            for (let index = 0; index < attri_values.entries.length; index++) {
                const element = attri_values.entries[index];
                attribute_values.push(element);
            }
        }
    }
    await exportData(attribute_values, "all_attribute_values");
}


async function getAllTagsFromSage() {
    let url = sage_url + "/TagItems";
    let header = {
        'Content-Type': 'application/json; charset=utf8'
    };
    var options = {
        url: url,
        json: true,
        method: "GET",
        headers: header
    }
    console.log("getting attributes from sage...");
    return await getRequest(options);
}

async function getAttributeValueByOptionId(tag_element, option_id) {
    if (tag_element.options.length) {
        for (const element of tag_element.options) {
            if (element.option_id == option_id) {
                return element;
            }
        }
    }
    return false;
}

async function getAttributeByTagId(attributes, id) {
    for (const element of attributes) {
        if (element.tag_id == id) {
            return element;
        }
    }
    return false;
}

async function prepareCategories(productCategories, category_mapping) {
    var plenty_category_ids = [];
    var categories_for_mapping = [];
    let plenty_category_id = uncategorized_id;
    for (let index = 0; index < productCategories.length; index++) {
        let sage_category_id = productCategories[index];
        if (sage_category_id == sage_main_id) {
            plenty_category_id = main_id;
        }
        else {
            let mapping_obj = await getCategoryBySageId(sage_category_id, category_mapping);
            plenty_category_id = mapping_obj.plenty_category_id;
        }
        var cat_for_plen = {
            categoryId: plenty_category_id
        }
        plenty_category_ids.push(cat_for_plen);
        var temp_for_mapping = {
            'sage_category_id': sage_category_id,
            'plenty_category_id': plenty_category_id
        }
        categories_for_mapping.push(temp_for_mapping);
    }
    if (plenty_category_ids.length == 0) {
        var cat_for_plen = {
            categoryId: uncategorized_id
        }
        plenty_category_ids.push(cat_for_plen);
        var temp_for_mapping = {
            'sage_category_id': uncategorized_id,
            'plenty_category_id': uncategorized_id
        }
        categories_for_mapping.push(temp_for_mapping);
    }
    var result = {
        'categories_for_mapping': categories_for_mapping,
        'plenty_category_ids': plenty_category_ids
    }
    return result;
}

async function checkExistence(attri_values, name) {
    for (let index = 0; index < attri_values.length; index++) {
        const element = attri_values[index];
        //console.log(element.backendName);
        if (element.backendName == name || element.backendName == "HN-" + name) {
            //console.log(name + ' exists');
            return false;
        }
    }
    console.log("--------");
    console.log(name);
    console.log("dosent exist");
    console.log("--------");
    return true;
}

async function getAttributeIdByName(attributes, name) {
    for (let index = 0; index < attributes.length; index++) {
        const element = attributes[index];
        if (element.backendName === name) {
            return element.id;
        }
    }
    for (let index = 0; index < attributes.length; index++) {
        const element = attributes[index];
        if (element.backendName === 'HN-' + name) {
            return element.id;
        }
    }
    return 0;
}

async function syncAttributesWithDB(channel) {
    try {
        token = await getToken();
        let attr_val_first_page = await getAllAttributeValues(variation_attribute_groupid);
        let attr_val = [];
        for (let index = 0; index < attr_val_first_page.lastPageNumber; index++) {
            let attr_page = await getAllAttributeValues(variation_attribute_groupid, index + 1);
            for (let index = 0; index < attr_page.entries.length; index++) {
                const element = attr_page.entries[index];
                attr_val.push(element);
            }
        }
        console.log(attr_val.length);
        for (let index = 0; index < attr_val.length; index++) {
            const element = attr_val[index];
            let checkAttrVal = await mysql.getItemInDB('variationname_attributes', element.backendName, 'name');
            if (checkAttrVal.length) {
                console.log(element.backendName + "already exists");
            }
            else {
                var payload = {
                    name: element.backendName,
                    id: element.id
                }
                await mysql.insertIntoVariationnameAttributesMapping(payload);
            }
        }

    } catch (error) {

    }

}


async function updateAttributes(channel) {
    var main_attributes = [];
    var attribute_values = [];
    var initArr = [];
    var initObj = {};
    var logdata = [];
    token = await getToken();
    var logdataname = "attribute_log_" + getGermanTimestamp();
    var sageAttr = await getAllTagsFromSage();
    await exportData(initObj, "attribute_mapping");
    await exportData(initObj, logdataname);
    sageAttr = sageAttr.GetTagItemsListResult.Data;
    var plentyAttr = await getAllAttributes();
    var number_of_pages = plentyAttr.lastPageNumber;
    for (let index = 1; index <= number_of_pages; index++) {
        attributes = await getAllAttributes(index);
        for (let x = 0; x < attributes.entries.length; x++) {
            var element = attributes.entries[x];
            main_attributes.push(element);
        }
    }
    for (let index = 0; index < main_attributes.length; index++) {
        const element = main_attributes[index];
        console.log(element.backendName);
        attribute_values.push(element);
        var attri_values = await getAllAttributeValues(element.id);
        var number_of_pages = attri_values.lastPageNumber;
        if (number_of_pages > 1) {
            for (let index = 1; index <= number_of_pages; index++) {
                var attri_values = await getAllAttributeValues(element.id, index);
                for (let index = 0; index < attri_values.entries.length; index++) {
                    const element = attri_values.entries[index];
                    attribute_values.push(element);
                }
            }
        }
        else {
            for (let index = 0; index < attri_values.entries.length; index++) {
                const element = attri_values.entries[index];
                attribute_values.push(element);
            }
        }
    }

    console.log("number of attr values + attributes:" + attribute_values.length);
    console.log("number of sage tags:" + sageAttr.length);
    for (let index = 0; index < sageAttr.length; index++) {
        const element = sageAttr[index];
        //console.log(element.Name);
        var check = true;
        check = await checkExistence(attribute_values, element.Name);
        var resp_id = 0;
        if (check) {
            var resp = await createAttribute("HN-" + element.Name);
            resp_id = resp.id;
            var payload = {
                name: "HN-" + element.Name,
                tag_id: element.Id,
                attribute_id: resp_id,
                options: []
            }
            var forTest = {
                id: element.Id,
                name: element.Name,
                attr_id: resp_id
            }
            logdata.push(forTest);
            //await exportData(logdata, "attribute_log");
            await exportData(logdata, logdataname);
            console.log(element.Name + " didnt exist");
        }
        else {
            var attr_id = await getAttributeIdByName(attribute_values, element.Name);
            var payload = {
                name: "HN-" + element.Name,
                tag_id: element.Id,
                attribute_id: attr_id,
                options: []
            }
            console.log(element.Name + " exists");
        }
        for (let x = 0; x < element.Options.length; x++) {
            const item = element.Options[x];

            var resp_id_attrvalue = 0;
            var resp_attr_id = 0;
            var value_exists = true;
            if (item.OptionValue != 'NEW TAG' && item.OptionValue != 'TEST' && item.IsPublic && !item.IsDeleted) {
                value_exists = await checkExistence(attribute_values, item.OptionValue);
                if (value_exists) {
                    //console.log("parent attr id before: " + resp_id);
                    if (!check) {
                        resp_id = await getAttributeIdByName(attribute_values, element.Name);
                    }
                    var option_name = "HN-" + item.OptionValue + ' ' + item.OptionId
                    var response = await createAttributeValue(resp.id, option_name);
                    resp_id_attrvalue = response.id;
                    resp_attr_id = response.attributeId;
                    var forTest = {
                        id: item.OptionId,
                        name: item.OptionValue,
                        attr_val_id: resp_id_attrvalue,
                        resp_attr_id: resp_attr_id
                    }
                    logdata.push(forTest);
                    await exportData(logdata, logdataname);
                    var optionPayload = {
                        option_id: item.OptionId,
                        name: option_name,
                        attribute_value_id: resp_id_attrvalue,
                        attribute_id: response.attributeId
                    };
                    payload.options.push(optionPayload);
                    await delay();
                }
            }
            else {
                //console.log(item.OptionValue + ' exists');
            }
        }
        initArr.push(payload);
    }
    await exportData(initArr, "attribute_mapping");

}

async function initializeAttributes() {
    var attributes = await loadJsonFile('attribute_mapping.json');
    var initObj = {};
    var initArr = [];
    var logdata = [];
    token = await getToken();
    if (attributes === null) {
        await exportData(initObj, "attribute_mapping");
    }
    await exportData(initObj, "attribute_log");
    console.log("initializing attributes");
    var sageAttr = await getAllTagsFromSage();
    sageAttr = sageAttr.GetTagItemsListResult.Data;
    //creating attributes first
    for (let index = 0; index < sageAttr.length; index++) {
        const element = sageAttr[index];
        var attr_name = "HN-" + element.Name;
        var resp_id = 0;
        console.log(element.Id);
        console.log(element.Name);
        console.log(element.Options.length);
        var resp = await createAttribute(attr_name);
        resp_id = resp.id;
        var payload = {
            name: attr_name,
            tag_id: element.Id,
            attribute_id: resp_id,
            options: []
        }
        var forTest = {
            id: element.Id,
            name: element.Name,
            attr_id: resp_id
        }
        logdata.push(forTest);
        await exportData(logdata, "attribute_log");
        for (let x = 0; x < element.Options.length; x++) {
            const item = element.Options[x];
            var resp_id = 0;
            var resp_attr_id = 0;
            if (item.OptionValue != 'NEW TAG' && item.OptionValue != 'TEST' && item.IsPublic && !item.IsDeleted) {
                var option_name = "HN-" + item.OptionValue
                console.log(item.OptionId);
                console.log(item.OptionValue);
                var response = await createAttributeValue(resp.id, option_name + ' ' + item.OptionId);
                resp_id = response.id;
                resp_attr_id = response.attributeId;
                var forTest = {
                    id: item.OptionId,
                    name: item.OptionValue,
                    attr_val_id: resp_id,
                    resp_attr_id: resp_attr_id
                }
                logdata.push(forTest);
                await exportData(logdata, "attribute_log");
                var optionPayload = {
                    option_id: item.OptionId,
                    name: option_name,
                    attribute_value_id: response.id,
                    attribute_id: response.attributeId
                };
                payload.options.push(optionPayload);
                await delay();
            }
        }
        initArr.push(payload);
    }
    await exportData(initArr, "attribute_mapping");
    console.log(payload);
}

async function deletePlentyArticle(id) {
    console.log("deleting " + id);
    var url = process.env.PLENTY_URL + '/rest/items/' + id;
    var header = {
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
    }
    try {
        const response = await axios.delete(url, { headers: header });
        console.log('Request successful. Response:', response.data);
        return response.data;
    } catch (error) {
        console.error('Error in the request:', error.message);
        throw error;
    }
}

async function createAttributeValue(id, name) {
    var url = process.env.PLENTY_URL + '/rest/items/attributes/' + id + '/values';
    var header = {
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
    }
    var simple_payload = {
        attributeId: id,
        backendName: name,
        isSurchargePercental: true,
        isLinkableToImage: true,
        typeOfSelectionInOnlineStore: "dropdown",
    }
    try {
        const response = await axios.post(url, simple_payload, { headers: header });
        console.log('Request successful. Response:', response.data);
        return response.data;
    } catch (error) {
        console.log(error);
        console.error('Error in the request:', error.message);
        throw error;
    }
}


async function createAttribute(name) {
    console.log("creating attribute: " + name);
    var url = process.env.PLENTY_URL + '/rest/items/attributes';
    var header = {
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
    }
    var simple_payload = {
        backendName: name,
        isSurchargePercental: true,
        isLinkableToImage: true,
        typeOfSelectionInOnlineStore: "dropdown",
        isGroupable: true
    }
    try {
        const response = await axios.post(url, simple_payload, { headers: header });
        //console.log('Request successful. Response:', response.data);
        return response.data;
    } catch (error) {
        console.error('Error in the request:', error.message);
        throw error;
    }
}

async function createVariationThroughDB(item, item_categories, father, attributes, packageItem, mandanten_id = n24_plenty_id, name = item.VariantName, api_url = process.env.PLENTY_URL) {
    var url = api_url + '/rest/items/' + father.plenty_item_id + '/variations';
    var keywords = '';
    var weight = 0;
    var width = 0;
    var length = 0;
    var height = 0;
    if (packageItem) {
        if ('Weight' in packageItem) {
            weight = packageItem.Weight;
        }
        if ('Width' in packageItem) {
            width = packageItem.Width;
        }
        if ('Length' in packageItem) {
            length = packageItem.Length;
        }
        if ('Height' in packageItem) {
            height = packageItem.Height;
        }
    }
    for (let index = 0; index < item.ChannelSpecificProductDetails.Keywords.length; index++) {
        const element = item.ChannelSpecificProductDetails.Keywords[index];
        keywords += element + ', ';
    }
    var header = {
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
    }
    var simple_payload = {
        itemId: father.plenty_item_id,
        isActive: true,
        isMain: false,
        name: name,
        externalId: item.Sku,
        variationCategories: item_categories,
        variations: [
            {
                isActive: true,
                isMain: false,
                name: name,
                externalId: item.Sku,
                variationCategories: item_categories,
                unit: {
                    unitId: 1,
                    content: 1
                }
            }
        ],
        isAvailableIfNetStockIsPositive: true,
        isInvisibleInListIfNetStockIsNotPositive: true,
        isVisibleInListIfNetStockIsPositive: true,
        variationClients: [
            {
                plentyId: mandanten_id
            }
        ],
        unit: {
            unitId: 1,
            content: 1
        },
        variationAttributeValues: attributes
    };
    try {
        console.log(url);
        console.log("Payload: ");
        console.log(simple_payload);
        console.log("-----------");
        console.log("variationAttributeValues:");
        console.log(simple_payload.variationAttributeValues);
        console.log("-----------");
        console.log("variations");
        console.log(simple_payload.variations);
        //const response = await axios.post(url, simple_payload, { headers: header });
        //console.log('Request successful. Response:', response.data);
        //console.log(response);
       // return response.data;
    } catch (error) {
        console.error(error.response);
        //mysql.logFail(error.response.statusText + ' ' +item.Sku)
        throw error;
    }
}

async function createVariation(item, item_categories, father, attributes, packageItem) {
    var url = process.env.PLENTY_URL + '/rest/items/' + father.itemId + '/variations';
    var keywords = '';
    var weight = 0;
    var width = 0;
    var length = 0;
    var height = 0;
    if (packageItem) {
        if ('Weight' in packageItem) {
            weight = packageItem.Weight;
        }
        if ('Width' in packageItem) {
            width = packageItem.Width;
        }
        if ('Length' in packageItem) {
            length = packageItem.Length;
        }
        if ('Height' in packageItem) {
            height = packageItem.Height;
        }
    }
    for (let index = 0; index < item.ChannelSpecificProductDetails.Keywords.length; index++) {
        const element = item.ChannelSpecificProductDetails.Keywords[index];
        keywords += element + ', ';
    }
    var header = {
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
    }
    var simple_payload = {
        itemId: father.itemId,
        isActive: true,
        isMain: false,
        name: item.VariantName,
        externalId: item.Sku,
        variationCategories: item_categories,
        variations: [
            {
                isActive: true,
                isMain: false,
                name: item.VariantName,
                externalId: item.Sku,
                variationCategories: item_categories,
                unit: {
                    unitId: 1,
                    content: 1
                }
            }
        ],
        variationClients: [
            {
                plentyId: n24_plenty_id
            }
        ],
        unit: {
            unitId: 1,
            content: 1
        },
        variationAttributeValues: attributes
    };
    await exportData(simple_payload, "output");
    try {
        const response = await axios.post(url, simple_payload, { headers: header });
        //console.log('Request successful. Response:', response.data);
        return response.data;
    } catch (error) {
        console.error('Error in the request:', error.message);
        throw error;
    }
}

async function handleCategories(sageCategories, category_table = category_table_name) {
    console.log("handling categories now");
    var categories = [];
    var categories_db = [];
    var back = [];
    for (let index = 0; index < sageCategories.length; index++) {
        let sage_category_id = sageCategories[index];
        //console.log(details);
        var db_entry = await mysql.getItemInDB(category_table, sage_category_id, 'sage_id');
        if (db_entry.length) {
            //console.log('category exists ' + db_entry[0].name);
            var cat_for_plen = {
                categoryId: db_entry[0].plenty_category_id
            }
            var cat_for_db = {
                categoryId: db_entry[0].plenty_category_id,
                sage_category_id: sage_category_id
            }
            categories_db.push(cat_for_db);
            categories.push(cat_for_plen);
        }
        else {

        }
        //console.log(db_entry);
    }
    if (categories.length < 1) {
        var cat_for_plen = {
            categoryId: uncategorized_id
        }
        var cat_for_db = {
            categoryId: uncategorized_id,
            sage_category_id: uncategorized_id
        }
        categories_db.push(cat_for_db);
        categories.push(cat_for_plen);
    }
    back['plenty'] = categories;
    back['db'] = categories_db;
    return back;
}

async function updateCategoryFromQueue(sage_id, channel) {
    // check if category exists in DB
    let cat_db = await mysql.getItemInDB(category_table_name, sage_id, 'sage_id');
    if (cat_db.length) {
        // update category
    }
    else {
        // create category in plenty
    }
}

async function updateItemFromQueue(id, channel) {
    channel = channel.toLocaleLowerCase();
    var checkChannel = await isChannelProductSyncActive(channel);
    console.log(checkChannel);
    if(checkChannel) {
        console.log("in updateitemfromqueue");
        let plenty_id_for_update = n24_plenty_id;
        let table_name = item_table_name;
        if (channel == "amazon") {
            console.log("updating amazon item");
            table_name = amazon_item_table_name;
            plenty_id_for_update = plenty_id;
        }
        if (channel == "suckmystraw") {
            console.log("updating sms item");
            table_name = sms_item_table_name;
            plenty_id_for_update = sms_plenty_id;
        }
    
        console.log("tablename = " +table_name);
        console.log("channel " + channel);
        token = await getToken();
        const sku = id;
        console.log("updating item with sku:" + sku);
        let existsDB = await mysql.getItemInDB(table_name, sku, 'sku');
        let existsPlenty = false;
        if (existsDB.length) {
            existsPlenty = await getItemFromPlenty(existsDB[0].plenty_item_id);
        }
        let categories;
        let item;
        let item_from_db;
        let variations;
        let product_item;
        let details;
        let sageCategories;
        if (existsDB.length && !existsPlenty.hasOwnProperty('error')) {
            item_from_db = existsDB[0];
            item = await getSingleArticleFromSage(sku, channel);
            if (item) {
                product_item = item.GetPimItemBySkuAndChannelResult.Data.ProductItem;
                details = product_item.ChannelSpecificProductDetails;
                sageCategories = details.ProductCategories;
                try {
                    categories = await handleCategories(sageCategories);
                } catch (error) {
                    console.log(error);
                }
                console.log("categories:");
                console.log(categories['db']);
                try {
                    variations = await handleVariantNameDB(product_item.VariantName);
                } catch (error) {
                    console.log(error);
                }
                if(plenty_id_for_update == sms_plenty_id) {
                    variations = null;
                }
                //mysql.updateTimestamp(item_table_name, sku, 'sku');
                token = await getToken();
                if (details.FatherId === null) {
                    var plenty_item = await updateFatherArticle(item, categories['plenty'], variations, item_from_db, plenty_id_for_update, table_name);
                    console.log("plenty_item: ");
                    console.log(plenty_item.id);
                    if (plenty_item) {
                        let temp = {
                            'id': plenty_item.mainVariationId,
                            'itemId': plenty_item.id,
                            'name': product_item.ProductName,
                            'sage_id': product_item.ChannelSpecificProductDetails.Id,
                            'father_id': product_item.ChannelSpecificProductDetails.FatherId,
                            'father_sku': product_item.ChannelSpecificProductDetails.FatherSku,
                            'sku': product_item.Sku,
                            'categories': categories['db'],
                            'variation_attributes': variations
                        }
                        console.log("updating table now");
                        await mysql.updateItemMappingItem(temp, table_name)
                        console.log(product_item.ProductName);
                    }
                    else {
                        console.log('in else');
                        await mysql.logFail('failed creating father article with ' + item);
                    }
                }
                else {
                    // searching for father item in DB first
                    var father_id = details.FatherId;
                    var father_item = await mysql.getItemInDB(item_table_name, father_id, 'sage_id');
                    father_item = father_item[0];
                    if (father_item) {
                        var plenty_item = await updateChildrenArticle(item, categories['plenty'], variations, item_from_db, plenty_id_for_update, table_name = item_table_name);
                        console.log("plenty_item: ");
                        console.log(plenty_item.id);
                        if (plenty_item) {
                            console.log(product_item.ProductName);
                            let temp = {
                                'id': plenty_item.id,
                                'itemId': plenty_item.itemId,
                                'name': product_item.ProductName,
                                'sage_id': product_item.ChannelSpecificProductDetails.Id,
                                'father_id': product_item.ChannelSpecificProductDetails.FatherId,
                                'father_sku': product_item.ChannelSpecificProductDetails.FatherSku,
                                'sku': product_item.Sku,
                                'categories': categories['db'],
                                'variation_attributes': variations
                            }
                            console.log("updating table now");
                            await mysql.updateItemMappingItem(temp, table_name)
                        }
                        else {
                            console.log('in else');
                            await mysql.logFail('failed creating father article with ' + item);
                        }
                    }
                    else {
                        console.log("no father item found in DB");
                        await mysql.logFail('no father item found for ' + sku);
                    }
                }
            }
        }
        else {
            console.log("item does not exist yet");
            await createItemFromQueue(sku, channel, true, plenty_id_for_update, table_name);
            await delay();
        }
        await delay();
    
        console.log("done!");
        return true;
    }
    
}

async function createItemFromQueue(id, channel, existsInPlenty = false, mandanten_id = n24_plenty_id, table_name = item_table_name, api_url = process.env.PLENTY_URL, token = false) {
    const sku = id;
    console.log("creating item with sku: " + id);
    if (channel == "amazon") {
        console.log("updating amazon item");
        table_name = "item_mapping_amazon";
        mandanten_id = plenty_id;
    }
    console.log("TABLE = " + table_name);
    let existsDB = await mysql.getItemInDB(table_name, sku, 'sku');
    let categories;
    let item;
    let variations;
    let product_item;
    let details;
    let sageCategories;
    if (existsDB.length && !existsInPlenty) {
        console.log("item already exists in db");
    }
    else {
        item = await getSingleArticleFromSage(sku, channel);
        if (item) {
            product_item = item.GetPimItemBySkuAndChannelResult.Data.ProductItem;
            details = product_item.ChannelSpecificProductDetails;
            sageCategories = details.ProductCategories;
            categories = await handleCategories(sageCategories);
            //console.log("variant name test: " + product_item.VariantName);
            variations = await handleVariantNameDB(product_item.VariantName);
            console.log(categories);
            console.log(variations);


            token = await getToken(data.password, data.username, api_url, token);
            if (details.FatherId === null) {
                var plenty_item = await createFatherArticleInPlenty(item, categories['plenty'], variations, mandanten_id, api_url);
                console.log("plenty_item: ");
                console.log(plenty_item.id);
                if (plenty_item) {
                    console.log(product_item.ProductName);
                    let temp = {
                        'id': plenty_item.variations[0].id,
                        'itemId': plenty_item.variations[0].itemId,
                        'name': product_item.ProductName,
                        'sage_id': product_item.ChannelSpecificProductDetails.Id,
                        'father_id': product_item.ChannelSpecificProductDetails.FatherId,
                        'father_sku': product_item.ChannelSpecificProductDetails.FatherSku,
                        'sku': product_item.Sku,
                        'categories': categories['db'],
                        'variation_attributes': variations
                    }
                    console.log("inserting into db now, table "+table_name);
                    await mysql.insertIntoItemMapping(temp, table_name);
                }
                else {
                    console.log('in else');
                    await mysql.logFail('failed creating father article with ' + item);
                }
            }
            else {
                // searching for father item in DB first
                var father_id = details.FatherId;
                var father_item = await mysql.getItemInDB(item_table_name, father_id, 'sage_id');
                father_item = father_item[0];
                if (father_item) {
                    var plenty_item = await createChildrenArticleInPlentyDB(item, categories['plenty'], father_item, variations, mandanten_id, api_url);
                    let temp = {
                        'id': plenty_item.id,
                        'itemId': plenty_item.itemId,
                        'name': product_item.ProductName,
                        'sage_id': product_item.ChannelSpecificProductDetails.Id,
                        'father_id': product_item.ChannelSpecificProductDetails.FatherId,
                        'father_sku': product_item.ChannelSpecificProductDetails.FatherSku,
                        'sku': product_item.Sku,
                        'categories': categories['db'],
                        'variation_attributes': variations
                    }
                    console.log("inserting into db now table: " + table_name);
                    await mysql.insertIntoItemMapping(temp, table_name);
                }
                else {
                    console.log("no father item found in DB");
                    await mysql.logFail('no father item found for ' + sku);
                }
            }
            await createImageFromQueue(sku, channel, token, api_url);
        }
    }


}

async function handleKeywordsAttributesAndPayloadForVariation(product_item, item_from_db, categories, variations, packageItem, plenty_id = n24_plenty_id) {
    try {
        var returnArr = [];
        var keywords = '';
        var weight = 0;
        var width = 0;
        var length = 0;
        var height = 0;
        var mayShowUnitPrice = false;
        if (product_item.BasePriceFactor != 1.0000) {
            mayShowUnitPrice = true;
            console.log(product_item.Sku + " mayshowunitprice = true");
        }
        if (packageItem) {
            if ('Weight' in packageItem) {
                console.log(packageItem.Weight);
                weight = packageItem.Weight * 1000;
            }
            if ('Width' in packageItem) {
                width = packageItem.Width * 10;
            }
            if ('Length' in packageItem) {
                length = packageItem.Length * 10;
            }
            if ('Height' in packageItem) {
                height = packageItem.Height * 10;
            }
        }
        for (let index = 0; index < product_item.ChannelSpecificProductDetails.Keywords.length; index++) {
            const element = product_item.ChannelSpecificProductDetails.Keywords[index];
            keywords += element + ', ';
        }
        var simple_payload = {
            isActive: product_item.ChannelSpecificProductDetails.Enabled,
            name: product_item.VariantName,
            itemId: item_from_db.plenty_item_id,
            id: item_from_db.plenty_id,
            weightG: weight,
            weightNetG: weight,
            widthMM: width,
            lengthMM: length,
            heightMM: height,
            externalId: product_item.Sku,
            variationClients: [{
                variationId: item_from_db.plenty_id,
                plentyId: plenty_id
            }],
            variationCategories: categories['plenty'],
            isAvailableIfNetStockIsPositive: true,
            isVisibleInListIfNetStockIsPositive: true,
            mayShowUnitPrice: mayShowUnitPrice,
           // variationAttributeValues: variations
        };
        console.log("in handleKeywordsAttributesAndPayloadForVariation");
        return simple_payload;
    } catch (error) {
        console.log(error);
    }

}

async function prepareBulkArrayForItemUpdate(sage_items, table_name = item_table_name, plenty_id_fromfunc = n24_plenty_id) {
    try {
        let father_items = [];
        let variation_items = [];
        let response = [];
        for (let index = 0; index < sage_items.length; index++) {
            const element = sage_items[index];
            let sku = element.GetPimItemBySkuAndChannelResult.Data.ProductItem.Sku;
            console.log("sku: " + sku);
            if (sku) {
                let existsDB = await mysql.getItemInDB(table_name, element.GetPimItemBySkuAndChannelResult.Data.ProductItem.Sku, 'sku');
                let categories;
                let item;
                let item_from_db;
                let variations;
                let product_item;
                let details;
                let sageCategories;
                item_from_db = existsDB[0];
                if (element) {
                    console.log("element found");
                    product_item = element.GetPimItemBySkuAndChannelResult.Data.ProductItem;
                    if (product_item.hasOwnProperty("ChannelSpecificProductDetails")) {
                        details = product_item.ChannelSpecificProductDetails;
                        if (details.hasOwnProperty('ProductCategories')) {
                            sageCategories = details.ProductCategories;
                        }
                        if (element.GetPimItemBySkuAndChannelResult.Data.PackageItems.length) {
                            var packageItem = element.GetPimItemBySkuAndChannelResult.Data.PackageItems[0];
                        }
                        else {
                            var packageItem = false;
                        }
                        try {
                            categories = await handleCategories(sageCategories);
                        } catch (error) {
                            console.log(error);
                        }
                        //console.log("categories:");
                        //console.log(categories['db']);
                        try {
                            variations = await handleVariantNameDB(product_item.VariantName);
                        } catch (error) {
                            console.log(error);
                        }
                        if (details.FatherId === null) {
                            try {
                                var variationPayloadData = await handleKeywordsAttributesAndPayloadForVariation(product_item, item_from_db, categories, variations, packageItem, plenty_id_fromfunc);
                                variation_items.push({
                                    'payload': variationPayloadData,
                                    'db_item': item_from_db,
                                    'product_item': product_item,
                                    'categories': categories,
                                    'variations': variations
                                })
                            } catch (error) {
                                console.log(error);
                            }
                            var keywords = '';
                            var weight = 0;
                            var width = 0;
                            var length = 0;
                            var height = 0;
                            if (packageItem) {
                                if ('Weight' in packageItem) {
                                    weight = packageItem.Weight;
                                }
                                if ('Width' in packageItem) {
                                    width = packageItem.Width;
                                }
                                if ('Length' in packageItem) {
                                    length = packageItem.Length;
                                }
                                if ('Height' in packageItem) {
                                    height = packageItem.Height;
                                }
                            }
                            for (let index = 0; index < product_item.ChannelSpecificProductDetails.Keywords.length; index++) {
                                const elementK = product_item.ChannelSpecificProductDetails.Keywords[index];
                                keywords += elementK + ', ';
                            }
                            /* father_items.push({
                                'payload': simple_payload,
                                'db_item': item_from_db,
                                'product_item': product_item,
                                'categories': categories,
                                'variations': variations
                            }) */
                        }
                        else {
                            // searching for father item in DB first
                            var father_id = details.FatherId;
                            var father_item = await mysql.getItemInDB(item_table_name, father_id, 'sage_id');
                            father_item = father_item[0];
                            if (father_item) {
                                //var plenty_item = await updateChildrenArticle(item, categories['plenty'], variations, item_from_db);
                                var variationPayloadData = await handleKeywordsAttributesAndPayloadForVariation(product_item, item_from_db, categories, variations, packageItem, plenty_id_fromfunc);
                                variation_items.push({
                                    'payload': variationPayloadData,
                                    'db_item': item_from_db,
                                    'product_item': product_item,
                                    'categories': categories,
                                    'variations': variations
                                });
                            }
                            else {
                                console.log("no father item found in DB");
                                await mysql.logFail('no father item found for ' + sku);
                            }
                        }
                    }

                }
            }
            response.push({
                'items': father_items,
                'variations': variation_items
            });
        }


        return response;
    } catch (error) {
        console.log(error);
    }

}

async function sortItemsAmazon(sage_items, plenty_items, channel, table = "item_mapping_amazon") {
    let possible_new = [];
    let update_items = [];
    for (let index = 0; index < plenty_items.length; index++) {
        /* for (let index = 0; index < 5; index++) { */
        console.log((index + 1) + '/' + plenty_items.length);
        let sku = plenty_items[index].externalId;
        console.log("SKU: " + sku);
        let existsPlenty = false;
        let existsDB = false;
        if (sku) {
            existsDB = await mysql.getItemInDB(table, sku, 'sku');
            existsPlenty = await findPlentyItemBySKU(sku, plenty_items);
            if (existsDB && existsPlenty) {
                console.log("item already exists in db");
                var sage_article = await getSingleArticleFromSage(sku, channel);
                update_items.push(sage_article);
                /* try {
                    await updateItemFromQueue(sku, channel);   
                } catch (error) {
                    console.log(error);
                }
                await delay(); */
            }
            else {
                console.log("not existing, pushing into new items");
                possible_new.push(sku);
            }
        }
        else {
            console.log("no sku " + plenty_items[index].id);
        }
    }
    return {
        'new': possible_new,
        'update': update_items
    };
}

async function sortItems(sage_items, plenty_items, channel, table = "item_mapping", for_amazon = false) {
    console.log("sorting items");
    let possible_new = [];
    let update_items = [];
    console.log("table from sortingItems: " + table);
    for (let index = 0; index < sage_items.GetSkuListByChannelResult.Data.length; index++) {
        //for (let index = 0; index < 10; index++) {
        console.log((index + 1) + '/' + sage_items.GetSkuListByChannelResult.Data.length);
        let sku = sage_items.GetSkuListByChannelResult.Data[index];
        let existsPlenty = false;
        let existsDB = 0;
        console.log(sku);
        //let exists = await checkItemExists(mapping, sku);
        existsDB = await mysql.getItemInDB(table, sku, 'sku');
        //existsPlenty = await findPlentyItemBySKU(sku, plenty_items);
        if (existsDB.length > 0) {
            console.log("item already exists in db");
            var sage_article = await getSingleArticleFromSage(sku, channel);
            update_items.push(sage_article);
            /* try {
                await updateItemFromQueue(sku, channel);   
            } catch (error) {
                console.log(error);
            }
            await delay(); */
        }
        else {
            console.log("not existing, pushing into new items");
            possible_new.push(sku);
        }
    }
    return {
        'new': possible_new,
        'update': update_items
    };
}

async function getDateFromChangedAt(dateString) {
    const timestamp = parseInt(dateString.match(/\/Date\((\d+)[+-](\d+)\)\//)[1], 10);
    return new Date(timestamp);
}

function formatDateToISO(date) {
    const pad = (num) => (num < 10 ? '0' : '') + num;

    const year = date.getFullYear();
    const month = pad(date.getMonth() + 1); // Months are zero-based
    const day = pad(date.getDate());
    const hours = pad(date.getHours());
    const minutes = pad(date.getMinutes());
    const seconds = pad(date.getSeconds());

    return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
}

async function compareLastUpdate_ChangedAt(changed_at, last_update) {
    const sage_item_changed_at = await getDateFromChangedAt(changed_at);
    const last_update_date = new Date(last_update);
    if (sage_item_changed_at.getTime() > last_update_date.getTime()) {
        return true;
    }
    return false;
}

// wip
async function sync_descriptions(channel) {
    try {
        token = await getToken();
        let sku_list = await getAllItemsFromSage(channel);
        /* let sku_list = ["2345-2",
        "3746-3",
        "9807-02-01",
        "6623",
        "6600"]; */
        let sage_items = [];
        sku_list = sku_list.GetSkuListByChannelResult.Data;
        for (let index = 0; index < sku_list.length; index++) {
            //for (let index = 0; index < 10; index++) {
            console.log((index + 1) + '/' + sku_list.length);
            const sku = sku_list[index];
            let existsDB = await mysql.getItemInDB('item_mapping', sku, 'sku');
            if (existsDB.length) {
                let sage_item = await getSingleArticleFromSage(sku, channel);
                if (sage_item.GetPimItemBySkuAndChannelResult.Success && sage_item.GetPimItemBySkuAndChannelResult.Data.ProductItem.ChannelSpecificProductDetails.Enabled) {
                    sage_items.push({
                        'sage': sage_item,
                        'db': existsDB[0]
                    });
                }
            }
        }
        for (let z = 0; z < sage_items.length; z++) {
            console.log((z + 1) + '/' + sage_items.length);
            try {
                let obj = sage_items[z];
                let item = obj.sage;
                let db_item = obj.db;
                let data = item.GetPimItemBySkuAndChannelResult.Data;
                let product_item = data.ProductItem;
                let details = product_item.ChannelSpecificProductDetails;
                let tags = details.Tags;
                let sku = details.Sku;
                console.log("Item mit SKU " + details.Sku + " description wird geupdatet");
                var result = await updateDescription(db_item, product_item);
                console.log(result);
                await delay();
                //await mapTagValueToVariation(sku, false, tags); 



            } catch (error) {
                console.log(error);
            }

        }

    } catch (error) {
        console.log(error);
    }

}


async function cleanupArticle(channel) {
    console.log("in cleanup Article");
    let sage_items = await getAllItemsFromSage(channel);
    token = await getToken();
    let plenty_items = await getAllItemsFromPlenty();
    for (let index = 0; index < plenty_items.length; index++) {
        const element = plenty_items[index];
        let db_item = await mysql.getItemInDB(item_table_name, element.id, 'plenty_id');
        if (db_item.length) {

        }
        else {
            console.log(element.id + " variation id, not in DB");
        }
    }
}

async function updateEAN(sku = null, item_id = null, variation_id = null, EAN = null, token=null, channel=null) {
    if(sku) {
        try {
            token = await getToken();
            let table_name = item_table_name;
            if(channel) {
                table_name = table_name+"_"+channel
            }
            let item = await mysql.getItemInDB(table_name, sku, 'sku');
            let sage_item = await getSingleArticleFromSage(sku, channel);
            let EAN = null;
            if(sage_item) {
                EAN = sage_item.GetPimItemBySkuAndChannelResult.Data.ProductItem.Ean;
            }   
            if(item) {
                await ean_plenty_post(item.plenty_id, item.item_id, EAN, token);
            }
            else {
                console.log("NO ITEM FOUND FOR " + sku);
            }
        } catch (error) {
            console.log(error);
        }
    }
    else {
        await ean_plenty_post(variation_id, item_id, EAN, token);
    }
    
}

async function ean_plenty_post(variation_id, item_id, EAN, token) {
    try {
        var header = {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        }
        let singleUrl = process.env.PLENTY_URL+"/rest/items/"+item_id+"/variations/"+variation_id+"/variation_barcodes";
        let singlePayload = {
            "barcodeId": 1,
            "variationId": variation_id,
            "code": EAN
        }
        await postToPlenty(singleUrl, singlePayload, header);
        await delay();
    } catch (error) {
        console.log(error);
    }
}

async function sms_image_sync() {
    try {
        console.log("in sms image sync");
        let channel_name = "suckmystraw";
        let table_name = "item_mapping_sms";
        let image_mapping_table_name = "image_mapping_sms";
        let mandant_id = sms_plenty_id;
        let sms_db_item = null;
        let sage_sku_list = await getAllItemsFromSage(channel_name)
        for (let index = 0; index < sage_sku_list.GetSkuListByChannelResult.Data.length; index++) {
            console.log((index + 1) + '/' + sage_sku_list.GetSkuListByChannelResult.Data.length);
            let sku = sage_sku_list.GetSkuListByChannelResult.Data[index];
            sms_db_item = await mysql.getItemInDB(table_name, sku, 'sku');
            if(sms_db_item[0]) {
                console.log("handling images for " + sku);
                await createImageFromQueue(sku, channel_name);
            }
        }    
    } catch (error) {
        console.log(error);
    }
    
    //createImageFromQueue(id, channel) { 
}

async function updateArticle(channel) {
    var product_sync_active = true;
    var mandant_id = n24_plenty_id;
    var table_name = item_table_name;
    var pw_for_token = data.password;
    var user_for_token = data.username;
    var url_for_token = process.env.PLENTY_URL;
    try {
        var channel_settings = await getChannelSettings(channel);
    } catch (error) {
        console.log(error);
    }
    product_sync_active = channel_settings.GetChannelSettingsResult.Data.ProductSyncActive;
    mandant_id = channel_settings.GetChannelSettingsResult.Data.PlentyMarketId;
    if(mandant_id == sms_plenty_id) {
        table_name = sms_item_table_name;
        pw_for_token = sms_password;
        user_for_token = sms_username;
        url_for_token = sms_live_url;
    }
    if(mandant_id == plenty_id) {
        table_name = amazon_item_table_name;
    }
    console.log("mandant_id = " + mandant_id);
    console.log("tables_name = " + table_name);
    if(product_sync_active) {
        try {
            let sage_items = await getAllItemsFromSage(channel);
            token = await getToken(pw_for_token, user_for_token, url_for_token);
            let plenty_items = await getAllItemsFromPlenty(mandant_id, url_for_token);
            let update_items = [];
            let new_items = [];
            let new_items_details = [];
            let not_enabled = [];
            console.log("sage items: " + sage_items.GetSkuListByChannelResult.Data.length);
            let itemObj = await sortItems(sage_items, plenty_items, channel, table_name);
            update_items = itemObj.update;
            new_items = itemObj.new;
            console.log("updateable items");
            console.log(update_items.length);
            var article_bulk = await prepareBulkArrayForItemUpdate(update_items, table_name, mandant_id);
            //console.log('items: ' + article_bulk[0]['items'].length);
            //console.log('variations: ' + article_bulk[0]['variations'].length);
            var items = [];
            var variations = [];
            var itemsBulk = [];
            var variationsBulk = [];
            if(article_bulk.length > 0) {
                items = article_bulk[0]['items'];
                variations = article_bulk[0]['variations'];
                itemsBulk = await prepareBulkArray(items, true);
                variationsBulk = await prepareBulkArray(variations, true);
        
                for (let index = 0; index < itemsBulk.length; index++) {
                    const element = itemsBulk[index];
                    console.log(index + " updating items...");
                    var url = url_for_token + "/rest/items"
                    var header = {
                        'Authorization': 'Bearer ' + token,
                        'Content-Type': 'application/json'
                    }
                    try {
                        await makePlentyPriceCallPUT(url, element, header);
                        await delay();
                    } catch (error) {
                        console.log(error);
                    }
        
                }
                for (let index = 0; index < variationsBulk.length; index++) {
                    const element = variationsBulk[index];
                    console.log(index + " updating variations...");
                    var url = url_for_token + "/rest/items/variations"
                    var header = {
                        'Authorization': 'Bearer ' + token,
                        'Content-Type': 'application/json'
                    }
                    try {
                        await makePlentyPriceCallPUT(url, element, header);
                        await delay();
                    } catch (error) {
                        console.log(error);
                    }
                }
            }
            
            console.log("possible new items: " + new_items.length);
            console.log("getting single items from sage...");
            for (let index = 0; index < new_items.length; index++) {
                let element = new_items[index];
                let data = null;
                let product_item = null;
                let cspd = null;
    
                let details = await getSingleArticleFromSage(element, channel);
                if (details.hasOwnProperty('GetPimItemBySkuAndChannelResult')) {
                    if (details.GetPimItemBySkuAndChannelResult.Success) {
                        if (details.GetPimItemBySkuAndChannelResult.hasOwnProperty('Data')) {
                            data = details.GetPimItemBySkuAndChannelResult.Data;
                            if (data !== null) {
                                if (data.hasOwnProperty('ProductItem')) {
                                    product_item = data.ProductItem;
                                    if (product_item !== null) {
                                        if (product_item.hasOwnProperty('ChannelSpecificProductDetails')) {
                                            cspd = product_item.ChannelSpecificProductDetails;
                                            if (cspd !== null) {
                                                if (cspd.Enabled) {
                                                    console.log(cspd.Sku)
                                                    new_items_details.push(details.GetPimItemBySkuAndChannelResult.Data);
                                                }
                                                else {
                                                    not_enabled.push(details.GetPimItemBySkuAndChannelResult.Data.ProductItem.ChannelSpecificProductDetails.Sku);
                                                    var checkExistDisabled = await mysql.getItemInDB(disabled_table_name, element, 'sku');
                                                    if (checkExistDisabled.length < 1) {
                                                        await mysql.insertIntoDisabled(details.GetPimItemBySkuAndChannelResult.Data.ProductItem.ChannelSpecificProductDetails);
                                                    }
                                                }
                                            }
                                            else {
                                                console.log("ChannelSpecificProductDetails is NULL for " + element);
                                                var checkExistDisabled = await mysql.getItemInDB(disabled_table_name, element, 'sku');
                                                if (checkExistDisabled.length < 1) {
                                                    var temp = {
                                                        'Sku': element
                                                    };
                                                    await mysql.insertIntoDisabled(temp);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
    
                        }
                        else {
                            console.log("NO DATA " + element);
                            var checkExistDisabled = await mysql.getItemInDB(disabled_table_name, element, 'sku');
                            if (checkExistDisabled.length < 1) {
                                var temp = {
                                    'Sku': element
                                };
                                await mysql.insertIntoDisabled(temp);
                            }
                        }
                    }
                    else {
                        console.log("NO Success for " + element);
                        var checkExistDisabled = await mysql.getItemInDB(disabled_table_name, element, 'sku');
                        if (checkExistDisabled.length < 1) {
                            var temp = {
                                'Sku': element
                            };
                            await mysql.insertIntoDisabled(temp);
                        }
                    }
                }
                else {
                    console.log("NO GetPimItemBySkuAndChannelResult " + element);
                }
            }
            console.log('handling enabled children now...');
            for (let index = 0; index < new_items_details.length; index++) {
                const element = new_items_details[index];
                let cspd = element.ProductItem.ChannelSpecificProductDetails;
                let father_sku = cspd.FatherSku;
                let father_not_enabled = await checkForVaddern(not_enabled, father_sku);
                if (father_not_enabled) {
                    new_items_details.splice(index, 1);
                    index--;
                }
            }
    
            console.log("new items: " + new_items_details.length);
            for (let index = 0; index < new_items_details.length; index++) {
                const element = new_items_details[index];
                console.log(element.ProductItem.Sku);
                /* console.log("publishing message to create article sku: " + element.ProductItem.Sku);
                publish("", queueName, Buffer.from(JSON.stringify(payload))); */
                await createItemFromQueue(element.ProductItem.Sku, channel, false, mandant_id, table_name, url_for_token, token);
                delay(2000);
            }
        } catch (error) {
            console.log(error);
        }
    }
    
}

async function getImageFromPlentyByItemId(id, api_url = process.env.PLENTY_URL) {
    let url = api_url + '/rest/items/' + id + '/images';
    //console.log("url: "+url);
    var header = {
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
    }
    var options = {
        url: url,
        json: true,
        method: "GET",
        headers: header
    }
    return getRequest(options);
}

//Map Tags with Article
async function mapTagValueToVariation(sku, needToken = true, tags_from_sageitem = null, table_name = item_table_name) {
    try {
        if (needToken) {
            token = await getToken();
        }
        console.log("start");
        var header = {
            'Content-Type': 'application/json; charset=utf8',
            'Authorization': 'Bearer ' + token
        }

        //Get Article Data from DB
        let dbEntry = await mysql.getItemInDB(table_name, sku, 'sku');
        if (dbEntry) {
            let itemId = dbEntry[0].plenty_item_id;
            let variationId = dbEntry[0].plenty_id;
            let singleUrl = process.env.PLENTY_URL + '/rest/v2/properties/relations';
            let tags;

            if (tags_from_sageitem) {
                tags = tags_from_sageitem;
            }
            else {
                //Get all Tags mapped to Article in Sage
                tags = await getTagsFromSku(sku);
            }


            //Delete all Relations before upload new ones
            await deleteAllPropertyRelationsForVariation(variationId);
            //Map every tag to Variation in Plenty
            for (const tag of tags) {
                try {
                    let selectionsToUpload = 0;

                    //Get Tag Data from DB
                    let tagDB = await mysql.getItemInDB(tag_table_name, tag.TagId, 'sage_tag_id');
                    if (!await checkBlacklistedTag(tag.TagId)) {
                        if (tagDB) {
                            let propertyId = tagDB[0].plenty_tag_id;
                            var selectionIds = [];

                            for (const option of tag.SelectedOptions) {
                                if (!option.IsDeleted && option.IsPublic) {
                                    selectionsToUpload = selectionsToUpload + 1
                                    let optionId = option.OptionId;
                                    let tagValueDB = await mysql.getItemInDB(tag_value_table_name, optionId, 'sage_option_id');
                                    if (tagValueDB) {
                                        selectionIds.push(tagValueDB[0].plenty_selection_id);
                                    }
                                }
                            }
                            if (selectionsToUpload > 0) {
                                try {
                                    let singlePayload = {
                                        "propertyId": propertyId,
                                        "type": "item",
                                        "targetId": variationId,
                                        "groupId": n24_property_group_id,
                                        "selectionValues": selectionIds.map(id => ({ selectionId: id })),
                                        "lang": "de",

                                    }
                                    await postToPlenty(singleUrl, singlePayload, header);
                                    await delay();
                                } catch (error) {
                                    console.log(error);
                                }

                            }

                        }

                    }
                } catch (error) {
                    console.log(error);
                }

            }
            console.log("Uploaded / updated all Tags for Plenty Variation Id " + variationId);

        }

    } catch (error) {
        console.log(error);
    }


}

async function deleteAllPropertyRelationsForVariation(variationId) {
    let relations = await getRelationsFromPlenty(variationId);
    relations = JSON.parse(relations);
    relations = relations.entries;

    for (const relation of relations) {
        await deletePropertyRelation(relation.id);
        await delay();
    }

}

async function deletePropertyRelation(relationId) {
    try {
        var header = {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        }
        let url = process.env.PLENTY_URL + '/rest/v2/properties/relations/' + relationId;
        const response = await axios.delete(url, { headers: header });
        //console.log('Request successful. Response:', response.data.id);
        console.log("Delete Relation with ID " + relationId);
        return response.data;
    } catch (error) {
        console.log(error);
        console.error('Error in the request:', error.message);
        throw error;
    }
}

async function getRelationsFromPlenty(variationId) {
    var header = {
        'Content-Type': 'application/json; charset=utf8',
        'Authorization': 'Bearer ' + token
    }
    var options = {
        url: process.env.PLENTY_URL + '/rest/v2/properties/relations?itemsPerPage=50000&targetId= ' + variationId,
        port: 443,
        path: '/rest/v2/properties/relations?itemsPerPage=50000&targetId= ' + variationId,
        method: 'GET',
        headers: header
    }
    return getRequest(options);
}


async function getTagsFromSku(sku) {
    let item = await getSingleArticleFromSage(sku, main_channel);
    return item.GetPimItemBySkuAndChannelResult.Data.ProductItem.ChannelSpecificProductDetails.Tags;
}


async function updateTags(channel) {
    token = await getToken();
    let sageTagTree = await getSageTags();
    let sageTags = sageTagTree.GetTagItemsListResult.Data;
    var url = process.env.PLENTY_URL + '/rest/v2/properties';
    var header = {
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
    }

    for (let i = 0; i < sageTags.length; i++) {
        let isBlacklisted = await checkBlacklistedTag(sageTags[i].Id);
        if (!isBlacklisted) {
            let counter = i + 1
            console.log("Tag Class " + counter + "/" + sageTags.length);
            //for(let i = 0; i < 1; i++){
            let dbEntries = await mysql.getItemInDB(tag_table_name, sageTags[i].Id, 'sage_tag_id');
            let dbEntry = dbEntries[0];

            if (!dbEntry) {
                try {
                    console.log("Tag Class with sage ID " + sageTags[i].Id + " not found in DB. Start Upload");

                    //Upload new Tag if it doesn't exist in DB
                    let payload = {
                        "cast": "multiSelection",
                        "type": "item",
                        "position": i,
                        names: [
                            {
                                "lang": "de",
                                "name": sageTags[i].Name
                            }
                        ],
                        groups: [
                            {
                                "id": 151
                            }
                        ]

                    }
                    let response = await postToPlenty(url, payload, header);

                    let mysqlData = [
                        {
                            sage_tag_id: sageTags[i].Id,
                            plenty_tag_id: response.id,
                            name: sageTags[i].Name
                        }
                    ];
                    await mysql.insertIntoTagMapping(mysqlData);

                    dbEntries = await mysql.getItemInDB(tag_table_name, sageTags[i].Id, 'sage_tag_id');
                    dbEntry = dbEntries[0];

                    console.log("Tag with Sage Id " + sageTags[i].Id + " successfully posted to plenty");

                } catch (error) {
                    console.log(error);
                    console.log(error.data);
                }
            } else {
                console.log("Group with sage ID " + sageTags[i].Id + " found in DB. Start Update");
                //Update Group
                try {
                    let names = await getNameIdByTag(dbEntry.plenty_tag_id);
                    //let names = await getGroupNameIdByGroup(73);

                    //Put updated group names
                    var names_url = process.env.PLENTY_URL + '/rest/v2/properties/names/' + names;
                    var names_payload = {
                        "lang": "de",
                        "name": sageTags[i].Name,
                    };
                    await axios.put(names_url, names_payload, { headers: header });

                    //Put updated group
                    var tag_url = process.env.PLENTY_URL + '/rest/v2/properties/' + dbEntry.plenty_tag_id;
                    var tag_payload = {
                        "position": i
                    };
                    await axios.put(tag_url, tag_payload, { headers: header });

                    //Update tag group mapping

                    let updateData = [
                        {
                            plenty_tag_id: dbEntry.plenty_tag_id,
                            name: sageTags[i].Name
                        }
                    ];
                    await mysql.updateTagMapping(updateData);
                } catch (error) {
                    console.log(error);
                    console.log(error.data);
                }
            }

            //Update or Upload all Tags for Tag Class
            let options = sageTags[i].Options;
            for (let j = 0; j < options.length; j++) {
                let tagCounter = j + 1
                console.log("Tag Value " + tagCounter + "/" + options.length);
                if (!options[j].IsDeleted && options[j].IsPublic) {

                    //Check if Tag exists in DB
                    let valueDBEntries = await mysql.getItemInDB(tag_value_table_name, options[j].OptionId, 'sage_option_id');
                    let valueEntry = valueDBEntries[0];
                    let plainDescription = htmlToText(options[j].Description, { wordwrap: false });

                    if (!valueEntry) {
                        try {
                            console.log("Tag with sage Option ID " + options[j].OptionId + " not found in DB. Start Upload");

                            //Upload new Tag if it doesn't exist in DB
                            var valueUrl = process.env.PLENTY_URL + '/rest/v2/properties/selections';
                            let valuePayload = {
                                "propertyId": dbEntry.plenty_tag_id,
                                "position": j,
                                names: [
                                    {
                                        "lang": "de",
                                        "name": options[j].OptionValue,
                                        "description": plainDescription,
                                    }
                                ]

                            }
                            let valueResponse = await postToPlenty(valueUrl, valuePayload, header);
                            let valueData = [
                                {
                                    sage_option_id: options[j].OptionId,
                                    plenty_selection_id: valueResponse.id,
                                    sage_group_id: sageTags[i].Id,
                                    plenty_tag_id: dbEntry.plenty_tag_id,
                                    name: options[j].OptionValue
                                }
                            ];
                            await mysql.insertIntoTagValueMapping(valueData);
                        } catch (error) {
                            console.log(error);
                            console.log(error.data);
                        }
                    } else {
                        try {
                            console.log("Tag Value with sage Option ID " + options[j].OptionId + " found in DB. Start Update");
                            //Update tag
                            let valueNameId = await getSelectionNameIdBySelection(valueEntry.plenty_selection_id);

                            //Put updated tag names
                            var selection_names_url = process.env.PLENTY_URL + '/rest/v2/properties/selections/names/' + valueNameId;
                            var selection_names_payload = {
                                "selectionId": valueEntry.plenty_selection_id,
                                "name": options[j].OptionValue,
                                "description": plainDescription,
                            };
                            await axios.put(selection_names_url, selection_names_payload, { headers: header });
                            //Put updated tag

                            var selection_url = process.env.PLENTY_URL + '/rest/v2/properties/selections/' + valueEntry.plenty_selection_id;
                            var selection_payload = {
                                "position": j
                            };
                            await axios.put(selection_url, selection_payload, { headers: header });
                            //Update tag group mapping

                            let tagUpdateData = [
                                {
                                    plenty_selection_id: valueEntry.plenty_selection_id,
                                    name: options[j].OptionValue
                                }
                            ];
                            await mysql.updateTagValueMapping(tagUpdateData);
                        } catch (error) {
                            console.log(error);
                            console.log(error.data);
                        }

                    }
                    await delay(1500);
                } else {
                    console.log("Tag Value " + tagCounter + "/" + options.length + " is disabled or not public");
                }
            }
        }
    }

    await checkForDeletedTags();
    await checkForDeletedTagValues();
    console.log("Finished Tag Update Task");
}

async function checkForDeletedTags() {
    console.log("Checking for deleted Tags...");
    // Prepare some arrays
    var plenty_tags = null;
    var tagClassesInSage = [];
    var tagsInPlenty = [];
    var tagDelta = [];

    console.log("Get Plenty tags");
    // Get all Plenty Tag for channel
    plenty_tags = await getN24TagsFromPlenty();
    plenty_tags = JSON.parse(plenty_tags);
    plenty_tags = plenty_tags.entries;

    //Extract Tag IDs from Plenty
    for (const tag of plenty_tags) {
        tagsInPlenty.push(tag.id);
    }

    console.log("Get Sage tags");
    //Get Tag Class IDs and mapped plenty tag ID from sage and DB
    let sageTagTree = await getSageTags();
    let sageTags = sageTagTree.GetTagItemsListResult.Data;
    for (let i = 0; i < sageTags.length; i++) {
        let blacklistedTag = await checkBlacklistedTag(sageTags[i].Id);
        if (!blacklistedTag) {
            let dbTags = await getItemInDB(tag_table_name, sageTags[i].Id, 'sage_tag_id');
            if (dbTags) {
                tagClassesInSage.push(dbTags[0].plenty_tag_id);
            }
        }
    }

    console.log("Get difference between sage and plenty");
    // Get differences between both Plenty and Sage
    tagDelta = tagsInPlenty.filter(tags => !tagClassesInSage.includes(tags));
    // Delete Tag Values and Tags from Plenty and DB
    for (let j = 0; j < tagDelta.length; j++) {
        await deleteValuesFromTag(tagDelta[j]);
        await deleteTag(tagDelta[j]);
        await mysql.deleteItemFromDB(tag_table_name, tagDelta[j], 'plenty_tag_id');
        console.log("Tag with Plenty Tag ID " + tagDelta[j] + " and all mapped values deleted");
    }


}
async function checkForDeletedTagValues() {
    console.log("Checking for deleted Tag Values...");
    var tagValuesInSage = [];
    let tagValues = await mysql.getAllItemsInDB(tag_value_table_name);

    let sageTagTree = await getSageTags();
    let sageTags = sageTagTree.GetTagItemsListResult.Data;
    for (let i = 0; i < sageTags.length; i++) {
        let blacklistedTag = await checkBlacklistedTag(sageTags[i].Id);
        if (!blacklistedTag) {
            let options = sageTags[i].Options
            for (let j = 0; j < options.length; j++) {
                tagValuesInSage.push(options[j]);
            }
        }
    }
    const deletedValues = [];

    tagValues.forEach(tag => {
        const correspondingSageTag = tagValuesInSage.find(sageTag => sageTag.OptionId === tag.sage_option_id);

        if (correspondingSageTag && (correspondingSageTag.IsDeleted || !correspondingSageTag.IsPublic)) {
            deletedValues.push(tag.sage_option_id);
        }
    });

    for (const deletedId of deletedValues) {
        let dbEntry = await mysql.getItemInDB(tag_value_table_name, deletedId, 'sage_option_id');
        let plentySelectionId = dbEntry[0].plenty_selection_id;
        await deleteTagValue(plentySelectionId);
        await mysql.deleteItemFromDB(tag_value_table_name, deletedId, 'sage_option_id');
        console.log("Tag Value with Sage Option ID " + deletedId + " and plenty slection Id " + plentySelectionId + " was deleted");
    }
}

//Delete all Tag Values assigned to one tag
async function deleteValuesFromTag(plentyTagId) {
    console.log("Deleting all Tag Values from tag " + plentyTagId);
    let selections = await getSelectionsFromPlenty(plentyTagId);
    for (const entry of selections.entries) {
        await deleteTagValue(entry.id);
        await mysql.deleteItemFromDB(tag_value_table_name, entry.id, 'plenty_selection_id');

    }

}

async function deleteTag(propertyId) {
    try {
        var header = {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        }
        let url = process.env.PLENTY_URL + '/rest/v2/properties/' + propertyId;
        const response = await axios.delete(url, { headers: header });
        //console.log('Request successful. Response:', response.data.id);
        console.log("Delete Selection with ID " + propertyId);
        return response.data;
    } catch (error) {
        console.log(error);
        console.error('Error in the request:', error.message);
        throw error;
    }
}

async function checkBlacklistedTag(tagId) {
    let blacklist = [66, 74, 64, 6, 62, 7, 88, 92, 79, 95, 90, 31, 93, 94];
    //check if tagId ist contained in blacklist
    return blacklist.includes(tagId);
}

async function getNameIdByTag(tagId) {
    let url = process.env.PLENTY_URL + '/rest/v2/properties/' + tagId + '/names';
    //console.log("url: "+url);
    var header = {
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
    }
    var options = {
        url: url,
        json: true,
        method: "GET",
        headers: header
    }

    let names = await getRequest(options)
    let filteredNameId = names.filter(item => item.lang === 'de');

    return filteredNameId[0].id;
}

async function getSelectionNameIdBySelection(selectionId) {
    let url = process.env.PLENTY_URL + '/rest/v2/properties/selections/' + selectionId + '/names';
    //console.log("url: "+url);
    var header = {
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
    }
    var options = {
        url: url,
        json: true,
        method: "GET",
        headers: header
    }

    let names = await getRequest(options)
    let filteredNameId = names.filter(item => item.lang === 'de');

    return filteredNameId[0].id;
}


async function getSageTags() {
    let url = sage_url + "/TagItems";
    let header = {
        'Content-Type': 'application/json; charset=utf8'
    };
    var options = {
        url: url,
        json: true,
        method: "GET",
        headers: header
    }
    console.log("Getting Tags from sage...");
    return await getRequest(options);
}

async function getItemFromPlenty(id, api_url = process.env.PLENTY_URL) {
    let url = api_url + '/rest/items/' + id;
    //console.log("url: "+url);
    var header = {
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
    }
    var options = {
        url: url,
        json: true,
        method: "GET",
        headers: header
    }
    return getRequest(options);

}

async function getSelectionsFromPlenty(id) {
    let url = process.env.PLENTY_URL + '/rest/v2/properties/' + id + '/selections';
    //console.log("url: "+url);
    var header = {
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
    }
    var options = {
        url: url,
        json: true,
        method: "GET",
        headers: header
    }
    return getRequest(options);
}
async function deleteTagValue(selectionId) {
    try {
        var header = {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        }
        let url = process.env.PLENTY_URL + '/rest/v2/properties/selections/' + selectionId;
        const response = await axios.delete(url, { headers: header });
        //console.log('Request successful. Response:', response.data.id);
        console.log("Delete Selection with ID " + selectionId);
        return response.data;
    } catch (error) {
        console.log(error);
        console.error('Error in the request:', error.message);
        throw error;
    }

}

async function getSelectionsFromPlenty(id) {
    let url = process.env.PLENTY_URL + '/rest/v2/properties/' + id + '/selections';
    //console.log("url: "+url);
    var header = {
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
    }
    var options = {
        url: url,
        json: true,
        method: "GET",
        headers: header
    }
    return getRequest(options);
}
async function deleteTagValue(selectionId) {
    try {
        var header = {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        }
        let url = process.env.PLENTY_URL + '/rest/v2/properties/selections/' + selectionId;
        const response = await axios.delete(url, { headers: header });
        //console.log('Request successful. Response:', response.data.id);
        console.log("Delete Selection with ID " + selectionId);
        return response.data;
    } catch (error) {
        console.log(error);
        console.error('Error in the request:', error.message);
        throw error;
    }

}

async function test_func() {
    try {
        await getAllItemsFromPlentymarkets();
    } catch (error) {
        console.log(error);
    }
}



async function deleteAllPlentyImages(sku) {
    let item = await mysql.getItemInDB(item_table_name, sku, 'sku');
    let images = await getAllImagesFromPlenty(item[0].plenty_item_id);
    for (let i = 0; i < images.length; i++) {
        await deleteImageFromPlenty(item[0].plenty_item_id, images[i].id);
        await sleep(1000)
    }
    await sleep(500);
    console.log("Deleted all images for " + item[0].plenty_item_id);
}


async function createImageFromQueueNoGetToken(id, channel) {
    const sku = id;
    //let existsDB = await mysql.getItemInDB(item_table_name, sku, 'sku');
    let item;
    let product_item;
    let mapping = await mysql.getItemInDB(item_table_name, sku, 'sku');
    let plentyItemId = mapping[0].plenty_item_id;
    let plentyId = mapping[0].plenty_id;
    let imageUrls;
    let itemExistsPlenty = await getItemFromPlenty(plentyItemId);
    if (!itemExistsPlenty.error) {
        //Get Data from Sage Enpoint
        item = await getSingleArticleFromSage(sku, channel);
        if (item) {

            //Get all Image Urls for Product
            product_item = item.GetPimItemBySkuAndChannelResult.Data.ProductItem;
            imageUrls = product_item.ChannelSpecificProductDetails.ImageURLs;
            //Start Upload for all Image Urls
            for (let i = 0; i < imageUrls.length; i++) {
                let counter = i + 1
                console.log("Upload Image " + counter + "/" + imageUrls.length);
                //Prepare Image Parameters
                let imageUrl = imageUrls[i];
                let imagePosition = i;
                let imageFileName = await extractImageNameFromUrl(imageUrl);



                //Check if Image was already uploaded to DB
                let existDB = await mysql.getImageInDB(image_table_name, 'sage_image_id', 'sku', imageFileName, sku);

                let existPlenty = await getImageFromPlentyByItemId(plentyItemId);
                // check if image exists for specific plenty_item
                if (existDB.length && existPlenty.length) {
                    let imageId = existDB[0].plenty_image_id;

                    //Gett all currently variations mapped to image
                    let variations = await getImageVariations(plentyItemId, existDB[0].plenty_image_id);
                    const variationIds = variations.map(variation => variation.variationId);
                    if (!variationIds.includes(plentyId)) {
                        variationIds.push(plentyId);
                    }

                    //Delete old Image from Plenty
                    await deleteImageFromPlenty(plentyItemId, imageId);

                    //Delete entry from db
                    await mysql.deleteItemFromDB(image_table_name, imageFileName, 'sage_image_id');

                    //Upload new Image to Plenty
                    await uploadNewImage(imageUrl, product_item, plentyItemId, plentyId, imagePosition, "update", sku);

                    //Map Image to all given variations
                    let dbUpdate = await mysql.getImageInDB(image_table_name, 'sage_image_id', 'sku', imageFileName, sku);
                    let newImageId = dbUpdate[0].plenty_image_id;


                    //Map uploaded Images to all variations
                    for (let j = 0; j < variationIds.length; j++) {
                        await mapImageToVariation(plentyItemId, variationIds[j], newImageId);

                        //Prevent short period time limit reach
                        await sleep(1500);
                    }

                } else {
                    //Upload a new Image to Plenty
                    await uploadNewImage(imageUrl, product_item, plentyItemId, plentyId, imagePosition, "new", sku);

                    //Prevent short period time limit reach
                    await sleep(1500);
                }

            }
            //Check if there are old orphaned images in Plenty that are delete in Sage
            await checkForDeletedImages(plentyId, plentyItemId, imageUrls, sku);
            console.log("Updated all Images for SKU " + sku);
        }
    } else {
        console.log("Der Artikel mit der ItemID " + plentyItemId + "ist nicht mehr in Plenty vorhanden und wird aus der Datenbank gelöscht!");
        await mysql.deleteItemFromDB(item_table_name, sku, 'sku');
    }
}

//Create new Images for a given Item (id) and channel
async function createImageFromQueue(id, channel, token = false, api_url = process.env.PLENTY_URL) {
    let table_name = item_table_name;
    let mandant_id = n24_plenty_id;
    let image_mapping_table_name = image_table_name;
    if (channel == "amazon") {
        table_name = "item_mapping_amazon";
        image_mapping_table_name = "image_mapping_amazon";
        mandant_id = plenty_id;
    }
    if (channel == "suckmystraw") {
        table_name = sms_item_table_name;
        image_mapping_table_name = image_table_name_sms_live;
        mandant_id = sms_plenty_id;
    }
    try {
        const sku = id;
        //let existsDB = await mysql.getItemInDB(item_table_name, sku, 'sku');
        let item;
        let product_item;
        let mapping = await mysql.getItemInDB(table_name, sku, 'sku');
        if (mapping.length) {
            let plentyItemId = mapping[0].plenty_item_id;
            let plentyId = mapping[0].plenty_id;
            let imageUrls;
            token = await getToken(data.password, data.username, api_url, token);
            let itemExistsPlenty = await getItemFromPlenty(plentyItemId, api_url);
            if (!itemExistsPlenty.error) {
                //Get Data from Sage Enpoint
                item = await getSingleArticleFromSage(sku, channel);
                if (item) {

                    //Get all Image Urls for Product
                    product_item = item.GetPimItemBySkuAndChannelResult.Data.ProductItem;
                    imageUrls = product_item.ChannelSpecificProductDetails.ImageURLs;
                    //Start Upload for all Image Urls
                    for (let i = 0; i < imageUrls.length; i++) {
                        //Prepare Image Parameters
                        let imageUrl = imageUrls[i];
                        let imagePosition = i;
                        let imageFileName = await extractImageNameFromUrl(imageUrl);



                        //Check if Image was already uploaded to DB
                        let existDB = await mysql.getImageInDB(image_mapping_table_name, 'sage_image_id', 'sku', imageFileName, sku);

                        let imageExistPlenty = await getImageFromPlentyByItemId(plentyItemId, url);
                        if (existDB.length && !imageExistPlenty.error) {
                            let imageId = existDB[0].plenty_image_id;

                            //Gett all currently variations mapped to image
                            let variations = await getImageVariations(plentyItemId, existDB[0].plenty_image_id, url);
                            const variationIds = variations.map(variation => variation.variationId);
                            if (!variationIds.includes(plentyId)) {
                                variationIds.push(plentyId);
                            }
                            await getAllImagesFromPlenty(plentyItemId, url)
                            //Delete old Image from Plenty
                            await deleteImageFromPlenty(plentyItemId, imageId, url);

                            //Delete entry from db
                            await mysql.deleteImageFromDB(image_mapping_table_name, 'sage_image_id', 'sku', imageFileName, sku);

                            //Upload new Image to Plenty
                            await uploadNewImage(imageUrl, product_item, plentyItemId, plentyId, imagePosition, "update", sku, mandant_id, image_mapping_table_name, url, token);

                            //Prevent short period time limit reach
                            await sleep(1500);

                            //Map Image to all given variations
                            let dbUpdate = await mysql.getImageInDB(image_mapping_table_name, 'sage_image_id', 'sku', imageFileName, sku);
                            let newImageId = dbUpdate[0].plenty_image_id;


                            //Map uploaded Images to all variations
                            for (let j = 0; j < variationIds.length; j++) {
                                await mapImageToVariation(plentyItemId, variationIds[j], newImageId, url);

                                //Prevent short period time limit reach
                                await sleep(1500);
                            }

                        } else {
                            //Upload a new Image to Plenty
                            await uploadNewImage(imageUrl, product_item, plentyItemId, plentyId, imagePosition, "new", sku, mandant_id, image_mapping_table_name, token);

                            //Prevent short period time limit reach
                            await sleep(1500);
                        }

                    }
                    //Check if there are old orphaned images in Plenty that are delete in Sage
                    await checkForDeletedImages(plentyId, plentyItemId, imageUrls, sku, url);
                    console.log("Updated all Images for SKU " + sku);
                }
            } else {
                console.log("Der Artikel mit der ItemID " + plentyItemId + "ist nicht mehr in Plenty vorhanden und wird aus der Datenbank gelöscht!");
                await mysql.deleteItemFromDB(item_table_name, sku, 'sku');
            }
        }
        else {
            console.log("Artikel existiert nicht in DB");
        }
    } catch (error) {
        console.log(error)
    }

}


//Function to upload an image to Plenty
async function uploadNewImage(imageUrl, product_item, plentyItemId, plentyId, position, uploadType, sku, mandant = n24_plenty_id, table = 'image_mapping', api_url = process.env.PLENTY_URL, token) {
    try {
        //Prepare further Image Parameters
        let imageFileName = await extractImageNameFromUrl(imageUrl);
        let ending = ".jpg";
        let fullName = imageFileName + ending;
        let imageSuffix = "/img.jpg";
        let completeUrl = imageUrl + imageSuffix;
        let base64Image = await imageUrlToBase64(completeUrl);
        let imageName = product_item.ChannelSpecificProductDetails.Description1;




        //Build Url, Header and Payload for API call
        var url = api_url + '/rest/items/' + plentyItemId + '/images/upload';
        var header = {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        }
        var simple_payload = {
            "itemId": plentyItemId,
            "position": position,
            "fileType": "JPG",
            "uploadFileName": fullName,
            "uploadImageData": base64Image,

            names: [
                {
                    "imageId": imageFileName,
                    "lang": "de",
                    "name": imageName,
                    "alternate": imageName
                }
            ],
            "lang": "de",
            "name": imageName,
            "availabilities": [
                {
                    "imageId": imageFileName,
                    "type": "mandant",
                    "value": mandant
                }
            ],
        }
        console.log("URL: " + url);
        console.log("header: " + header);
        console.log("payload: " + simple_payload);
        //Send images to Plenty and save Plenty Image ID from Response
        let data = await postToPlenty(url, simple_payload, header);
        await delay();
        let imageId = data.id;


        //Prepare Data for MYSQL Database and insert
        let mysqlData = [
            {
                sage_image_id: imageFileName,
                plenty_image_id: imageId,
                sage_sku: sku
            }
        ];

        //Check if it's a new entry to the database or if an existing entry should be updated
        switch (uploadType) {
            case 'new':
                await mysql.insertIntoImageMapping(mysqlData[0], table);
                //Map Images from Main Item to Variation Items on new upload
                await mapImageToVariation(plentyItemId, plentyId, imageId, api_url);
                break;
            case 'update':
                await mysql.insertIntoImageMapping(mysqlData[0], table);
                break;
        }
        console.log("New Image " + imageId + " for SKU " + sku + " uploaded");
    } catch (error) {
        console.log(error);
    }
}

//Map uploaded Image to Variation
async function mapImageToVariation(fatherId, variationId, imageId, api_url = process.env.PLENTY_URL) {
    var url = api_url + '/rest/items/' + fatherId + '/variations/' + variationId + '/variation_images';
    var header = {
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
    }
    var simple_payload = {
        "imageId": imageId,
        "itemId": fatherId,
        "variationId": variationId
    }
    await postToPlenty(url, simple_payload, header);
    await delay();
}

//Delete an Image from Plenty
async function deleteImageFromPlenty(itemId, imageId, api_url = process.env.PLENTY_URL) {
    try {
        var header = {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        }
        let url = api_url + '/rest/items/' + itemId + '/images/' + imageId;
        const response = await axios.delete(url, { headers: header });
        //console.log('Request successful. Response:', response.data.id);
        console.log("Delete Image with ID " + imageId);
        return response.data;
    } catch (error) {
        console.log(error);
        //console.error('Error in the request:', error.message);
        // throw error;
    }
}

async function getInactiveItemsFromPlenty(page = 1) {
    var url = process.env.PLENTY_URL + "/rest/items/variations?page=" + page + "&isActive=false";
    try {
        var header = {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        }
        const response = await axios.get(url, { headers: header });
        //console.log('Request successful. Response:', response.data.id);
        return response.data;
    }
    catch (error) {
        console.log(error);
        //console.error('Error in the request:', error.message);
        //throw error;
    }
}

async function getItemsFromPlenty(page = 1, plenty_id = process.env.N24_PLENTY_ID, api_url = process.env.PLENTY_URL) {
    var url = api_url + "/rest/items/variations?page=" + page + "&plentyId=" + plenty_id;
    if(!plenty_id) {
        url = api_url + "/rest/items/variations?page=" + page;
    }
    try {
        var header = {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        }
        const response = await axios.get(url, { headers: header });
        //console.log('Request successful. Response:', response.data.id);
        return response.data;
    }
    catch (error) {
        console.log(error);
        //console.error('Error in the request:', error.message);
        //throw error;
    }
}

async function getAllImagesFromPlenty(itemId, api_url = process.env.PLENTY_URL) {
    try {
        var header = {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        }
        let url = api_url + '/rest/items/' + itemId + '/images';
        const response = await axios.get(url, { headers: header });
        //console.log('Request successful. Response:', response.data.id);
        return response.data;
    } catch (error) {
        console.log(error);
        //console.error('Error in the request:', error.message);
        //throw error;
    }
}


//Check if there are old orphaned images in Plenty that are delete in Sage
async function checkForDeletedImages(variationId, itemId, imageUrls, sku, url_plenty = process.env.PLENTY_URL) {

    try {
        let variationImages = await getVariationImages(itemId, variationId, url_plenty);
        let mappedImages = [];
        let sageImages = []
        //Get Images from Plenty
        for (let i = 0; i < variationImages.length; i++) {
            let image = variationImages[i];
            let imageDB = await mysql.getImageInDB(image_table_name, 'plenty_image_id', 'sku', image.id, sku);
            let sageImageId = imageDB[0].sage_image_id;
            sageImageId = sageImageId.toString();
            mappedImages.push(sageImageId);
        }
        //Get Images from Sage
        for (let j = 0; j < imageUrls.length; j++) {
            let url = imageUrls[j];
            let imageId = await extractImageNameFromUrl(url);
            //imageId to string
            imageId = imageId.toString();
            sageImages.push(imageId);
        }
        //Compare Images From Sage and Images from Plenty and spot differences
        const imagesNotInSage = mappedImages.filter(image => !sageImages.includes(image));

        //Unmap Image and Plenty Variation
        for (let l = 0; l < imagesNotInSage.length; l++) {
            let imageDB = await mysql.getImageInDB(image_table_name, 'sage_image_id', 'sku', imagesNotInSage[l], sku);
            let plentyImageId = imageDB[0].plenty_image_id;
            await deleteImageMappingPlenty(itemId, plentyImageId, variationId, url_plenty);
            console.log("Unmapped image with SageImageId " + imagesNotInSage[l]);
            //Check if there are any other variations mapped to that image and delete image if there are none
            let updatedVariations = await getImageVariations(itemId, plentyImageId, url_plenty);
            if (updatedVariations.length === 0) {
                await deleteImageFromPlenty(itemId, plentyImageId, url_plenty);
                await mysql.deleteImageFromDB(image_table_name, 'plenty_image_id', 'sku', plentyImageId, sku);
                console.log("Deleted orphaned Image with SageImageId: " + imagesNotInSage[l]);

            }
        }
    } catch (error) {
        console.log("checkForDeletedImages for Variation " + variationId + " of Item " + itemId + " failed. Continue without check");
    }
}

//Unmap Image and Variation
async function deleteImageMappingPlenty(itemId, imageId, variationId, api_url = process.env.PLENTY_URL) {
    try {
        var header = {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        }
        let url = api_url + '/rest/items/' + itemId + '/variations/' + variationId + '/variation_images/' + imageId;
        const response = await axios.delete(url, { headers: header });
        //console.log('Request successful. Response:', response.data.id);
        return response.data;
    } catch (error) {
        console.log(error);
        //console.error('Error in the request:', error.message);
        //throw error;
    }
}

//get all Images mapped to one variation

async function getVariationImages(itemId, variationId, api_url = process.env.PLENTY_URL) {
    try {
        var header = {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        }
        let url = api_url + '/rest/items/' + itemId + '/variations/' + variationId + '/images';
        const response = await axios.get(url, { headers: header });
        //console.log('Request successful. Response:', response.data);
        return response.data;
    } catch (error) {
        console.log(error);
        //console.error('Error in the request:', error.message);
        // throw error;
    }
}

//Get all Variations mapped to one Image
async function getImageVariations(itemId, imageId, api_url = process.env.PLENTY_URL) {
    try {
        var header = {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        }
        let url = api_url + '/rest/items/' + itemId + '/images/' + imageId + '/variation_images';
        const response = await axios.get(url, { headers: header });
        //console.log('Request successful. Response:', response.data);
        return response.data;
    } catch (error) {
        console.log(error);
        //console.error('Error in the request:', error.message);
        //throw error;
    }
}

//Send data to Plenty Rest API
async function postToPlenty(url, payload, header) {
    try {
        const response = await axios.post(url, payload, { headers: header });
        //console.log('Request successful. Response:', response.data.id);
        return response.data;
    } catch (error) {
        console.log(error);
        //console.error('Error in the request:', error.message);
        //throw error;
    }
}


//Delay for x milliseconds
function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}

//Regex to get Image Filename from Url
async function extractImageNameFromUrl(url) {
    // Regex-Pattern für die Extraktion des Bildnamens
    const pattern = /\/product\/original\/([^/]+)$/;

    // Regex-Match auf die URL anwenden, um den Bildnamen zu extrahieren
    const match = url.match(pattern);

    if (match && match[1]) {
        // Das zweite Element in der Match-Gruppe ist der Bildname
        return match[1];
    } else {
        // Rückgabe eines Standardwerts oder einer Fehlermeldung, wenn kein Bildname gefunden wurde
        return null; // oder throw new Error('Bildname konnte nicht extrahiert werden.');
    }
}

//Convert Image from URL to Base64 String
async function imageUrlToBase64(url) {
    try {
        const response = await axios.get(url, {
            responseType: 'arraybuffer' // Response als ArrayBuffer erhalten
        });

        if (response.status === 200) {
            const base64Image = Buffer.from(response.data, 'binary').toString('base64');
            return base64Image;
        } else {
            throw new Error('Fehler beim Herunterladen des Bildes');
        }
    } catch (error) {
        console.error('Fehler:', error.message);
        return null;
    }
}

async function checkForVaddern(array, sku) {
    for (let index = 0; index < array.length; index++) {
        const father_sku = array[index];
        if (sku == father_sku) {
            return true;
        }
    }
    return false;
}

async function handleLongtext(item) {
    try {
        console.log("handling longtext");
        let tags = item.ChannelSpecificProductDetails.Tags;
        let longtext = item.ChannelSpecificProductDetails.Longtext;
        for (let index = 0; index < tags.length; index++) {
            const element = tags[index];
            // id 1 = Waschkennzeichen, soll mit in description
            /* if(element.TagId == 1) {
                let selectedOptions = element.SelectedOptions;
                for (let a = 0; a < selectedOptions.length; a++) {
                    let sel_opt = selectedOptions[a];
                    if(!sel_opt.IsDeleted && sel_opt.IsPublic) {
                        let opt_val = '';
                        let descr = '';
                        let text_to_add = '';
                        opt_val = sel_opt.OptionValue;
                        descr = sel_opt.Description;
                        text_to_add = opt_val + ' </br> ' + descr;
                        longtext += text_to_add;
                    }
                }
            } */
        }
        return longtext;
    } catch (error) {
        console.log(error);
    }

}

async function updateDescription(item_db, item) {
    try {
        console.log("updating description");
        var url = process.env.PLENTY_URL + '/rest/items/' + item_db.plenty_item_id + '/variations/' + item_db.plenty_id + '/descriptions/de';
        var longtext = await handleLongtext(item);
        var keywords = '';
        var header = {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        }
        for (let index = 0; index < item.ChannelSpecificProductDetails.Keywords.length; index++) {
            const element = item.ChannelSpecificProductDetails.Keywords[index];
            keywords += element + ', ';
        }
        var simple_payload = {
            id: item_db.plenty_item_id,
            lang: "de",
            name1: item.ProductName,
            name2: item.Sku,
            name3: item.ChannelSpecificProductDetails.Description2,
            shortDescription: item.ChannelSpecificProductDetails.Shorttext,
            description: longtext,
            technicalData: item.ChannelSpecificProductDetails.Textilkennzeichnung,
            keywords: keywords,
        };
    } catch (error) {
        console.log(error);
    }
    try {
        //console.log("update description, payload: ");
        //console.log(simple_payload);
        fs.appendFile("test.txt", JSON.stringify(simple_payload), function (err) {
            if (err) {
                return console.log(err);
            }
            console.log("The file was saved!");
        });
        const response = await axios.put(url, simple_payload, { headers: header });
        await mysql.updateTimestamp(item_table_name, item.Sku, 'sku');
        return true;
    } catch (error) {
        //console.error('Error in the request:', error.message);
        console.error(error.response);
        mysql.logFail(error.response + ' ' + item.Sku)
        throw error;
    }
}

async function updateVariation(item, categories, packageItem, attributes, item_db, plenty_id_for_update = n24_plenty_id) {
    console.log("updating variation");
    var url = process.env.PLENTY_URL + '/rest/items/' + item_db.plenty_item_id + '/variations/' + item_db.plenty_id;
    var keywords = '';
    var weight = 0;
    var width = 0;
    var length = 0;
    var height = 0;
    var mayShowUnitPrice = false;
    if (item.BasePriceFactor != 1.0000) {
        mayShowUnitPrice = true;
        console.log(item.Sku + " mayshowunitprice = true");
    }
    if (packageItem) {
        if ('Weight' in packageItem) {
            weight = packageItem.Weight;
        }
        if ('Width' in packageItem) {
            width = packageItem.Width;
        }
        if ('Length' in packageItem) {
            length = packageItem.Length;
        }
        if ('Height' in packageItem) {
            height = packageItem.Height;
        }
    }
    for (let index = 0; index < item.ChannelSpecificProductDetails.Keywords.length; index++) {
        const element = item.ChannelSpecificProductDetails.Keywords[index];
        keywords += element + ', ';
    }
    var header = {
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
    }
    var simple_payload = {
        isActive: item.ChannelSpecificProductDetails.Enabled,
        name: item.VariantName,
        externalId: item.Sku,
        variationClients: [{
            variationId: item_db.plenty_id,
            plentyId: plenty_id_for_update
        }],
        isAvailableIfNetStockIsPositive: true,
        isInvisibleInListIfNetStockIsNotPositive: true,
        isVisibleInListIfNetStockIsPositive: true,
        mayShowUnitPrice: mayShowUnitPrice,
        variationCategories: categories,
        variationAttributeValues: attributes
    };
    try {
        // console.log(simple_payload);
        const response = await axios.put(url, simple_payload, { headers: header });
        await mysql.updateTimestamp(item_table_name, item.Sku, 'sku');
        return response.data;
    } catch (error) {
        //console.error('Error in the request:', error.message);
        console.error(error.response);
        //mysql.logFail(error.response.statusText + ' ' +item.Sku)
        throw error;
    }
}

async function updateVariationItem(item, categories, packageItem, attributes, item_db, plenty_id_for_update = n24_plenty_id, table_name = item_table_name) {
    console.log("updating variation");
    try {
        var desc_resp = await updateDescription(item_db, item);
        await delay();
        await mapTagValueToVariation(item.Sku, false, item.ChannelSpecificProductDetails.Tags, table_name);
    } catch (error) {
        console.log(error)
    }
    try {
        var vari_resp = await updateVariation(item, categories, packageItem, attributes, item_db, plenty_id_for_update);
        await delay();
    } catch (error) {
        console.log(error);
    }
    try {
        await updateEAN(null, item_db.plenty_item_id, item_db.plenty_id, item.Ean, token);
        await delay();
    } catch (error) {
        console.log(error);
    }
    //console.log(vari_resp);
    return vari_resp;
}

async function updateMainItem(item, category_ids, packageItem, attributes, item_db, plenty_id_for_updatemain = n24_plenty_id, table_name = item_table_name) {
    console.log("updating article");
    try {
        var desc_resp = await updateDescription(item_db, item);
        await delay();
        await mapTagValueToVariation(item.Sku, false, item.ChannelSpecificProductDetails.Tags, table_name);
    } catch (error) {
        console.log(error)
    }
    try {
        var vari_resp = await updateVariation(item, category_ids, packageItem, attributes, item_db, plenty_id_for_updatemain);
        await delay();
    } catch (error) {
        console.log(error);
    }
    try {
        await updateEAN(null, item_db.plenty_item_id, item_db.plenty_id, item.Ean, token);
        await delay();
    } catch (error) {
        
    }
    var url = process.env.PLENTY_URL + '/rest/items/' + item_db.plenty_item_id;
    var keywords = '';
    var weight = 0;
    var width = 0;
    var length = 0;
    var height = 0;
    if (packageItem) {
        if ('Weight' in packageItem) {
            weight = packageItem.Weight;
        }
        if ('Width' in packageItem) {
            width = packageItem.Width;
        }
        if ('Length' in packageItem) {
            length = packageItem.Length;
        }
        if ('Height' in packageItem) {
            height = packageItem.Height;
        }
    }
    for (let index = 0; index < item.ChannelSpecificProductDetails.Keywords.length; index++) {
        const element = item.ChannelSpecificProductDetails.Keywords[index];
        keywords += element + ', ';
    }
    var header = {
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
    }
    var simple_payload = {
        weightG: weight,
        weightNetG: weight,
        widthMM: width,
        lengthMM: length,
        heightMM: height,
        isActive: item.ChannelSpecificProductDetails.Enabled,
        variationClients: [{
            variationId: item_db.plenty_item_id,
            plentyId: plenty_id_for_updatemain
        }],
        variations: [
            {
                isActive: true,
                isMain: true,
                name: item.VariantName,
                externalId: item.Sku,
                variationCategories: category_ids,
                unit: {
                    unitId: 1,
                    content: 1
                },
                variationAttributeValues: attributes
            }
        ],
        isAvailableIfNetStockIsPositive: true,
        isInvisibleInListIfNetStockIsNotPositive: true,
        isVisibleInListIfNetStockIsPositive: true,
        variationAttributeValues: attributes
    };
    //await exportData(simple_payload, "init_article_log");
    try {
        console.log(simple_payload);
        const response = await axios.put(url, simple_payload, { headers: header });
        await delay();
        //console.log('Request successful. Response:', response.data);
        return response.data;
    } catch (error) {
        //console.error('Error in the request:', error.message);
        console.error(error.response);
        // mysql.logFail(error.response.statusText + ' ' +item.Sku)
        throw error;
    }
}

async function createMainItem(item, category_ids, packageItem, attributes, mandanten_id = n24_plenty_id, api_url = process.env.PLENTY_URL) {
    console.log("creating father article");
    var url = api_url + '/rest/items';
    var keywords = '';
    var weight = 0;
    var width = 0;
    var length = 0;
    var height = 0;
    var mayShowUnitPrice = false;
    if (item.BasePriceFactor != 1.0000) {
        mayShowUnitPrice = true;
        console.log(item.Sku + " mayshowunitprice = true");
    }
    if (packageItem) {
        if ('Weight' in packageItem) {
            weight = packageItem.Weight;
        }
        if ('Width' in packageItem) {
            width = packageItem.Width;
        }
        if ('Length' in packageItem) {
            length = packageItem.Length;
        }
        if ('Height' in packageItem) {
            height = packageItem.Height;
        }
    }
    for (let index = 0; index < item.ChannelSpecificProductDetails.Keywords.length; index++) {
        const element = item.ChannelSpecificProductDetails.Keywords[index];
        keywords += element + ', ';
    }
    var header = {
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
    }
    var simple_payload = {
        weightG: weight,
        weightNetG: weight,
        widthMM: width,
        lengthMM: length,
        heightMM: height,
        plentyId: mandanten_id,
        texts: [
            {
                lang: "de",
                name1: item.ProductName,
                name2: item.ChannelSpecificProductDetails.Description1,
                name3: item.ChannelSpecificProductDetails.Description2,
                shortDescription: item.ChannelSpecificProductDetails.Shorttext,
                description: item.ChannelSpecificProductDetails.Longtext,
                keywords: keywords
            }
        ],
        variations: [
            {
                isActive: item.ChannelSpecificProductDetails.Enabled,
                isMain: true,
                name: item.VariantName,
                externalId: item.Sku,
                variationCategories: category_ids,
                plentyId: mandanten_id,
                unit: {
                    unitId: 1,
                    content: 1
                },
                //variationAttributeValues: attributes
            }
        ],
        isAvailableIfNetStockIsPositive: true,
        isInvisibleInListIfNetStockIsNotPositive: true,
        mayShowUnitPrice: mayShowUnitPrice,
        variationClients: [
            {
                plentyId: mandanten_id
            }
        ],
        //variationAttributeValues: attributes
    };
    //await exportData(simple_payload, "init_article_log");
    try {
        console.log(simple_payload);
        const response = await axios.post(url, simple_payload, { headers: header });
        //console.log('Request successful. Response:', response.data);
        return response.data;
    } catch (error) {
        //console.error('Error in the request:', error.message);
        console.error(error.response);
        // mysql.logFail(error.response.statusText + ' ' +item.Sku)
        throw error;
    }
}

async function createCategory(payload, url_category = process.env.PLENTY_URL) {
    var url = url_category + '/rest/categories';
    var header = {
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
    }
    console.log("creating category...");

    try {
        const response = await axios.post(url, payload, { headers: header });
        //console.log('Request successful. Response:', response.data);
        return response.data;
    } catch (error) {
        console.error('Error in the request:', error.message);
        throw error;
    }
}

async function updateCategory(payload) {
    var url = process.env.PLENTY_URL + '/rest/categories';
    var header = {
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
    }
    console.log("updating category...");

    //console.log(payload);
    const options = {
        url: url,
        method: 'PUT',
        body: JSON.stringify(payload),
        headers: header
    };

    try {
        const response = await axios.put(url, payload, { headers: header });
        console.log('Request successful. Response:', response.data);
        return response.data;
    } catch (error) {
        console.error('Error in the request:', error.message);
        throw error;
    }
}

async function getToken(password_token = data.password, user_token = data.username, url_token = process.env.PLENTY_URL, token = false) {
    try {
        if(!token) {
            var header = {
                'Content-Type': 'application/json',
                'Content-Length': data.length
            }
            const url = url_token + "/rest/login";
            var payload = {
                username: user_token,
                password: password_token
            }
            //var a = await postRequest(options);
            var a = await axios.post(url, payload, { headers: header });
            var out = a.data;
            if ("error" in out) {
                var msg = "Error in getToken";
                throw new Error("Error in getToken");
            }
            token = out.access_token;
            //console.log("token: " + token);
            return token;
        }
        else {
            return token;
        }
        
    } catch (error) {
        console.log(error);
    }

}

async function variation_attribute_value_name_sync() {
    token = await getToken();
    try {
        let attr_values = await mysql.getAllItemsInDB(variation_table_name);
        for (let index = 0; index < attr_values.length; index++) {
            const element = attr_values[index];
            const name = element.name;
            const nameCut = name.slice(0, -3);
            console.log("changing " + nameCut);
            var payload = {
                "valueId": element.attributevalue_id,
                "name": nameCut,
                "lang": "de"
            }
            var url = process.env.PLENTY_URL + "/rest/items/attribute_values/" + element.attributevalue_id + "/names"
            var header = {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
            try {
                await makePlentyPriceCallPOST(url, payload, header);
                await delay(1700);
            } catch (error) {
                console.log(error);
            }
        }
    } catch (error) {
        console.log(error);
    }

}

//hierneu
async function shipping_profile_update(channel) {
    try {
        token = await getToken();
        let items = await mysql.getAllItemsInDB(item_table_name);
        let bulkResp = await prepareBulkArray(items, false, true);
        console.log("bulk length: " + bulkResp.length);
        for (var g = 0; g < bulkResp.length; g++) {
            var element = bulkResp[g];
            console.log(g + " updating shipping profiles...");
            //console.log(element);
            var url = process.env.PLENTY_URL + "/rest/items/item_shipping_profiles"
            var header = {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
            try {
                await makePlentyPriceCallPOST(url, element, header);
                await delay();
            } catch (error) {
                console.log(error);
            }
            try {
                await makePlentyPriceCallPUT(url, element, header);
                await delay();
            } catch (error) {
                console.log(error);
            }
        }
    } catch (error) {
        console.log(error);
    }

}




async function handleMessage(message) {
    try {
        var jsonVal = JSON.parse(message);
        var type = null;
        var id = main_id;
        var channel = main_channel;
        var item_id = null;
        var name = null;
        if (jsonVal.hasOwnProperty('type')) {
            type = jsonVal.type;
        }
        if (jsonVal.hasOwnProperty('channel')) {
            channel = jsonVal.channel;
            channel = channel.toLocaleLowerCase();
        }
        if (jsonVal.hasOwnProperty('id')) {
            item_id = jsonVal.id;
        }
        if (jsonVal.hasOwnProperty('name')) {
            name = jsonVal.name;
        }

        switch (type) {
            //getChannelSettings
            //sms_image_sync
            case 'sms_image_sync':
                await sms_image_sync();
                break;
            case 'get_channel_settings':
                await getChannelSettings(channel);
                break;
            case 'add_orphans_to_db':
                await addOrphansToDB(channel);
                break;
            case 'no_sku_fix':
                await no_sku_fix(channel);
                break;
            case 'delete_orphans_from_amazon':
                await delteOrphans(channel);
                break;
            case 'attribute_value_sync':
                await syncAttributesWithDB(channel);
                break;
            case 'price_update':
                await updatePriceFromQueue(sku, channel);
                break;
            case 'category_description_sync':
                await category_description_sync(channel);
                break;
            case 'category_sync':
                //await categoryUpdate(channel);
                await category_sync(channel);
                break;
            case 'shipping_profile_sync':
                await shipping_profile_update(channel);
                break;
            case 'variation_attribute_value_name_sync':
                await variation_attribute_value_name_sync();
                break;
            case 'category_init':
                await initializeCategories(channel);
                break;
            case 'article_sync':
                await updateArticle(channel);
                break;
            case 'amazon_article_sync':
                await amazonArticleSync(channel);
                break;
            case 'amazon_description_sync':
                await amazonDescriptionSync(channel);
                break;
            case 'article_cleanup':
                await cleanupArticle(channel);
                break;
            case 'description_sync':
                await sync_descriptions(channel);
                break;
            case 'article_init':
                await initializeArticle(channel);
                break;
            case 'attribute_update':
                await updateAttributes(channel);
                break;
            case 'price_update_sync':
                await updatePrices(channel);
                break;
            case 'attribute_init':
                await initializeAttributes(channel);
                break;
            case 'image_init':
                await initializeImages(channel);
                break;
            case 'test_func':
                await test_func();
                break;
            case 'amazon_article_init':
                await addAmazonToDB();
            case 'createArticle':
                await createItemFromQueue(item_id, channel);
            case 'updateCategory':
                await updateCategoryFromQueue(item_id, channel);
            case 'article_update':
                await updateItemFromQueue(item_id, channel);
            case 'mysql_init':
                //await mysql_init();
                break;
            case 'attribute_export':
                await exportAttributes();
                //purgeAttributes();
                //deleteAllSubCategories();
                break;
            case 'tag_export':
                await exportAttributesSage();
                break;
            case 'article_image_update':
                await createImageFromQueue(item_id, channel);
                break;
            case 'image_variation':
                await checkForDeletedImages(item_id, channel);
                break;
            case 'tag_update':
                await updateTags(channel);
                break
            case 'ean_update':
                await updateEAN(sku, null, null, null, null, channel);
                break
            case 'amazon_orphan_sync':
                checkForItemsNotInSKUList(channel);
                break
            case 'schacka':
                //filterItemsWithMandant();
                await addAttributeValuesToDB(2);
                break
            case 'sms_item_init':
                //filterItemsWithMandant();
                await initialize_sms_items();
                break
            default:
                break;
        }
    } catch (err) {
        return;
    }
}



async function amazonEANsync(items_sage) {
    try {
        for (let index = 0; index < items_sage.length; index++) {
            const element = items_sage[index];
            let sku = element.GetPimItemBySkuAndChannelResult.Data.ProductItem.Sku;
            let plenty_item_db = await mysql.getItemInDB("item_mapping_amazon", sku, 'sku')
            let EAN = element.GetPimItemBySkuAndChannelResult.Data.ProductItem.Ean;
            let item_id = false;
            let variation_id = false;
            console.log("EAN: " + EAN + " SKU: " + sku);
            console.log("ItemId:" + plenty_item_db[0].plenty_item_id);
            console.log("VariationId:" + plenty_item_db[0].plenty_id);
            item_id = plenty_item_db[0].plenty_item_id;
            variation_id = plenty_item_db[0].plenty_id;
            if (EAN.length == 13 && item_id && variation_id) {
                console.log("updating EAN");
                let url = process.env.PLENTY_URL + "/rest/items/" + item_id + "/variations/" + variation_id + "/variation_barcodes";
                var header = {
                    'Authorization': 'Bearer ' + token,
                    'Content-Type': 'application/json'
                }
                let payload = {
                    "barcodeId": 1,
                    "variationId": variation_id,
                    "code": EAN
                }
                //appendToJSON(payload, "amazon");
                //await makePlentyPriceCallPOST(url, payload, header);
            }
        }
    } catch (error) {
        console.log(error);
    }
}

async function amazonDescriptionSync(channel = "amazon") {
    try {
        token = await getToken();
        let plenty_db_items = await mysql.getTable('item_mapping_amazon');
        plenty_db_items = plenty_db_items;
        console.log("in descr sync for amazon");
        /* let sku_list = ["2345-2",
        "3746-3",
        "9807-02-01",
        "6623",
        "6600"]; */
        let sage_items = [];
        for (let index = 0; index < plenty_db_items.length; index++) {
            //for (let index = 0; index < 3; index++) {
            console.log((index + 1) + '/' + plenty_db_items.length);
            let sku = false;
            sku = plenty_db_items[index]['sku'];
            console.log("variation_id " + plenty_db_items[index]['plenty_id']);
            if (sku) {
                console.log(sku);
                let sage_item = await getSingleArticleFromSage(sku, channel);
                if(sage_item) {
                    if (sage_item.GetPimItemBySkuAndChannelResult.Success) {
                        if(sage_item.GetPimItemBySkuAndChannelResult.Data.ProductItem.ChannelSpecificProductDetails) {
                            if(sage_item.GetPimItemBySkuAndChannelResult.Data.ProductItem.ChannelSpecificProductDetails.Enabled) {
                                sage_items.push({
                                    'sage': sage_item,
                                    'db': plenty_db_items[index]
                                });
                            }
                        }
                    }
                }
            }
            else if (plenty_db_items[index]['father_sku'] == null) {
                console.log("variation_id: " + plenty_db_items[index]['plenty_id']);
                let child = await mysql.getItemInDB('item_mapping_amazon', plenty_db_items[index]['plenty_id'], 'plenty_mainVariationId');
                if(child) {
                    if(child.hasOwnProperty('sku')) {
                        let sku = child[0]['sku'];
                        let sage_item = await getSingleArticleFromSage(sku, channel);
                        if (sage_item.GetPimItemBySkuAndChannelResult.Success) {
                            if(sage_item.GetPimItemBySkuAndChannelResult.Data.ProductItem.ChannelSpecificProductDetails) {
                                if(sage_item.GetPimItemBySkuAndChannelResult.Data.ProductItem.ChannelSpecificProductDetails.Enabled) {
                                    sage_items.push({
                                        'sage': sage_item,
                                        'db': plenty_db_items[index]
                                    });
                                }
                            }
                        }
                    }
                }
            }

        }
        console.log("sage_items: " + sage_items.length);
        for (let z = 0; z < sage_items.length; z++) {
            //for (let z = 0; z < 2; z++) {
            try {
                let obj = sage_items[z];
                let item = obj.sage;
                let db_item = obj.db;
                let data = item.GetPimItemBySkuAndChannelResult.Data;
                let product_item = data.ProductItem;
                let details = product_item.ChannelSpecificProductDetails;
                let tags = details.Tags;
                let sku = details.Sku;
                console.log("Item mit SKU " + sku + " description wird geupdatet");
                var result = await updateDescription(db_item, product_item);
                //console.log(result);
                await delay();
                //await mapTagValueToVariation(sku, false, tags); 



            } catch (error) {
                console.log(error);
            }

        }

    } catch (error) {
        console.log(error);
    }
}

// hier
async function amazonArticleSync() {
    channel = "amazon";
    let empty = [];
    //await exportData(empty, "amazon");
    const disabled_table_name_amazon = 'disabled_items_amazon';
    const item_mapping_amazon = "item_mapping_amazon";
    try {
        let sage_items = await getAllItemsFromSage(channel);
        token = await getToken();
        let plenty_items = await getAllItemsFromAmazon();
        let update_items = [];
        let new_items = [];
        let new_items_details = [];
        let not_enabled = [];
        console.log("sage items: " + sage_items.GetSkuListByChannelResult.Data.length);
        //let itemObj = await sortItemsAmazon(sage_items, plenty_items ,channel, item_mapping_amazon);
        let itemObj = await sortItems(sage_items, plenty_items, channel, item_mapping_amazon);
        update_items = itemObj.update;
        new_items = itemObj.new;
        console.log("updateable items");
        console.log(update_items.length);
        var article_bulk = await prepareBulkArrayForItemUpdate(update_items, item_mapping_amazon, plenty_id);
        console.log('items: ' + article_bulk[0]['items'].length);
        console.log('variations: ' + article_bulk[0]['variations'].length);
        var items = [];
        var variations = [];
        var itemsBulk = [];
        var variationsBulk = [];
        items = article_bulk[0]['items'];
        variations = article_bulk[0]['variations'];
        itemsBulk = await prepareBulkArray(items, true);
        variationsBulk = await prepareBulkArray(variations, true);

        for (let index = 0; index < itemsBulk.length; index++) {
            const element = itemsBulk[index];
            console.log(index + " updating items...");
            var url = process.env.PLENTY_URL + "/rest/items"
            var header = {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
            try {
                await makePlentyPriceCallPUT(url, element, header);
                await delay();
                empty.push(element);
                console.log("would do PUT bulk call for items");
                //console.log(element);
            } catch (error) {
                console.log(error);
            }

        }
        for (let index = 0; index < variationsBulk.length; index++) {
            const element = variationsBulk[index];
            console.log(index + " updating variations...");
            var url = process.env.PLENTY_URL + "/rest/items/variations"
            var header = {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
            try {
                await makePlentyPriceCallPUT(url, element, header);
                await delay();
                empty.push(element);
                console.log("would do PUT bulk call for variations");
            } catch (error) {
                console.log(error);
            }
        }

        console.log("possible new items: " + new_items.length);
        console.log("getting single items from sage...");
        for (let index = 0; index < new_items.length; index++) {
            let element = new_items[index];
            let data = null;
            let product_item = null;
            let cspd = null;
            console.log(element);
            let details = await getSingleArticleFromSage(element, channel);
            if (details.hasOwnProperty('GetPimItemBySkuAndChannelResult')) {
                if (details.GetPimItemBySkuAndChannelResult.Success) {
                    if (details.GetPimItemBySkuAndChannelResult.hasOwnProperty('Data')) {
                        data = details.GetPimItemBySkuAndChannelResult.Data;
                        if (data !== null) {
                            if (data.hasOwnProperty('ProductItem')) {
                                product_item = data.ProductItem;
                                if (product_item !== null) {
                                    if (product_item.hasOwnProperty('ChannelSpecificProductDetails')) {
                                        cspd = product_item.ChannelSpecificProductDetails;
                                        if (cspd !== null) {
                                            if (cspd.Enabled) {
                                                console.log(cspd.Sku)
                                                new_items_details.push(details.GetPimItemBySkuAndChannelResult.Data);
                                            }
                                            else {
                                                not_enabled.push(details.GetPimItemBySkuAndChannelResult.Data.ProductItem.ChannelSpecificProductDetails.Sku);
                                                var checkExistDisabled = await mysql.getItemInDB(disabled_table_name_amazon, element, 'sku');
                                                if (checkExistDisabled.length < 1) {
                                                    await mysql.insertIntoDisabledAmazon(details.GetPimItemBySkuAndChannelResult.Data.ProductItem.ChannelSpecificProductDetails);
                                                }
                                            }
                                        }
                                        else {
                                            console.log("ChannelSpecificProductDetails is NULL for " + element);
                                            var checkExistDisabled = await mysql.getItemInDB(disabled_table_name_amazon, element, 'sku');
                                            if (checkExistDisabled.length < 1) {
                                                var temp = {
                                                    'Sku': element
                                                };
                                                await mysql.insertIntoDisabledAmazon(temp);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                    else {
                        console.log("NO DATA " + element);
                        var checkExistDisabled = await mysql.getItemInDB(disabled_table_name_amazon, element, 'sku');
                        if (checkExistDisabled.length < 1) {
                            var temp = {
                                'Sku': element
                            };
                            await mysql.insertIntoDisabledAmazon(temp);
                        }
                    }
                }
                else {
                    console.log("NO Success for " + element);
                    var checkExistDisabled = await mysql.getItemInDB(disabled_table_name_amazon, element, 'sku');
                    if (checkExistDisabled.length < 1) {
                        var temp = {
                            'Sku': element
                        };
                        await mysql.insertIntoDisabledAmazon(temp);
                    }
                }
            }
            else {
                console.log("NO GetPimItemBySkuAndChannelResult " + element);
            }
        }
        console.log('handling enabled children now...');
        for (let index = 0; index < new_items_details.length; index++) {
            const element = new_items_details[index];
            let cspd = element.ProductItem.ChannelSpecificProductDetails;
            let father_sku = cspd.FatherSku;
            let father_not_enabled = await checkForVaddern(not_enabled, father_sku);
            if (father_not_enabled) {
                new_items_details.splice(index, 1);
                index--;
            }
        }

        console.log("new items: " + new_items_details.length);
        for (let index = 0; index < new_items_details.length; index++) {
            const element = new_items_details[index];
            console.log(element.ProductItem.Sku);
            var payload = {
                "payload": "81",
                "type": "createArticle",
                "id": element.ProductItem.Sku
            }
            empty.push(element);
            /* console.log("publishing message to create article sku: " + element.ProductItem.Sku);
            publish("", queueName, Buffer.from(JSON.stringify(payload))); */
            /* await createItemFromQueue(element.ProductItem.Sku, channel);
            delay(2000); */
        }
        fs.appendFile("test.txt", JSON.stringify(empty), function (err) {
            if (err) {
                return console.log(err);
            }
            console.log("The file was saved!");
        });
        //await amazonEANsync(update_items);
        //await exportData(empty, "amazon");
    } catch (error) {
        console.log(error);
    }
}

async function addAmazonToDB() {
    console.log("in addAmazonToDB");
    let amazon_items = await getAllItemsFromAmazon();
    try {
        for (let a = 0; a < amazon_items.length; a++) {
            const element = amazon_items[a];
            let variation_id = null;
            let item_id = null;
            let name = null;
            let sku = null;
            let mainVariationId = null;
            variation_id = element.id;
            item_id = element.itemId;
            mainVariationId = element.mainVariationId;
            name = element.name;
            sku = element.externalId;
            console.log("adding item: " + variation_id + " item_id: " + item_id + " sku: " + sku);
            let temp = {
                'id': variation_id,
                'itemId': item_id,
                'plenty_mainVariationId': mainVariationId,
                'name': name,
                'sage_id': null,
                'father_id': null,
                'father_sku': null,
                'sku': sku,
                'categories': null,
                'variation_attributes': null
            }
            await mysql.insertIntoItemMappingAmazon(temp);
        }

    } catch (error) {
        console.log(error);
    }

}

async function getAllInactive() {
    try {
        token = await getToken();
        var plenty_items = [];
        var first_page_for_total_count = await getItemsFromPlenty(1, plenty_id);
        var pages = first_page_for_total_count.lastPageNumber;
        for (let index = 1; index <= pages; index++) {
            //for (let index = 1; index <= 1; index++) {
            console.log(index + '/' + pages);
            var items = await getItemsFromPlenty(index, plenty_id);
            delay();
            for (let a = 0; a < items.entries.length; a++) {
                const element = items.entries[a];
                plenty_items.push(element);
            }
        }
        console.log(plenty_items.length);
        return plenty_items;
    } catch (error) {
        console.log(error);
    }
}

async function deleteOrphansCall(item_id, variation_id) {
    var url = process.env.PLENTY_URL + "/rest/items/" + item_id+"/variations/"+variation_id;
    var header = {
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
    }
    try {
        await makePlentyPriceCallDELETE(url, header);
    } catch (error) {
        console.log(error);
    }
}


async function delteOrphans() {
    token = await getToken();
    let orphans = await mysql.getTable('item_mapping_orphan');
    for (let index = 0; index < orphans.length; index++) {
        console.log((index + 1) + "/" + orphans.length);
        const element = orphans[index];
        if(element.is_main === 0) {
            console.log("deleting " + element.plenty_id + " " + element.plenty_item_id);
            await deleteOrphansCall(element.plenty_item_id, element.plenty_id);
            await delay(1700);
        }
        else {
            console.log(element.name +" "+ element.plenty_id + " " + element.plenty_item_id + " IS MAIN not deleting");
        }
    }
}

async function no_sku_fix() {
    console.log("no_sku_fix");
    let orphans = await getAllInactiveItemsFromPlenty();
    console.log(orphans.length);
    await insertOrphansinDB(orphans);
}

async function addOrphansToDB() {
    console.log("in addOrphansToDB");
    let orphans = await getAllOrphans();
    await insertOrphansinDB(orphans);
}


async function insertOrphansinDB(orphans) {
    try {
        for (let a = 0; a < orphans.length; a++) {
            const element = orphans[a];
            let variation_id = null;
            let item_id = null;
            let name = null;
            let sku = null;
            let mainVariationId = null;
            let is_main = null;
            variation_id = element.id;
            item_id = element.itemId;
            mainVariationId = element.mainVariationId;
            name = element.name;
            sku = element.externalId;
            is_main = element.isMain;
            console.log("adding item: " + variation_id + " item_id: " + item_id + " sku: " + sku);
            if (sku) {
                console.log(" sku vorhanden");
            }
            else {
                let temp = {
                    'id': variation_id,
                    'itemId': item_id,
                    'plenty_mainVariationId': mainVariationId,
                    'is_main': is_main,
                    'name': name,
                    'sage_id': null,
                    'father_id': null,
                    'father_sku': null,
                    'sku': sku,
                    'categories': null,
                    'variation_attributes': null
                }
                await mysql.insertIntoItemMappingInactive(temp);
            }

        }

    } catch (error) {
        console.log(error);
    }
}

async function getPriceBySKU(sku, channel) {
    let url = process.env.SAGE_URL + '/PriceInfo/' + sku + '?channel=' + channel;
    try {
        var header = {
            'Content-Type': 'application/json'
        }
        const response = await axios.get(url, { headers: header });
        //console.log('Request successful. Response:', response.data.id);
        return response.data;
    } catch (error) {
        console.log(error);
        console.error('Error in the request:', error.message);
        throw error;
    }
}

async function checkItemHasPricePlenty(variation_id) {
    var url = process.env.PLENTY_URL + "/rest/items/variations/variation_sales_prices?variationId=" + variation_id;
    try {
        var header = {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        }
        const response = await axios.get(url, { headers: header });
        //console.log('Request successful. Response:', response.data.id);
        return response.data;
    }
    catch (error) {
        console.log(error);
        console.error('Error in the request:', error.message);
        throw error;
    }
}

async function updatePriceDB(sku, price = null, is_special = null, special_price = null, table = item_table_name) {
    await mysql.updatePriceOfItem(sku, price, is_special, special_price, table);
}

async function getStandardPricePlenty(plenty_item) {
    for (let index = 0; index < plenty_item.length; index++) {
        const element = plenty_item[index];
        if (element.salesPriceId == 1) {
            return element.price;
        }
    }
}

async function getSpecialPricePlenty(plenty_item) {
    for (let index = 0; index < plenty_item.length; index++) {
        const element = plenty_item[index];
        if (element.salesPriceId == special_price_plenty_id) {
            return element.price;
        }
    }
}


async function preparePriceArray(allItems, channel, table = item_table_name) {
    var priceArr = [];
    var priceArrPUT = [];
    var priceArrDELETE = [];
    var response = [];
    for (let index = 0; index < allItems.length; index++) {
        console.log((index + 1) + "/" + allItems.length);
        let sku = allItems[index];
        console.log(sku);
        let priceData = await getPriceBySKU(sku, channel);
        let itemData = await mysql.getItemInDB(table, sku, 'sku');
        if (itemData.length) {
            let item_plenty = await checkItemHasPricePlenty(itemData[0]['plenty_id']);
            await delay(1000);
            if (item_plenty && item_plenty.entries.length) {
                // if this is bigger than 1 => plenty item has special price
                if (item_plenty.entries.length > 1) {
                    if (!priceData.GetPriceBySkuAndChannelResult.Data.HasSpecialPrice) {
                        priceArrDELETE.push({
                            "price": priceData.GetPriceBySkuAndChannelResult.Data,
                            "item": itemData[0]
                        })
                    }
                    priceArrPUT.push({
                        "price": priceData.GetPriceBySkuAndChannelResult.Data,
                        "item": itemData[0]
                    });
                    priceArr.push({
                        "price": priceData.GetPriceBySkuAndChannelResult.Data,
                        "item": itemData[0]
                    });
                    await updatePriceDB(sku, priceData.GetPriceBySkuAndChannelResult.Data.StandardPrice, priceData.GetPriceBySkuAndChannelResult.Data.HasSpecialPrice, priceData.GetPriceBySkuAndChannelResult.Data.SpecialPrice, table)

                }
                else {
                    let plenty_price = await getStandardPricePlenty(item_plenty.entries);
                    if (priceData.GetPriceBySkuAndChannelResult.Data.HasSpecialPrice) {
                        priceArr.push({
                            "price": priceData.GetPriceBySkuAndChannelResult.Data,
                            "item": itemData[0]
                        });
                    }
                    if (priceData.GetPriceBySkuAndChannelResult.Data.StandardPrice != plenty_price) {
                        priceArrPUT.push({
                            "price": priceData.GetPriceBySkuAndChannelResult.Data,
                            "item": itemData[0]
                        });
                        await updatePriceDB(sku, priceData.GetPriceBySkuAndChannelResult.Data.StandardPrice, priceData.GetPriceBySkuAndChannelResult.Data.HasSpecialPrice, priceData.GetPriceBySkuAndChannelResult.Data.SpecialPrice, table)
                    }
                }
            }
            else {
                var plenty_price = await getStandardPricePlenty(item_plenty.entries);
                if (priceData.GetPriceBySkuAndChannelResult.Data.StandardPrice != plenty_price) {
                    priceArr.push({
                        "price": priceData.GetPriceBySkuAndChannelResult.Data,
                        "item": itemData[0]
                    });
                    await updatePriceDB(sku, priceData.GetPriceBySkuAndChannelResult.Data.StandardPrice, priceData.GetPriceBySkuAndChannelResult.Data.HasSpecialPrice, priceData.GetPriceBySkuAndChannelResult.Data.SpecialPrice, table)
                }
            }
        }

    }
    response.push({
        "POST": priceArr,
        "PUT": priceArrPUT,
        "DELETE": priceArrDELETE
    })
    return response;
}

async function prepareBulkArray(priceArr, items = false, shipping_profile = false) {
    var priceRange = priceArr.length / 50;
    var pricesBulk = [];
    priceRange = Math.ceil(priceRange);
    try {
        for (var e = 0; e < priceRange; e++) {
            var temp = [];
            if (e < 0) {
                var ende = (e + 1) * 50
                var start = e;
            }
            else {
                var ende = (e + 1) * 50
                var start = e * 50;
            }
            for (var f = start; f < ende; f++) {
                if (f < priceArr.length) {
                    if (items) {
                        console.log("in if items");
                        try {
                            var element = priceArr[f]['payload'];
                            temp.push(element);
                        } catch (error) {
                            console.log(error);
                        }
                    }
                    if (shipping_profile) {
                        try {
                            var element = priceArr[f]['plenty_item_id'];
                            var payload = {
                                "itemId": element,
                                "profileId": n24_shipping_profile
                            }
                            temp.push(payload);
                        } catch (error) {
                            console.log(error);
                        }
                    }
                    else {
                        var element = priceArr[f]['price'];
                        var item = priceArr[f]['item'];
                        try {
                            if (element) {
                                if (element.hasOwnProperty('HasSpecialPrice')) {
                                    if (element['HasSpecialPrice']) {
                                        var singlePrice = {
                                            "variationId": item['plenty_id'],
                                            "salesPriceId": 3,
                                            "price": element['SpecialPrice']
                                        }
                                        temp.push(singlePrice);
                                        var singlePrice = {
                                            "variationId": item['plenty_id'],
                                            "salesPriceId": 1,
                                            "price": element['StandardPrice']
                                        }
                                        temp.push(singlePrice);
                                    }
                                    else {
                                        var singlePrice = {
                                            "variationId": item['plenty_id'],
                                            "salesPriceId": 1,
                                            "price": element['StandardPrice']
                                        }
                                        temp.push(singlePrice);
                                    }
                                }
                            }
                            else {
                                console.log("in else");
                            }
                        } catch (error) {
                            console.log(error);
                        }
                    }
                }
            }
            pricesBulk.push(temp);
        }
    } catch (error) {
        console.log(error);
    }
    return pricesBulk;
}

// updating prices here
async function updatePrices(channel) {
    try {
        let mandant_id = n24_plenty_id;
        let table_name = item_table_name;
        if (channel == "amazon") {
            table_name = "item_mapping_amazon";
            mandant_id = plenty_id;
        }
        if (channel == "suckmystraw") {
            table_name = "item_mapping_sms";
            mandant_id = sms_plenty_id;
        }
        token = await getToken();
        let allItems = await getAllItemsFromSage(channel);
        allItems = allItems.GetSkuListByChannelResult.Data;
        /* let allItems = ["2345-2",
        "3746-3",
        "5301",
        "6623",
        "6600"]; */
        console.log(allItems.length);
        var priceArr = [];
        var priceArrPUT = [];
        var priceArrDELETE = [];
        var priceArrResponse;
        // getting prices from sage and the related item from the db
        priceArrResponse = await preparePriceArray(allItems, channel, table_name);
        priceArr = priceArrResponse[0]['POST'];
        priceArrPUT = priceArrResponse[0]['PUT'];
        priceArrDELETE = priceArrResponse[0]['DELETE'];
        // preparing bulk array
        console.log("preparing bulk now");
        console.log(priceArr.length);
        console.log(priceArrPUT.length);
        var pricesBulk = [];
        var pricesBulkPUT = [];
        pricesBulk = await prepareBulkArray(priceArr);
        pricesBulkPUT = await prepareBulkArray(priceArrPUT);

        console.log("bulk:");
        console.log(pricesBulk);
        console.log("bulk PUT:");
        console.log(pricesBulkPUT);
        for (var g = 0; g < priceArrDELETE.length; g++) {
            var element = priceArrDELETE[g]['item'];
            console.log(g + " deleting sales_prices...");
            console.log(element);
            var url = process.env.PLENTY_URL + "/rest/items/" + element['plenty_item_id'] + "/variations/" + element['plenty_id'] + "/variation_sales_prices/" + special_price_plenty_id;
            var header = {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
            try {
                await makePlentyPriceCallDELETE(url, header);
                await delay();
            } catch (error) {
                console.log(error);
            }
        }
        for (var g = 0; g < pricesBulk.length; g++) {
            var element = pricesBulk[g];
            console.log(g + " updating prices...");
            console.log(element);
            var url = process.env.PLENTY_URL + "/rest/items/variations/variation_sales_prices"
            var header = {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
            try {
                await makePlentyPriceCallPOST(url, element, header);
                await delay();
            } catch (error) {
                console.log(error);
            }
        }
        for (var g = 0; g < pricesBulkPUT.length; g++) {
            var element = pricesBulkPUT[g];
            console.log(g + " updating prices...");
            console.log(element);
            var url = process.env.PLENTY_URL + "/rest/items/variations/variation_sales_prices"
            var header = {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
            try {
                await makePlentyPriceCallPUT(url, element, header);
                await delay();
            } catch (error) {
                console.log(error);
            }
        }
    } catch (error) {
        console.log(error);
    }
}

async function makePlentyPriceCallPUT(url, element, header) {
    try {
        const response = await axios.put(url, element, { headers: header });
        console.log('PUT response:', response.data);
        return response.data;
    } catch (error) {
        console.log(error);
    }
}

async function makePlentyPriceCallDELETE(url, header) {
    try {
        const response = await axios.delete(url, { headers: header });
        console.log('DELETE response:', response.data);
        return response.data;
    } catch (error) {
        console.log(error);
    }
}



async function makePlentyPriceCallPOST(url, element, header) {
    try {
        const response = await axios.post(url, element, { headers: header });
        console.log('POST response:', response.data);
        return response.data;
    } catch (error) {
        console.log(error);
    }
}

async function updatePriceFromQueue(sku, channel) {
    try {
        let price_entry = await getPriceBySKU(sku, channel);
        let itemData_db = await mysql.getItemInDB('item_mapping', sku, 'sku');
        let item_plenty = await checkItemHasPricePlenty(itemData_db[0]['plenty_id']);
        price_entry = price_entry.GetPriceBySkuAndChannelResult.Data;
        if (item_plenty && item_plenty.entries.length) {

        }

        if (price_entry) {
            if (element.hasOwnProperty('HasSpecialPrice')) {
                if (element['HasSpecialPrice']) {
                    var singlePrice = {
                        "variationId": item['plenty_id'],
                        "salesPriceId": 3,
                        "price": element['SpecialPrice']
                    }
                    temp.push(singlePrice);
                    var singlePrice = {
                        "variationId": item['plenty_id'],
                        "salesPriceId": 1,
                        "price": element['StandardPrice']
                    }
                    temp.push(singlePrice);
                }
                else {
                    var singlePrice = {
                        "variationId": item['plenty_id'],
                        "salesPriceId": 1,
                        "price": element['StandardPrice']
                    }
                    temp.push(singlePrice);
                }
            }
        }
    } catch (error) {
        console.log(error);
    }

}

// helper function to add the old json files to the DB
async function mysql_init() {
    try {
        console.log("initializing mysqldb");
        let mapping = await loadJsonFile('item_mapping.json');
        let categories = await loadJsonFile('id_mapping.json');
        let variation_attributes = await loadJsonFile('variationname_attributevalues.json');
        await mysql.startMysqlConnection();
        for (let index = 0; index < categories.length; index++) {
            const element = categories[index];
            mysql.insertIntoCategoryMapping(element);
        }
        //mysql.insertIntoItemMapping(mapping[0]);
        //mysql.insertIntoCategoryMapping(categories[0]);
        //mysql.insertIntoVariationnameAttributesMapping(variation_attributes[0]);
    } catch (error) {
        console.error(error); // Handle error globally
    }

}

async function getFatherById(id, item_mapping) {
    for (let index = 0; index < item_mapping.length; index++) {
        const element = item_mapping[index];
        if (element.sage_id == id) {
            return element;
        }
    }
    return false;
}

async function getCategoryBySageId(sage_id, mapping) {
    if (mapping !== null && mapping !== undefined) {
        if (mapping.length > 0) {
            for (const obj of mapping) {
                if (obj.sage_id == sage_id) {
                    return obj;
                }
            }
            return false;
        }
    }
    return false;
}

async function checkCategoryExistsInPlenty(id, plenty_obj) {
    if (plenty_obj !== null && plenty_obj !== undefined) {
        if (plenty_obj.length > 0) {
            for (const obj of plenty_obj) {
                //console.log("looking for " + id);
                if (id == obj.id) {
                    console.log(obj.id);
                    return true;
                }
            }
        }
    }
    return false;
}

async function checkCategoryExistsInPlentyNew(id) {
    let url = process.env.PLENTY_URL + "/rest/categories/" + id;
    try {
        var header = {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        }
        const response = await axios.get(url, { headers: header });
        //console.log('Request successful. Response:', response.data.id);
        return response.data;
    }
    catch (error) {
        console.log(error);
        //console.error('Error in the request:', error.message);
        //throw error;
    }
}



async function iterateCategoryTree(categoryTree, callback, level = 2) {
    for (const category of categoryTree) {
        await callback(category, level);

        if (category.Children && category.Children.length > 0) {
            await iterateCategoryTree(category.Children, callback, level + 1); // Pass the updated id_mapping
        }
    }
}

async function getPartentIdById(id, data = null) {
    console.log("looking for parent of " + id);
    console.log("data: " + data.length);
    if (data !== null) {
        var mapping = data;
    }
    else {
        var mapping = await loadJsonFile(id_mapping_filename);
    }
    if (mapping !== undefined) {
        for (const obj of mapping) {
            if (obj.sage_id === id) {
                foundObject = obj;
                break; // Stop the loop once the desired object is found
            }
        }

        if (foundObject) {
            console.log("Object found, plenty_category_id:", foundObject.plenty_category_id);
            return foundObject.plenty_category_id;
        }
        else {
            console.log("Object not found");
            return false;
        }
    }
    else {
        for (const obj of id_mapping) {
            if (obj.sage_id === targetSageId) {
                foundObject = obj;
                break; // Stop the loop once the desired object is found
            }
        }

        if (foundObject) {
            console.log("Object found:", foundObject);
            return foundObject.plenty_category_id;
        }
        else {
            console.log("Object not found");
            return false;
        }
    }
}


async function findPlentyParentID(sage_parent_id) {
    /*if(mapping !== null && mapping !== undefined) {
        if(mapping.length > 0) {
            for (const obj of mapping) {
                //console.log("looking for " + id);
                if(parent_id == obj.sage_parent_id) {
                    return obj.plenty_parent_id;
                }
            }
        }
    }
    return false;*/
    let parentItem = await mysql.getItemInDB(category_table_name, sage_parent_id, 'sage_parent_id');
    if (parentItem.length) {
        return parentItem[0].plenty_parent_id;
    } else {
        return false;
    }

}



async function getAllPlentyCategories(array, parent_id, all_categories = []) {
    for (let index = 0; index < array.length; index++) {
        const element = array[index];
        if (element.hasChildren && element.parentCategoryId === parent_id) {
            //console.log(element);
            var children = await getCategoryFromPlenty(element.id);
            children = JSON.parse(children);
            children = children.entries;
            if (children.length) {
                for (let index = 0; index < children.length; index++) {
                    const element = children[index];
                    all_categories.push(element);
                }
            }
            await getAllPlentyCategories(children, element.id, all_categories);

        }
    }
    return all_categories;
}

async function getVariationIdByName(name, array) {
    name = name + ' HN';
    for (let index = 0; index < array.length; index++) {
        const element = array[index];
        if (name == element.name) {
            return element.id;
        }
    }
}


async function handleVariantNameDB(variant_name, table = variation_table_name, attribute_group_id = variation_attribute_groupid) {
    console.log("handling variantname now: " + variant_name);
    if(table == variation_table_name) {
        variant_name = variant_name + ' HN';
    }
    var payload = [];
    var entry = await mysql.getItemInDB(table, variant_name, 'name');
    if (entry.length) {
        //console.log("entry exists already");
        console.log(entry[0]);
        var temp = {
            valueId: entry[0].attributevalue_id,
            id: attribute_group_id
        };
        payload.push(temp);
    }
    else {
        var result = await createAttributeValue(attribute_group_id, variant_name);
        var temp = {
            name: variant_name,
            id: result.id
        };
        payload.push(temp);
        await mysql.insertIntoAttributesMapping(temp, table);
        delay();
    }
    return payload;
}

async function handleVariantName(variant_name, variationname_attributevalues) {
    //console.log(variationname_attributevalues);
    variant_name = variant_name + ' HN';
    var variation_not_exist = false;
    console.log("handling variation name " + variant_name);
    if (variationname_attributevalues.length > 0) {
        for (let index = 0; index < variationname_attributevalues.length; index++) {
            const element = variationname_attributevalues[index];
            console.log("current name: " + element.name);
            if (element.name == variant_name) {
                console.log("variation exists already");
                return variationname_attributevalues;
            }
            else {
                variation_not_exist = true;
            }
        }
        if (variation_not_exist) {
            var result = await createAttributeValue(variation_attribute_groupid, variant_name);
            variationname_attributevalues.push({
                name: variant_name,
                id: result.id
            });
            console.log("variation " + variant_name + " created");
            await exportData(variationname_attributevalues, "variationname_attributevalues");
            return variationname_attributevalues;
        }
    }
    else {
        var result = await createAttributeValue(variation_attribute_groupid, variant_name);
        variationname_attributevalues.push({
            "name": variant_name,
            id: result.id
        });
        console.log("variation " + variant_name + " created");
        await exportData(variationname_attributevalues, "variationname_attributevalues");
        return variationname_attributevalues;
    }
}

async function initializeImages(channel) {
    token = await getToken();
    await mysql.emptyTable(image_table_name);
    var allItems = await mysql.getTable('item_mapping');
    //delete all plenty images
    for (let i = 0; i < allItems.length; i++) {
        let counter = i + 1;
        console.log("Delete Images. Item " + counter + "/" + allItems.length);
        const element = allItems[i];
        await deleteAllPlentyImages(element['sku']);
    }
    for (let index = 0; index < allItems.length; index++) {
        let counter = index + 1;
        const element = allItems[index];
        console.log("Sync Image. Item " + counter + "/" + allItems.length + " (SKU: " + element['sku'] + ")");
        await createImageFromQueueNoGetToken(element['sku'], channel);
    }
}

async function initializeArticle(channel) {
    var initObj = {};
    var allItems = [];
    var items = [];
    var variationname_attributevalues = [];
    var item_mapping = [];
    var fatherItems = [];
    var childrenItems = [];
    var disabledItems = [];
    var plentyItems = [];
    var init_article_log = [];
    var init_article_loghelp = [];
    var category_mapping = await loadJsonFile('id_mapping.json');
    var attribute_mapping = await loadJsonFile('attribute_mapping.json');
    variationname_attributevalues = await loadJsonFile('variationname_attributevalues.json');
    var sage_items = await loadJsonFile('items.json');
    //await exportData(initObj, "init_article_log");
    try {
        await getToken();
        if (sage_items.length > 0) {
            items = sage_items;
        }
        else {
            await exportData(initObj, "items");
            allItems = await getAllItemsFromSage(channel);
            allItems = allItems.GetSkuListByChannelResult.Data;
            console.log("length: " + allItems.length);
            /* allItems = [
                "3746-2",
                "3746-3",
                "3746-4"
            ] */
            for (let index = 0; index < allItems.length; index++) {
                //for (let index = 0; index < 10; index++) {
                const element = allItems[index];
                console.log(element);
                let details = await getSingleArticleFromSage(element, channel);
                items.push(details);
            }
            await exportData(items, "items");
        }
        console.log(items.length);
        console.log("seperating into father and child articles");
        for (let index = 0; index < items.length; index++) {
            const element = items[index];
            //console.log(variationname_attributevalues.length);
            if (element.GetPimItemBySkuAndChannelResult.Data.ProductItem.ChannelSpecificProductDetails) {
                if (element.GetPimItemBySkuAndChannelResult.Data.ProductItem.ChannelSpecificProductDetails.Enabled) {
                    if (element.GetPimItemBySkuAndChannelResult.Data.ProductItem.ChannelSpecificProductDetails.FatherId === null) {
                        fatherItems.push(element);
                        /* if(element.GetPimItemBySkuAndChannelResult.Data.ProductItem.VariantName) { 
                            variationname_attributevalues = await handleVariantName(element.GetPimItemBySkuAndChannelResult.Data.ProductItem.VariantName, variationname_attributevalues);
                            await delay(1200);
                        } */
                    }
                    else {
                        childrenItems.push(element);
                        /* if(element.GetPimItemBySkuAndChannelResult.Data.ProductItem.VariantName) { 
                            variationname_attributevalues = await handleVariantName(element.GetPimItemBySkuAndChannelResult.Data.ProductItem.VariantName, variationname_attributevalues);
                            await delay(1200);
                        } */
                    }
                }
                else {
                    disabledItems.push(element);
                }
            }
        }
        console.log("father length: " + fatherItems.length);
        console.log("children length: " + childrenItems.length);
        //await exportData(variationname_attributevalues, "variationname_attributevalues");
        //await exportData(initObj, "father_items");
        //await exportData(fatherItems, "father_items");
        fatherItems = await loadJsonFile('father_items.json');
        item_mapping = await loadJsonFile('item_mapping.json');
        //await exportData(initObj, "children_items");
        //await exportData(childrenItems, "children_items");
        //await exportData(initObj, "item_mapping");
        //await exportData(initObj, "plenty_items");
        for (let index = 0; index < fatherItems.length; index++) {
            const element = fatherItems[index];
            if (element.hasOwnProperty('GetPimItemBySkuAndChannelResult')) {
                var productItem = element.GetPimItemBySkuAndChannelResult.Data.ProductItem;
                if (!productItem.VariantName || productItem.VariantName === null) {
                    console.log(productItem.Sku + " kein Variant Name");
                }
                var plenty_category_ids;
                var categories_for_mapping = [];
                var attributes_for_plenty;
                var plenty_item = null;

                var tags = productItem.ChannelSpecificProductDetails.Tags;
                var productCategories = productItem.ChannelSpecificProductDetails.ProductCategories;
                var result = await prepareCategories(productCategories, category_mapping)
                plenty_category_ids = result.plenty_category_ids;
                categories_for_mapping.push(result.categories_for_mapping);
                var attribute_value_id = await getVariationIdByName(productItem.VariantName, variationname_attributevalues);
                attributes_for_plenty = [{
                    valueId: attribute_value_id,
                    id: variation_attribute_groupid
                }];
                //attributes_for_plenty = await prepareAttributeValues(tags, attribute_mapping, true);
                let test = {
                    'name': productItem.ProductName,
                    'sage_id': productItem.ChannelSpecificProductDetails.Id,
                    'father_id': productItem.ChannelSpecificProductDetails.FatherId,
                    'father_sku': productItem.ChannelSpecificProductDetails.FatherSku,
                    'sku': productItem.Sku,
                    'categories': categories_for_mapping,
                    'variation_attributes': attribute_value_id
                }
                init_article_log.push(test);
                await exportData(init_article_log, "init_article_log");
                console.log(productItem.Sku);
                plenty_item = await createFatherArticleInPlenty(element, plenty_category_ids, attributes_for_plenty);
                plentyItems.push(plenty_item);
                let temp = {
                    'id': plenty_item.variations[0].id,
                    'itemId': plenty_item.variations[0].itemId,
                    'name': productItem.ProductName,
                    'sage_id': productItem.ChannelSpecificProductDetails.Id,
                    'father_id': productItem.ChannelSpecificProductDetails.FatherId,
                    'father_sku': productItem.ChannelSpecificProductDetails.FatherSku,
                    'sku': productItem.Sku,
                    'categories': categories_for_mapping,
                    'variation_attributes': attributes_for_plenty
                }
                item_mapping.push(temp);
                await delay(1000);
            }
        }
        //await exportData(plentyItems, "plenty_items");
        //await exportData(item_mapping, "item_mapping");
        for (let index = 0; index < childrenItems.length; index++) {
            const element = childrenItems[index];
            var plenty_category_ids;
            var categories_for_mapping = [];
            var productItem = element.GetPimItemBySkuAndChannelResult.Data.ProductItem;
            console.log(productItem.Sku);
            var tags = productItem.ChannelSpecificProductDetails.Tags;
            var productCategories = productItem.ChannelSpecificProductDetails.ProductCategories;
            var result = await prepareCategories(productCategories, category_mapping);
            plenty_category_ids = result.plenty_category_ids;
            categories_for_mapping.push(result.categories_for_mapping);
            var father = await getFatherById(productItem.ChannelSpecificProductDetails.FatherId, item_mapping);
            if (father) {
                var attribute_value_id = await getVariationIdByName(productItem.VariantName, variationname_attributevalues);
                attributes_for_plenty = [{
                    valueId: attribute_value_id,
                    id: variation_attribute_groupid
                }];
                //attributes_for_plenty = await prepareAttributeValues(tags, attribute_mapping, false, father.variation_attributes);
                let test = {
                    'element': element,
                    'name': productItem.ProductName,
                    'sage_id': productItem.ChannelSpecificProductDetails.Id,
                    'father_id': productItem.ChannelSpecificProductDetails.FatherId,
                    'father_sku': productItem.ChannelSpecificProductDetails.FatherSku,
                    'sku': productItem.Sku,
                    'categories': categories_for_mapping,
                    'variation_attributes': attributes_for_plenty
                }
                init_article_log.push(test);
                await exportData(init_article_log, "init_article_log");
                console.log(productItem.Sku);
                var plenty_item = await createChildrenArticleInPlenty(element, plenty_category_ids, father, attributes_for_plenty);
                plentyItems.push(plenty_item);
                let temp = {
                    'id': plenty_item.id,
                    'itemId': plenty_item.itemId,
                    'name': productItem.ProductName,
                    'sage_id': productItem.ChannelSpecificProductDetails.Id,
                    'father_id': productItem.ChannelSpecificProductDetails.FatherId,
                    'father_sku': productItem.ChannelSpecificProductDetails.FatherSku,
                    'sku': productItem.Sku,
                    'categories': categories_for_mapping,
                    'variation_attributes': attributes_for_plenty
                }
                item_mapping.push(temp);

            }
            await delay(1000);
        }
        await exportData(plentyItems, "plenty_items");
        await exportData(item_mapping, "item_mapping");

    } catch (err) {
        console.log(err);
        return;
    }

}

async function initializeCategories(channel) {
    var id_mapping = [];
    var plenty_categories = null;
    token = await getToken();
    sage_tree = await getTreeCategoriesFromSage(channel);
    sage_main_id = sage_tree.GetCategoryTreeResult.Data.Id;
    plenty_categories = await getCategoryFromPlenty(main_id);
    plenty_categories = JSON.parse(plenty_categories);
    plenty_categories = plenty_categories.entries;
    plenty_categories = await getAllPlentyCategories(plenty_categories, main_id, plenty_categories);
    var tree_categories = sage_tree.GetCategoryTreeResult.Data.Children;
    await iterateCategoryTree(tree_categories, async (category, level) => {
        console.log(`Category ID: ${category.Id}, Name: ${category.Text}, ParentID: ${category.ParentId} Level: ${level}`);
        var the_parent_id = category.ParentId;
        const hasChildren = category.Children.length > 0;
        if (the_parent_id === sage_main_id) {
            the_parent_id = main_id;
        }
        else {
            the_parent_id = await getPartentIdById(the_parent_id, id_mapping);
        }
        var simple_payload = [{
            level: level,
            hasChildren: hasChildren,
            parentCategoryId: plenty_parent_id,
            plentyId: n24_plenty_id,
            type: "item",
            details: [
                {
                    parentCategoryId: plenty_parent_id,
                    lang: "de",
                    name: category.Text,
                    description: category.LongText,
                    shortDescription: category.ShortText,
                    position: category.Order,
                    itemListView: "ItemViewCategoriesList",
                    singleItemView: "ItemViewSingleItem",
                    pageView: "PageDesignContent",
                    plentyId: n24_plenty_id
                }
            ]
        }];
        let response = await createCategory(simple_payload);
        console.log("response" + response);
        console.log(response[0].parentCategoryId);
        let plenty_category_id = response[0].id;
        let plenty_parent_id = response[0].parentCategoryId;
        let temp = {
            'plenty_category_id': plenty_category_id,
            'sage_id': category.Id,
            'name': category.Text,
            'sage_parent_id': category.ParentId,
            'plenty_parent_id': plenty_parent_id
        }
        console.log("pushing in id_mapping");
        //console.log(temp);
        id_mapping.push(temp);

    });
    console.log("done: " + id_mapping);
    await exportData(id_mapping, id_mapping_filename);
}

async function loadJsonFile(filePath) {
    try {
        const data = await fs.readFile(filePath, 'utf-8');
        return JSON.parse(data);
    } catch (error) {
        console.error('Error reading JSON file:', error.message);
        throw error;
    }
}




async function exportData(jsonObj, output_filename) {
    try {
        //console.log("in export data");
        const jsonString = JSON.stringify(jsonObj, null, 2); // The third argument (2) specifies the number of spaces for indentation
        // Specify the file path
        const filePath = output_filename + '.json';
        var test = await checkFileExists(filePath);
        if (test) {
            console.log("file exists");
            await fs.writeFile(filePath, jsonString, 'utf8', (err) => {
                if (err) {
                    console.error('Error writing file:', err);
                } else {
                    console.log('File written successfully:', filePath);
                }
            });
        }
        else {
            console.log("creating file");
            await fs.writeFile(filePath, jsonString); // You can write any default content here
        }
        // Write the JSON string to the file

    } catch (error) {
        console.log(error);
    }

}

async function category_description_sync(channel) {
    try {
        let sage_tree = await getTreeCategoriesFromSage(channel);
        let tree_categories = sage_tree.GetCategoryTreeResult.Data.Children;
        token = await getToken();
        await iterateCategoryTree(tree_categories, async (category, level) => {
            try {
                level--;
                console.log(`Category ID: ${category.Id}, Name: ${category.Text}, ParentID: ${category.ParentId} Level: ${level}`);
                let plenty_cat = await mysql.getItemInDB(category_table_name, category.Id, 'sage_id');
                plenty_cat = plenty_cat[0];
                var update_payload = [{
                    id: plenty_cat.plenty_category_id,
                    details: [
                        {
                            categoryId: plenty_cat.plenty_category_id,
                            lang: "de",
                            name: category.Text,
                            description: category.LongText,
                            shortDescription: category.ShortText,
                            position: category.Order,
                            itemListView: "ItemViewCategoriesList",
                            singleItemView: "ItemViewSingleItem",
                            pageView: "PageDesignContent",
                            plentyId: n24_plenty_id
                        }
                    ]
                }];
                console.log(update_payload[0].details);
                await updateCategory(update_payload);
                await delay();
            } catch (error) {
                console.log(error);
            }
        });
    } catch (error) {
        console.log(error);
    }
}

async function category_sync(channel) {
    try {
        console.log("in category_sync");
        var mandant_id = n24_plenty_id;
        var pw_for_token = data.password;
        var usr_for_token = data.username;
        var url_for_token = process.env.PLENTY_URL;
        try {
            var channel_settings = await getChannelSettings(channel);
        } catch (error) {
            console.log(error);
        }
        mandant_id = channel_settings.GetChannelSettingsResult.Data.PlentyMarketId;
        if(mandant_id == sms_plenty_id) {
            console.log("mandant id is " + mandant_id);
            pw_for_token = sms_password;
            usr_for_token = sms_username;
            url_for_token = process.env.SMS_PLENTY_URL;
        }
        try {
            token = await getToken(pw_for_token, usr_for_token, url_for_token);
            let sage_tree = await getTreeCategoriesFromSage(channel);
            let tree_categories = sage_tree.GetCategoryTreeResult.Data.Children;
            await iterateCategoryTree(tree_categories, async (category, level) => {
                console.log(`Category ID: ${category.Id}, Name: ${category.Text}, ParentID: ${category.ParentId} Level: ${level}`);
                let category_db_item = await mysql.getItemInDB(category_table_name, category.Id, 'sage_id');
                let plenty_parent_id = null;
                if (level > 2) {
                    plenty_parent_id = await findPlentyParentID(category.ParentId);
                }
                if (category_db_item.length) {
                    category_db_item = category_db_item[0];
                    console.log("exists in DB");
                    let hasChildren = category.Children.length > 0;
                    var update_payload = [{
                        id: category_db_item.plenty_category_id,
                        level: level - 1,
                        hasChildren: hasChildren,
                        plentyId: n24_plenty_id,
                        type: "item",
                        details: [
                            {
                                parentCategoryId: plenty_parent_id,
                                lang: "de",
                                name: category.Text,
                                description: category.LongText,
                                shortDescription: category.ShortText,
                                position: category.Order,
                                itemListView: "ItemViewCategoriesList",
                                singleItemView: "ItemViewSingleItem",
                                pageView: "PageDesignContent",
                                plentyId: mandant_id
                            }
                        ]
                    }];
                    /* try {
                        var resp = await updateCategory(update_payload);   
                    } catch (error) {
                        console.log(error);
                    }
                    if(resp[0].id) {
                        try {
                            await mysql.updateTimestamp(category_table_name, category_db_item.plenty_category_id, 'plenty_category_id');
                        } catch (error) {
                            console.log(error);
                        }
                    } */
                }
                else {
                    console.log("does not exist in DB - creating category");
                    let hasChildren = category.Children.length > 0;
                    var simple_payload = [{
                        level: level - 1,
                        hasChildren: hasChildren,
                        parentCategoryId: plenty_parent_id,
                        plentyId: mandant_id,
                        type: "item",
                        details: [
                            {
                                parentCategoryId: plenty_parent_id,
                                lang: "de",
                                name: category.Text,
                                description: category.LongText,
                                shortDescription: category.ShortText,
                                position: category.Order,
                                itemListView: "ItemViewCategoriesList",
                                singleItemView: "ItemViewSingleItem",
                                pageView: "PageDesignContent",
                                plentyId: mandant_id
                            }
                        ]
                    }];
                    console.log("creating category");
                    console.log("name: " + category.Text)
                    console.log("sage category id: " + category.Id);
                    try {
                        let response = await createCategory(simple_payload, url_for_token);
                        let plenty_category_id = response[0].id;
                        let plenty_parent_id_response = response[0].parentCategoryId;
                        let dbItem = {
                            'plenty_category_id': plenty_category_id,
                            'sage_id': category.Id,
                            'name': category.Text,
                            'sage_parent_id': category.ParentId,
                            'plenty_parent_id': plenty_parent_id_response
                        }    
                        try {
                            await mysql.insertIntoCategoryMapping(dbItem);    
                        } catch (error) {
                            console.log(error);
                        }
                    } catch (error) {
                        console.log(error);
                    }
                }
                await delay();
            });

        } catch (error) {
            console.log(error);
        }
    } catch (error) {
        console.log(error);
    }
}

async function categoryUpdate(channel) {
    var sage_tree = null;
    var plenty_categories = null;
    try {
        token = await getToken();
        sage_tree = await getTreeCategoriesFromSage(channel);
        sage_main_id = sage_tree.GetCategoryTreeResult.Data.Id;
        plenty_categories = await getCategoryFromPlenty(main_id);
        plenty_categories = JSON.parse(plenty_categories);
        plenty_categories = plenty_categories.entries;
        plenty_categories = await getAllPlentyCategories(plenty_categories, main_id, plenty_categories);

        var tree_categories = sage_tree.GetCategoryTreeResult.Data.Children;
        // iterate sage category tree
        await iterateCategoryTree(tree_categories, async (category, level) => {
            console.log(`Category ID: ${category.Id}, Name: ${category.Text}, ParentID: ${category.ParentId} Level: ${level}`);
            // get the mapping object from json file by sage id
            // var plenty_category_from_mapping = await getCategoryBySageId(category.Id, mapping);
            var plenty_categories_from_mapping = await mysql.getItemInDB(category_table_name, category.Id, 'sage_id');
            var cat_exists = false;
            if (plenty_categories_from_mapping.length) {
                var plenty_category_from_mapping = plenty_categories_from_mapping[0];
                var mappingPlentyCategoryId = plenty_category_from_mapping.plenty_category_id;
                cat_exists = true;
                console.log("from db: " + plenty_category_from_mapping.name);
            }

            var plenty_parent_id = null;
            // check if object exists
            if (plenty_category_from_mapping) {
                // returns true if category exists in plenty already
                cat_exists = await checkCategoryExistsInPlenty(plenty_category_from_mapping.plenty_category_id, plenty_categories);
            }

            // if category tree is at level 2 (level 1 is main level) then sets the parent id to main parent category id
            if (level == 2) {
                plenty_parent_id = null;
            }
            else {
                // gets the plenty parent category id by sage parent id
                plenty_parent_id = await findPlentyParentID(category.ParentId);
            }

            // if category dosent exists, creates it and pushes the response in the mapping
            if (!cat_exists) {
                let hasChildren = category.Children.length > 0;
                var simple_payload = [{
                    level: level - 1,
                    hasChildren: hasChildren,
                    parentCategoryId: plenty_parent_id,
                    plentyId: n24_plenty_id,
                    type: "item",
                    details: [
                        {
                            parentCategoryId: plenty_parent_id,
                            lang: "de",
                            name: category.Text,
                            description: category.LongText,
                            shortDescription: category.ShortText,
                            position: category.Order,
                            itemListView: "ItemViewCategoriesList",
                            singleItemView: "ItemViewSingleItem",
                            pageView: "PageDesignContent",
                            plentyId: n24_plenty_id
                        }
                    ]
                }];
                console.log("creating category");
                console.log(category.Text);
                console.log("sage category id: " + category.Id);
                //let response = await createCategory(simple_payload);

                let plenty_category_id = response[0].id;
                let plenty_parent_id_response = response[0].parentCategoryId;
                let dbItem = {
                    'plenty_category_id': plenty_category_id,
                    'sage_id': category.Id,
                    'name': category.Text,
                    'sage_parent_id': category.ParentId,
                    'plenty_parent_id': plenty_parent_id_response
                }
                //await mysql.deleteItemFromDB(category_table_name, category.Id, 'sage_id');
                //await mysql.insertIntoCategoryMapping(dbItem);
            } else {
                //update existing category
                if (level == 2) {
                    plenty_parent_id = null;
                }
                else {
                    // gets the plenty parent category id by sage parent id
                    plenty_parent_id = await findPlentyParentID(category.ParentId);
                }
                let hasChildren = category.Children.length > 0;
                var update_payload = [{
                    id: mappingPlentyCategoryId,
                    level: level - 1,
                    hasChildren: hasChildren,
                    plentyId: n24_plenty_id,
                    type: "item",
                    details: [
                        {
                            parentCategoryId: plenty_parent_id,
                            lang: "de",
                            name: category.Text,
                            description: category.LongText,
                            shortDescription: category.ShortText,
                            position: category.Order,
                            itemListView: "ItemViewCategoriesList",
                            singleItemView: "ItemViewSingleItem",
                            pageView: "PageDesignContent",
                            plentyId: n24_plenty_id
                        }
                    ]
                }];
                //await updateCategory(update_payload);
                console.log("Plenty Category " + mappingPlentyCategoryId + " updated");
            }
        });
        //await checkPlentyCategories();
        console.log("done");

    } catch (err) {
        console.log(err);
        return;
    }

}

async function checkPlentyCategories() {

    // Prepare some arrays
    var plenty_categories = null;
    var categoriesInSage = [];
    var categoriesInPlenty = [];
    var categoryDelta = [];

    // Get all Plenty Categories for channel
    plenty_categories = await getCategoryFromPlenty(main_id);
    plenty_categories = JSON.parse(plenty_categories);
    plenty_categories = plenty_categories.entries;
    plenty_categories = await getAllPlentyCategories(plenty_categories, main_id, plenty_categories);


    //Extract Category IDs from Plenty
    for (const category of plenty_categories) {
        if (category.id !== uncategorized_id) {
            categoriesInPlenty.push(category.id);
        }
    }

    // Get all Categories from sage
    let sageCategories = await getTreeCategoriesFromSage(main_channel);
    var tree_categories = sageCategories.GetCategoryTreeResult.Data.Children;

    // Get all Plenty IDs based on Sage Category ID
    await iterateCategoryTree(tree_categories, async (category, level) => {
        let sageToPlentyId = await getItemInDB(category_table_name, category.Id, 'sage_id');
        categoriesInSage.push(sageToPlentyId[0].plenty_category_id);
    });

    // Gett differences between both Plenty and Sage
    categoryDelta = categoriesInPlenty.filter(category => !categoriesInSage.includes(category));

    // Delete Categories from Plenty and DB
    for (let i = 0; i < categoryDelta.length; i++) {
        let plentyCategoryId = categoryDelta[i];
        await deleteCategoryFromPlenty(plentyCategoryId);
        await mysql.deleteItemFromDB(category_table_name, plentyCategoryId, 'plenty_category_id');

        console.log("Deleting Category with Plenty Category ID " + plentyCategoryId);
    }
}

function delay(time = 1500) {
    return new Promise(function (resolve) { setTimeout(resolve, time) })
}


async function getChannelSettings(channel = main_channel) {
    var url = process.env.SAGE_URL + "/ChannelSettings/"+channel;
        var resp1;
    try {
        var header = {
            'Content-Type': 'application/json'
        }
        const response = await axios.get(url, { headers: header });
        //console.log('Request successful. Response:', response.data.id);
        resp1 = response.data;
        console.log(resp1);
        return resp1;
    }
    catch (error) {
        console.log(error);
        //console.error('Error in the request:', error.message);
        //throw error;
    }
}

async function isChannelProductSyncActive(channel) {
    try {
        var channel_settings = await getChannelSettings(channel);   
        return channel_settings.GetChannelSettingsResult.Data.ProductSyncActive;
    } catch (error) {
        console.log(error);
    }
   
}

async function checkForItemsNotInSKUList(channel = 'amazon') {
    var mandanten_id = sms_plenty_id;
    var table = "all_items_sms"
    try {
        let items_plenty = await getAllItemsFromPlenty(mandanten_id);
        console.log(items_plenty.length);
        for (let index = 0; index < items_plenty.length; index++) {
            const element = items_plenty[index];
            let plenty_sku = null;
            plenty_sku = element.externalId;
            let variation_id = null;
            let item_id = null;
            let name = null;
            let mainVariationId = null;
            variation_id = element.id;
            item_id = element.itemId;
            mainVariationId = element.mainVariationId;
            name = element.name;
            console.log("adding item: " + variation_id + " item_id: " + item_id + " sku: " + plenty_sku);
            let temp = {
                'id': variation_id,
                'itemId': item_id,
                'plenty_mainVariationId': mainVariationId,
                'name': name,
                'sage_id': null,
                'father_id': null,
                'father_sku': null,
                'sku': plenty_sku,
                'categories': null,
                'variation_attributes': null
            }
            await mysql.insertIntoItemMapping(temp, table);
            if(plenty_sku) {
                console.log(plenty_sku);
            }
            else {
                console.log("no sku " + element.id);
            }
        }    
    } catch (error) {
        console.log(error);
    }
    
}

async function filterItemsWithMandant() {
    let all_items_amazon = await mysql.getAllItemsInDB("all_items_amazon");
    let all_items_n24 = await mysql.getAllItemsInDB("all_items_n24");
    let all_items_sms = await mysql.getAllItemsInDB("all_items_sms");
    let sku_list_amazon = await getAllItemsFromSage("amazon");
    let sku_list_n24 = await getAllItemsFromSage("naturbettwaren24");
    sku_list_amazon = sku_list_amazon.GetSkuListByChannelResult.Data;
    sku_list_n24 = sku_list_n24.GetSkuListByChannelResult.Data;
    for (let index = 0; index < sku_list_amazon.length; index++) {
        const element = sku_list_amazon[index];
        console.log(element);
        await mysql.deleteItemFromDB('all_items', element, 'sku');
        
    }
    for (let index = 0; index < sku_list_n24.length; index++) {
        const element = sku_list_n24[index];
        console.log(element);
        await mysql.deleteItemFromDB('all_items', element, 'sku');
        
    }
    /* for (let index = 0; index < all_items_amazon.length; index++) {
        const element = all_items_amazon[index];
        console.log(element.plenty_id);
        let found_item = false;
        found_item = await mysql.getItemInDB('all_items', element.plenty_id, 'plenty_id');
        if(found_item) {
            console.log("found item in amazon_table, deleting now...");
            await mysql.deleteItemFromDB('all_items', element.plenty_id, 'plenty_id');
        }
    }
    for (let index = 0; index < all_items_n24.length; index++) {
        const element = all_items_n24[index];
        console.log(element.plenty_id);
        let found_item = false;
        found_item = await mysql.getItemInDB('all_items', element.plenty_id, 'plenty_id');
        if(found_item) {
            console.log("found item in amazon_table, deleting now...");
            await mysql.deleteItemFromDB('all_items', element.plenty_id, 'plenty_id');
        }
    } */
    console.log("done");
}

async function addAttributeValuesToDB(attributeId = 2) {
    try {
        console.log("getting attributes for id "+attributeId+" from plenty");
        token = await getToken();
        let url = process.env.PLENTY_URL + "/rest/items/attributes/"+attributeId+"/values";
        var header = {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        }
        var options = {
            url: url,
            json: true,
            method: "GET",
            headers: header
        }
        console.log("getting attributes from plenty...");
        let attributeValues = await getRequest(options);
        console.log(attributeValues.entries.length);
        
        for (let index = 0; index < attributeValues.entries.length; index++) {
            const element = attributeValues.entries[index];
            console.log(element.id);
            console.log(element.backendName);
            var item = {
                'name': element.backendName,
                'id': element.id
            }
            mysql.insertIntoAttributesMapping(item, 'attributes_colors')
        }
        
    } catch (error) {
        console.log(error);
    }
}

async function initialize_sms_items() {
    token = await getToken();
    try {
        var channel = "suckmystraw";
        var attributeId = 2;
        sage_items = await getAllItemsFromSage(channel);
        console.log(sage_items.GetSkuListByChannelResult.Data + " items");
        sage_items = sage_items.GetSkuListByChannelResult.Data;
        let achter = [];
        let zehner = [];
        let zwölfer = [];
        const paper_category = 1041;
        const paper_8_id = 12045;
        let paper_8_item = await mysql.getItemInDB('item_mapping_sms', paper_8_id, 'plenty_item_id');
        paper_8_item = paper_8_item[0];
        const metall_category = 1042;
        for (let index = 0; index < sage_items.length; index++) {
            const element = sage_items[index];
            let sku = null;
            let product_name = null;
            let color = null;
            let object = null;
            let item = await getSingleArticleFromSage(element, channel);
            sku = item.GetPimItemBySkuAndChannelResult.Data.ProductItem.Sku;
            product_name = item.GetPimItemBySkuAndChannelResult.Data.ProductItem.ProductName;
            let size = getSize(sku);
            size = parseInt(size, 10);
            color = extractColor(product_name);
            console.log(product_name);
            console.log(size);
            console.log(color);
            console.log("category: " + item.GetPimItemBySkuAndChannelResult.Data.ProductItem.ChannelSpecificProductDetails.ProductCategories[0]);
            object = {
                "size": size,
                "color": color,
                "name": product_name,
                "sku": sku,
                "category_id": item.GetPimItemBySkuAndChannelResult.Data.ProductItem.ChannelSpecificProductDetails.ProductCategories[0],
                "sage_item": item
            }
            switch (size) {
                case 8:
                    achter.push(object);
                    console.log("kommt in achter");
                    break;
                case 10:
                    zehner.push(object);
                    console.log("kommt in zehn");
                    break;
                case 12:
                    zwölfer.push(object);
                    console.log("kommt in zwölf");
                    break;
                default:
                    break;
            }
        }

        console.log("handling arrays now...");
        //for (let index = 0; index < achter.length; index++) {
        for (let index = 0; index < 1; index++) {
            const element = achter[index];
            let variant_name = null;
            let db_entry = false;
            let product_item = null;
            let sku = null;
            console.log(index + "/"+(achter.length - 1));
            sku = element.sku;
            variant_name = element.sage_item.GetPimItemBySkuAndChannelResult.Data.ProductItem.VariantName;
            product_item = element.sage_item.GetPimItemBySkuAndChannelResult.Data.ProductItem;
            db_entry = await mysql.getItemInDB(sms_item_table_name, sku, 'sku');
            if(db_entry.length) {
                console.log(sku + " item existiert bereits");
            }
            else {
                //console.log("creating item...");
                if(element.category_id == paper_category) {
                    var catArr = [];
                    var catdb_Arr = [];
                    var cat_db_entry = await mysql.getItemInDB(category_table_name, paper_category, 'sage_id');
                    var attributeArr = [];
                    let category = {
                        categoryId: paper_category
                    }
                    let categoryDB = {
                        categoryId: cat_db_entry[0].plenty_category_id,
                        sage_category_id: paper_category
                    }
                    catArr.push(category);
                    catdb_Arr.push(categoryDB);
                    attributeArr = await handleVariantNameDB(element.color, 'attributes_colors', attributeId);
                    //console.log(attributeArr);
                    let resp = await createChildrenArticleInPlentyDB(element.sage_item, catArr, paper_8_item, attributeArr, sms_plenty_id, element.color);
                    let temp = {
                        'id': resp.id,
                        'itemId': resp.itemId,
                        'name': product_item.ProductName,
                        'sage_id': product_item.ChannelSpecificProductDetails.Id,
                        'father_id': product_item.ChannelSpecificProductDetails.FatherId,
                        'father_sku': product_item.ChannelSpecificProductDetails.FatherSku,
                        'sku': product_item.Sku,
                        'categories': catdb_Arr,
                        'variation_attributes': attributeArr
                    }
                    console.log("inserting into db now");
                    await mysql.insertIntoItemMapping(temp, table_name);
                    await createImageFromQueue(sku, 'suckmystraw');
                }
            }
        }

    } catch (error) {
        console.log(error);
    }
}

function extractColor(name) {
    // Ensure the input is a string
    if (typeof name !== 'string') {
        throw new Error('Input must be a string');
    }

    // Convert to lowercase for case-insensitive matching
    const lowerName = name.toLowerCase();

    // Search for a matching color in the name
    for (const color of straw_colors) {
        if (lowerName.includes(color)) {
            return color; // Return the color as found in the array
        }
    }

    // If no color is found
    return null;
}

function getSize(str) {
    // Ensure the input is a string
    if (typeof str !== 'string') {
        throw new Error('Input must be a string');
    }
    
    // Return the last two characters
    return str.slice(-2);
}

const straw_colors = [
    "schwarz", "blau", "dunkelgrün", "hellgrün", "gelb", "hellpink", "petrol", "mint",
    "metallic blau", "metallic grün", "metallic rot", "metallic silber", "neon grün",
    "orange", "pink", "purple", "rot", "weiß"
];