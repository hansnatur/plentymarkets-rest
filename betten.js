const express = require('express');
const app = express();
const request = require('request');
const amqp = require('amqplib/callback_api');
const { WebClient } = require('@slack/web-api');
var bodyParser = require('body-parser');
const send = require('send');
app.use(express.json());
app.use(bodyParser.json());
require('dotenv').config();

let queue = process.env.RABBITMQ_QUEUE;
let thechannel;
const sage_article_url = "/PimItem/Sku/5399/Channel/naturbettwaren24";
let username = process.env.PLENTY_USERNAME;
let password = process.env.PLENTY_PASSWORD;
const categoryParentId = 131;
let sagearticles = [];
let sagecategories = [];
let plentycategories = [];
const sage_url = process.env.SAGE_URL;
const data = JSON.stringify({
    username: username,
    password: password
});
const slack_token = process.env.SLACK;
const slack = new WebClient(slack_token);

let token = "";



(async () => {
    try {
        await connectRabbitMq();
        //console.log("sendet plenty message...");
        await sendMessage(getGermanTimestamp() + " starting naturbettwaren24 worker... ");
        // 1. get articles from sage
        console.log("get articles from sage...");
        sagearticles = await getArticlesFromSage();
        console.log("done");
        // 2. get plenty token
        console.log("get token from plentymarket...");
        var a = await getToken();
        console.log("done");
        var out = JSON.parse(a);
        if ("error" in out) {
            var msg = "Error in getToken"
            thechannel.sendToQueue(queue, Buffer.from(msg));
            throw new Error("Error in getToken");
        }
        token = out["access_token"];
        // 3. get categories from sage

        // 4. get categories from plenty

        // 5. check for new categories and create if new

        // 6. get articles from plenty

        // 7. check for new articles and create if new

        // 8. check for difference in stock and update if different
        
        

    } catch (e) {
        console.error(e);
    } finally {
        console.log('Done');
    }
})();

function getArticlesFromSage() {
    let url = sage_url+sage_article_url;
    let header = {
        'Content-Type': 'application/json; charset=utf8'
    };
    var options = {
        url: url,
        json: true,
        method: "GET",
        encoding: "utf-8",
        headers: header
    }
    return getRequest(options);
}

async function postRequest(params) {
    try {
        return new Promise(function (resolve, reject) {
            request.post(params, (err, res, body) => {
                if (err) {
                    var msg = "Error in POSTRequest " + err;
                    thechannel.sendToQueue(queue, Buffer.from(msg));
                    return reject(new Error('statusCode=' + res));
                }
                if (body) {
                    resolve(body);
                }
            });
        });
    }
    catch (e) {
        var msg = "Error in postRequest " + e;
        await sendMessage(getGermanTimestamp() + msg);
        thechannel.sendToQueue(queue, Buffer.from(msg));
        console.error(e);
    } finally {
        //console.log('We do cleanup here');
    }
}

async function getRequest(params) {
    try {
        return new Promise(function (resolve, reject) {
            request.get(params, (err, res, body) => {
                if (err) {
                    var msg = "Error in GET httpRequest " + err;
                    console.log(msg);
                    //thechannel.sendToQueue(queue, Buffer.from(msg));
                    return reject(new Error(err + ' statusCode=' + res));
                }
                if (body) {
                    resolve(body);
                }
            });
        });
    }
    catch (e) {
        var msg = "Error in httpRequest " + e;
        await sendMessage(getGermanTimestamp() + msg);
        thechannel.sendToQueue(queue, Buffer.from(msg));
        console.error(e);
    } finally {
        //console.log('We do cleanup here');
    }
}

function getToken() {
    var header = {
        'Content-Type': 'application/json',
        'Content-Length': data.length
    }
    var options = {
        url: process.env.PLENTY_URL + "/rest/login",
        port: 443,
        path : '/rest/login',
        method: 'POST',
        body: data,
        headers: header
    }
    return postRequest(options);
}

async function connectRabbitMq() {
    return new Promise(function (resolve, reject) {
        amqp.connect(process.env.CLOUDAMQP_URL, function (error0, connection) {
            if (error0) {
                reject();
                throw error0;
            }
            connection.createChannel(function (error1, channel) {
                if (error1) {
                    reject();
                    throw error1;
                }

                var msg = 'starting plentystock update';
                thechannel = channel;
                thechannel.assertQueue(queue, {
                    durable: false
                });

                resolve();
            });
        });
    });
}

async function sendMessage(slacktext) {
    if(slacktext.length > 0) {
        try {
            const result = await slack.chat.postMessage({
              channel: '#plentymarket', // Ersetze 'channel-name' durch den Namen des gewünschten Channels
              text: slacktext,
            });
          } catch (error) {
            console.error('Fehler beim Senden der Nachricht:', error);
          }
    }
}

function getGermanTimestamp() {
    // Aktuelles Datum und Uhrzeitobjekt erstellen
    var jetzt = new Date();

    // Datum formatieren (TT.MM.JJJJ)
    var tag = jetzt.getDate();
    var monat = jetzt.getMonth() + 1; // Monate sind nullbasiert, daher +1
    var jahr = jetzt.getFullYear();

    // Uhrzeit formatieren (HH:MM:SS)
    var stunden = jetzt.getHours();
    var minuten = jetzt.getMinutes();
    var sekunden = jetzt.getSeconds();

    // Führende Nullen hinzufügen, wenn nötig
    tag = (tag < 10) ? '0' + tag : tag;
    monat = (monat < 10) ? '0' + monat : monat;
    stunden = (stunden < 10) ? '0' + stunden : stunden;
    minuten = (minuten < 10) ? '0' + minuten : minuten;
    sekunden = (sekunden < 10) ? '0' + sekunden : sekunden;

    // Das Datum und die Uhrzeit im deutschen Format zusammenstellen
    return tag + '.' + monat + '.' + jahr + ' ' + stunden + ':' + minuten + ':' + sekunden;
}