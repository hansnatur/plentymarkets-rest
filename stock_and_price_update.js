const express = require('express');
const app = express();
const request = require('request');
const amqp = require('amqplib/callback_api');
var bodyParser = require('body-parser');
app.use(express.json());
app.use(bodyParser.json());
require('dotenv').config();

let queue = process.env.RABBITMQ_QUEUE;
let thechannel;
let username = process.env.PLENTY_USERNAME;
let password = process.env.PLENTY_PASSWORD;
const data = JSON.stringify({
    username: username,
    password: password
});
let token = "";


async function connectRabbitMq() {
    return new Promise(function(resolve, reject) {
        amqp.connect(process.env.CLOUDAMQP_URL, function(error0, connection) {
            if (error0) {
                reject();
                throw error0;
            }
            connection.createChannel(function(error1, channel) {
                if (error1) {
                    reject();
                    throw error1;
                }
        
                var msg = 'starting plentystock update';
                thechannel = channel;
                thechannel.assertQueue(queue, {
                    durable: false
                });
                
                resolve();
            });
        });
    });   
}

function getOptions(url, port, path, method, body, headers) {
    if(body) {
        options = {
            url: url,
            port: port,
            path: path,
            method: method,
            body: body,
            headers: headers
        }
    }
    else {
        options = {
            url: url,
            port: port,
            path: path,
            method: method,
            headers: headers
        }
    }
    return options;
}



function getToken() {
    var header = {
        'Content-Type': 'application/json',
        'Content-Length': data.length
    }
    var tokenOptions = getOptions(process.env.PLENTY_URL+"/rest/login", 443, '/rest/login', 'POST', data, header);
    return httpRequest(tokenOptions, true, 'POST');    
}

function getItems() {
    var header = {
        'Content-Type': 'application/json; charset=utf8',
        'Authorization': 'Bearer '+token
    }
    var itemsOptions = getOptions(process.env.PLENTY_URL+"/rest/items", 443, '/rest/items', 'GET', data, header);
    return httpRequest(itemsOptions, false);
}

function getAllItems(page) {
    var header = {
        'Content-Type': 'application/json; charset=utf8',
        'Authorization': 'Bearer '+token
    }
    var itemsOptions = getOptions(process.env.PLENTY_URL+'/rest/items?page='+page, 443, '/rest/items?page='+page, 'GET', data, header);
    return httpRequest(itemsOptions, false);
}

function getRequest(url) {
    var header = {
        'Content-Type': 'application/json; charset=utf8',
        'Authorization': 'Bearer '+token
    }
    var variationOptions = {
        url: url,
        json: true,
        method: "GET",
        encoding: "utf-8",
        headers: header
    }
    return httpRequest(variationOptions, false);
}

function removeVariationRelationships(variations, relationships) {

    var tagVariationIds = relationships;
	for (let index = 0; index < tagVariationIds.length; index++) {
		const element = tagVariationIds[index];
		for (let varis = 0; varis < variations.length; varis++) {
			if(variations[varis].id == element) {
				let variationIndex = varis;
				variations.splice(variationIndex, 1);
			}
		}
    }
    return variations;
}


function getSkusFromVariations(variations) {
	var Skus = [];
	for (var i = 0; i < variations.length; i++) {
		if(variations[i].externalId) {
			Skus.push(variations[i].externalId);
		}
	}
	return Skus;
}
function delay(){
    return new Promise(function(resolve){ setTimeout(resolve, 2000)})
}

(async() => {
    try {
    await connectRabbitMq();
    
    // 1. get token
    console.log("getting token...");
    var a = await getToken();
    var out = JSON.parse(a);
    if("error" in out) {
        var msg = "Error in getToken"
        thechannel.sendToQueue(queue, Buffer.from(msg));
        throw new Error("Error in getToken");
    }
    console.log("done");
    
    token = out["access_token"];
    console.log(token);

    var articles = [];
    var variations = [];
    var warehouseIds;
    var blockedWarehouses = [];
    var urls = [];
    var prices = [];
    var relationships = [];
    var skus = [];
    var stocks = [];
    var stocksBundles = [];
    var bundles = [];
    // 2. get items 
    console.log("getting items, checking pages");
    var b = await getItems();
    console.log("done");
    out = JSON.parse(b);
    var pages = out.lastPageNumber;
    // 3. get more than 100 items
    console.log("getting items from all pages...")
    for (let index = 1; index <= pages; index++) {

        var c = await getAllItems(index);

        out = JSON.parse(c);
        for (let index = 0; index < out['entries'].length; index++) {
            const element = out['entries'][index];
            articles.push(element);	
        }
    }
    console.log("done");

    // 4. create url for each article
    for(var i = 0; i < articles.length; i++) {
        //console.log(i + ". generate url...")
        urls.push(process.env.PLENTY_URL+"/rest/items/" + articles[i].id + "/variations");
    }
    console.log("url length " + urls.length);

    // 5. get variations 
    console.log("getting variations...")
    for (let index = 0; index < urls.length; index++) {
        const element = urls[index];

        var d = await getRequest(element);

        //console.log(d);
        if(d.entries) {
            for (let index = 0; index < d.entries.length; index++) {
                const element = d.entries[index];
                variations.push(element);
            }
        }
        console.log(index + "...");
    }
    console.log("done");

    // 6. get tagvariations
    console.log("getting relationhips...")
    var e = await getRequest(process.env.PLENTY_URL+'/rest/tags/relationships');
    for (let index = 0; index < e.length; index++) {
        const element = e[index]['relationshipValue'];
        relationships.push(element);
    }
    console.log("done");

    // 7. removing relations
    variations = removeVariationRelationships(variations, relationships);
    console.log("variations without relations " + variations.length);

    // 8. generating sku array
    skus = getSkusFromVariations(variations);

    // 9. get blocked warehouseids
    warehouseIds = await getRequest(process.env.SAGE_URL+'/warehouseitems');
    warehouseIds = warehouseIds.GetWarehouseItemsResult.Data;
    for (let index = 0; index < warehouseIds.length; index++) {
        const element = warehouseIds[index];
        if(element.IsStockBlocked) {
            console.log(element.WarehouseId);
            blockedWarehouses.push(element.WarehouseId);
        }
    }		

    // 10. 
    console.log("getting stocks from sage...");
    for (let index = 0; index < skus.length; index++) {
        var element = skus[index];
        if(index%5 == 0) {
            console.log(index+'...');
        }
        var f = await getRequest(process.env.SAGE_URL+"/pimitem/Sku/"+skus[index]);
        f = f.GetPimItemBySkuResult.Data;
        if(f.ProductItem.IsBundle) {
            var productItem = f.ProductItem;
            var tempArray = [];
            for (let index = 0; index < productItem.BundleItems.length; index++) {
                var otherArray = [];
                otherArray['amount'] = productItem.BundleItems[index].Amount;
                otherArray['bundleSku'] = element;
                otherArray['sku'] = productItem.BundleItems[index].Sku;
                tempArray.push(otherArray);
            }
            bundles.push(tempArray);
            //stocks[element] = "bundle " +element;
        }
        else {
            if(f.StockItems.length > 0) {
                var tempStock = 0;
                var stockValue = 0;
                var actualStock = 0;
                
                for (var b = 0; b < f.StockItems.length; b++) {	
                    if(!(blockedWarehouses.includes(f.StockItems[b].WarehouseId))) {
                        //stockValue = parseInt(f.StockItems[b].StockAmount);
                        tempStock = parseInt(f.StockItems[b].StockAmount);
                        stockValue += tempStock;
                    }
                }
                if(stockValue < 5) {
                    stockValue = 0;
                }
                
                var valuesToPush = [];
                valuesToPush['sku'] = element;
                valuesToPush['stock'] = stockValue;
                valuesToPush['actualStock'] = actualStock;
                stocks.push(valuesToPush);
            }
            else {
                var valuesToPush = [];
                valuesToPush['sku'] = element;
                valuesToPush['stock'] = 0;
                valuesToPush['actualStock'] = 0;
                stocks.push(valuesToPush);
            }
        }
    }
    console.log("done");
    console.log('sku array: ' + skus.length);
    console.log('stock array: ' + stocks.length);
    console.log('bundles array: ' + bundles.length);

    // 11. set up bundle stock array
    console.log("setting up bundle stocks");
    for (let index = 0; index < bundles.length; index++) {
        var bundleSku = bundles[index][0]['bundleSku'];
        var bundleStock = 0;
        for (let schmindex = 0; schmindex < bundles[index].length; schmindex++) {
            var partSkuFound = false;
            var partSku = bundles[index][schmindex]['sku'];
            var partAmount = bundles[index][schmindex]['amount'];
            var actualAmount = 0;
            var tempStock = 0;
            var partStock = 0;
            for (let a = 0; a < stocks.length; a++) {
                if(stocks[a]['sku'] === partSku) {
                    actualAmount = stocks[a]['actualStock'];
                    partSkuFound = true;
                }
            }
            if(!partSkuFound) {
                //console.log("getting stock for " + partSku);
                var out = await getRequest(process.env.SAGE_URL+"/pimitem/Sku/"+partSku)
                out = out.GetPimItemBySkuResult.Data.StockItems;
                for (var b = 0; b < out.length; b++) {	
                    if(!(blockedWarehouses.includes(out[b].WarehouseId))) {
                        //stockValue = parseInt(f.StockItems[b].StockAmount);
                        partStock = parseInt(out[b].StockAmount);
                        actualAmount += partStock;
                    }
                }
                //console.log("nothing found for " + partSku)
            }
            tempStock = Math.floor(actualAmount / partAmount);
            if(schmindex > 0) {
                if(tempStock >= 5) {
                    bundleStock = tempStock;
                }
                else {
                    bundleStock = 0;
                }    
            }
            else {
                if(tempStock >= 5) {
                    bundleStock = tempStock;
                }
                else {
                    bundleStock = 0;
                }
            }
        }
        var tempBundleArray = [];
        tempBundleArray['sku'] = bundleSku;
        tempBundleArray['stock'] = bundleStock;
        stocksBundles.push(tempBundleArray);
        var valuesToPush = [];
        valuesToPush['sku'] = bundleSku;
        valuesToPush['stock'] = bundleStock;
        valuesToPush['actualStock'] = 0;
        stocks.push(valuesToPush);
    }
    console.log("bundles: " + stocksBundles.length);
    console.log("stock now: " + stocks.length);
    var articles;
    articles = setArticleArray(variations, stocks);
    console.log("done. Lenght = " + articles.length);

    // 12. update stock 
    console.log("");
    console.log("updating stock now...");
    //"rest/items/"+articleStocks[x][0]+"/variations/"+articleStocks[x][1]+"/stock/correction"
    for (let index = 0; index < articles.length; index++) {
        var element = articles[index];
        var stock = element['stock'];
        if(typeof stock == 'undefined') {
            stock = 0;
        }
        var header = {
            "Content-Type": "application/json; charset=utf8",
            "Authorization": "Bearer "+token
        };
        var body = {
            "warehouseId": element['mainWarehouseId'],
            "quantity": stock,
            "reasonId": 301,
            "storageLocationId": 0
        };
        var options = {
            url: process.env.PLENTY_URL+"/rest/items/"+element['itemId']+"/variations/"+element['id']+"/stock/correction",
            method: "PUT",
            headers: header,
            json: true,
            encoding: "utf-8",
            body: body
        }
        var out = await httpRequest(options, true, 'PUT');
        console.log('--');
        console.log("item_id: " +element['itemId'])
        console.log("id: " + element['id']);
        console.log("sku: " + element['sku']);
        console.log("stock: " + stock);
        console.log('**');
        await delay();
    }
    
    
    // 13. get prices from sage by sku
    console.log("getting prices");
    for(let blup = 0; blup < articles.length; blup++) {
        let element = articles[blup];
        var sku = element['sku'];
        if(sku) {
            var options = {
                url: process.env.SAGE_URL+"/PriceItems/"+sku,
                json: true,
                method: "GET",
                encoding: "utf-8",
          };
          var out = await httpRequest(options, true, 'GET');
          //console.log(out);
          var data = out.GetPriceItemsBySkuResult.Data
          if(data.length) {
            for (var i = 0; i < data.length; i++) {
                if(data[i].ValidFromAmount == 0) {
                    var objectTest = {
                            price: data[i].Price,
                            id: element['id'],
                            itemId: element['itemId'],
                            pricelistId: data[i].PriceListId
                        }
                    /* console.log(sku + "++++");
                    console.log("preis: " + data[i].Price); */
                    prices.push(objectTest);
                }
                else {
                    //console.log("Valid From Amount not 0 ");
                }
            };
          }
        }
    }
    console.log("done");
    console.log("preis array: " + prices.length);

    var priceRange = prices.length / 50;
    var pricesBulk = [];

    for(var e = 0; e < priceRange; e++) {
        var temp = [];
        if(e < 0) {
            var ende = (e+1)*50
            var start = e;
        }
        else {
            var ende = (e+1)*50
            var start = e*50;
        }  
        for(var f = start; f < ende; f++) {
            var element = prices[f];
            if(element) {
                //console.log(element);
                if(element.hasOwnProperty('pricelistId')) {
                    if (element['pricelistId'] == 1) {
                        var singlePrice = {
                            "variationId": element['id'],
                            "salesPriceId": 1,
                            "price": element['price']
                        }
                        temp.push(singlePrice);
                    }
                }
            }
        }
        pricesBulk.push(temp);
    }
    

    // 14. delete aktionprices 
    console.log("deleting aktionspreise ")
   /*  for(var t = 0; t < prices.length; t++) {
        var element = prices[t];
        var salesPriceID = 1;
        if(element['pricelistId'] == 5) {
            salesPriceID = 3;
            console.log("delete aktionspreis");
            console.log(element['id']);
            let options = {
                url: process.env.PLENTY_URL + "/rest/items/"+element['itemId']+"/variations/"+element['id']+"/variation_sales_prices",
                json: true,
                method: "DELETE",
                encoding: "utf-8",
                headers : {
                    'Content-Type': 'application/json; charset=utf8',
                    'Authorization': 'Bearer '+token,
                }	
            };
            var out = await httpRequest(options, true, 'DELETE');
            console.log(out);
        }
        /* else {
            salesPriceID = 1;
            // console.log("update preis");
            //console.log(element['id']);
            //console.log(element['price']);
            let options = {
                url: process.env.PLENTY_URL + "/rest/items/variations/variation_sales_prices",
                json: true,
                method: "PUT",
                encoding: "utf-8",
                body: 
                    [
                        {
                            "variationId": element['id'],
                            "salesPriceId": salesPriceID,
                            "price": element['price']
                        }
                    ]
                ,
                headers : {
                    'Content-Type': 'application/json; charset=utf8',
                    'Authorization': 'Bearer '+token,
                }	
            };
            var out = await httpRequest(options, true, 'PUT');
            console.log(out);
            await delay();
        } 
    } */
    // 15. updating prices
    console.log("updating prices");
    for(var g = 0; g < pricesBulk.length; g++) {
        var element = pricesBulk[g];
        console.log("+++++++++++++++ " + g +"+++++++++++++++");
        salesPriceID = 1;
            // console.log("update preis");
            //console.log(element['id']);
            //console.log(element['price']);
            let options = {
                url: process.env.PLENTY_URL + "/rest/items/variations/variation_sales_prices",
                json: true,
                method: "PUT",
                encoding: "utf-8",
                body: element,
                headers : {
                    'Content-Type': 'application/json; charset=utf8',
                    'Authorization': 'Bearer '+token,
                }	
            };
            var out = await httpRequest(options, true, 'PUT');
            console.log(out);
            await delay();
    }

    console.log("done!");
} catch (e) {
    console.error(e);
} finally {
    //console.log('We do cleanup here');
}
})();


async function httpRequest(params, postData, type = 'GET') {
    try {
        return new Promise(function(resolve, reject) {
            if(type === 'POST') {
                request.post(params, (err, res, body) => {
                    if(err) {
                        var msg = "Error in POST httpRequest "+err;
                        thechannel.sendToQueue(queue, Buffer.from(msg));
                        return reject(new Error('statusCode=' + res));
                    }
                    if(body) {
                        resolve(body);
                    }
                });
            }
            else if(type === 'PUT') {
                //console.log("in put");
                request.put(params, (err, res, body) => {
                    if(err) {
                        var msg = "Error in PUT httpRequest "+err;
                        thechannel.sendToQueue(queue, Buffer.from(msg));
                        return console.log(err);
                    }
                    if(body) {
                        //console.log(res);
                        //console.log(body);
                        resolve(body);
                        return body;
                        /* console.log("BODY: ");
                        console.log(body); */
                    }
                    //console.log(err);
                    //console.log(res);
                    //console.log(body);
                    /* if(err) {
                        return reject(new Error('statusCode=' + res));
                    }
                    if(body) {
                        resolve(body);
                    } */
                });
            }
            else if(type === 'DELETE') {
                //console.log("in put");
                request.delete(params, (err, res, body) => {
                    if(err) {
                        var msg = "Error in DELETE httpRequest "+err;
                        thechannel.sendToQueue(queue, Buffer.from(msg));
                        return console.log(err);
                    }
                    if(body) {
                        //console.log(res);
                        //console.log(body);
                        resolve(body);
                        return body;
                        /* console.log("BODY: ");
                        console.log(body); */
                    }
                    //console.log(err);
                    //console.log(res);
                    //console.log(body);
                    /* if(err) {
                        return reject(new Error('statusCode=' + res));
                    }
                    if(body) {
                        resolve(body);
                    } */
                });
            }
            else if(type === 'GET') {
                request.get(params, (err, res, body) => {
                    if(err) {
                        var msg = "Error in GET httpRequest "+err;
                        thechannel.sendToQueue(queue, Buffer.from(msg));
                        return reject(new Error(err + 'statusCode=' + res));
                    }
                    if(body) {
                        resolve(body);
                    }
                });	
            }
        }); 
    }
    catch (e) {
        var msg = "Error in httpRequest "+e;
        thechannel.sendToQueue(queue, Buffer.from(msg));
        console.error(e);
    } finally {
        //console.log('We do cleanup here');
    }
}

function getStocksBySku(stocks, sku) {
    for (let index = 0; index < stocks.length; index++) {
        const element = stocks[index];
        if(element['sku'] == sku) {
            return element['stock'];
        }
    }
}

function setArticleArray(variations, stocks) {
	articleStocks = [];
	console.log("set Article Array");
	var size = variations.length;
	for(var i = 0; i < size; i++) {
		//console.log(i);
		var arrayToPush = [];
		if(variations[i].externalId) {
			var sku = variations[i].externalId;
            arrayToPush['sku'] = sku;
            console.log("sku: " + sku);
			arrayToPush['itemId'] = variations[i].itemId;
			arrayToPush['id'] = variations[i].id;
			arrayToPush['mainVariationId'] = variations[i].mainVariationId;
			arrayToPush['mainWarehouseId'] = variations[i].mainWarehouseId;
            var stock = getStocksBySku(stocks, sku);
			if(stock < 5) {
				arrayToPush[4] = 0;	
			}
			else if(stock < 10) {
				arrayToPush['stock'] = stock;
			}
			else {
				arrayToPush['stock'] = 10;
			}
            if(typeof stock == 'undefined') {
                stock = 0;
            }
            arrayToPush['stock'] = stock;
            console.log("stock: " + arrayToPush['stock']);			
			articleStocks.push(arrayToPush);
		}
		else {
			var sku = variations[i].externalId;
            arrayToPush['sku'] = sku;
			arrayToPush['itemId'] = variations[i].itemId;
			arrayToPush['id'] = variations[i].id;
			arrayToPush['mainVariationId'] = variations[i].mainVariationId;
			arrayToPush['mainWarehouseId'] = variations[i].mainWarehouseId;
            console.log('----------------');
            console.log("keine sku: " + sku);
            console.log("item id: " +variations[i].itemId);
            console.log('id: '+ variations[i].id)
            console.log('**********');
			arrayToPush['stock'] = 0;
			articleStocks.push(arrayToPush);
		}
	}	
    return articleStocks;
}