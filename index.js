const express = require('express');
const app = express();
const request = require('request');
const amqp = require('amqplib/callback_api');
const readline = require('readline');
const fs = require('fs');
const { WebClient } = require('@slack/web-api');
var bodyParser = require('body-parser');
const send = require('send');
const { exit } = require('process');
app.use(express.json());
app.use(bodyParser.json());
require('dotenv').config();

process.env.TZ = 'Europe/Berlin';

let queue = process.env.RABBITMQ_QUEUE;
let thechannel;
let username = process.env.PLENTY_USERNAME;
let password = process.env.PLENTY_PASSWORD;
let startTime = null;
let end = null;
const minStock = 0;
const data = JSON.stringify({
    username: username,
    password: password
});
const slack_token = process.env.SLACK;
const slack = new WebClient(slack_token);

let token = "";

async function sendMessage(slacktext) {
    if(slacktext.length > 0) {
        try {
            const result = await slack.chat.postMessage({
              channel: '#plentymarket', // Ersetze 'channel-name' durch den Namen des gewünschten Channels
              text: slacktext,
            });
          } catch (error) {
            console.error('Fehler beim Senden der Nachricht:', error);
          }
    }
    await delay();
}

function askQuestion(query) {
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
    });

    return new Promise(resolve => rl.question(query, ans => {
        rl.close();
        resolve(ans);
    }))
}

async function sendMessagestock(slacktext) {
    if(slacktext.length > 0) {
        try {
            const result = await slack.chat.postMessage({
              channel: '#plentystock', // Ersetze 'channel-name' durch den Namen des gewünschten Channels
              text: slacktext,
            });
          } catch (error) {
            console.error('Fehler beim Senden der Nachricht:', error);
          }
    }
}


async function connectRabbitMq() {
    return new Promise(function (resolve, reject) {
        amqp.connect(process.env.CLOUDAMQP_URL, function (error0, connection) {
            if (error0) {
                reject();
                throw error0;
            }
            connection.createChannel(function (error1, channel) {
                if (error1) {
                    reject();
                    throw error1;
                }

                var msg = 'starting plentystock update';
                thechannel = channel;
                thechannel.assertQueue(queue, {
                    durable: false
                });

                resolve();
            });
        });
    });
}

function getOptions(url, port, path, method, body, headers) {
    if (body) {
        options = {
            url: url,
            port: port,
            path: path,
            method: method,
            body: body,
            headers: headers
        }
    }
    else {
        options = {
            url: url,
            port: port,
            path: path,
            method: method,
            headers: headers
        }
    }
    return options;
}



function getToken() {
    var header = {
        'Content-Type': 'application/json',
        'Content-Length': data.length
    }
    var tokenOptions = getOptions(process.env.PLENTY_URL + "/rest/login", 443, '/rest/login', 'POST', data, header);
    return httpRequest(tokenOptions, true, 'POST');
}

function getItems() {
    var header = {
        'Content-Type': 'application/json; charset=utf8',
        'Authorization': 'Bearer ' + token
    }
    var options = {
        url: process.env.PLENTY_URL + "/rest/items/variations",
        port: 443,
        path: '/rest/items/variations',
        method: 'GET',
        headers: header
    } 
    var itemsOptions = getOptions(process.env.PLENTY_URL + "/rest/items/variations", 443, '/rest/items/variations', 'GET', data, header);
    return httpRequest(options, false);
}

function getSpecificItem(id) {
    var header = {
        'Content-Type': 'application/json; charset=utf8',
        'Authorization': 'Bearer ' + token
    }
    var options = {
        url: process.env.PLENTY_URL + "/rest/items/"+id+"/variations",
        port: 443,
        path: '/rest/items/variations',
        method: 'GET',
        headers: header
    } 
    return httpRequest(options, false);
}

async function getAllItemsById(id, page) {
    var header = {
        'Content-Type': 'application/json; charset=utf8',
        'Authorization': 'Bearer ' + token
    }
    var options = {
        url: process.env.PLENTY_URL + '/rest/items/'+id+'/variations?page=' + page,
        port: 443,
        path: '/rest/items/'+id+'/variations?page=' + page,
        method: 'GET',
        headers: header
    } 
    return httpRequest(options, false);
}

async function getAllItems(page) {
    var header = {
        'Content-Type': 'application/json; charset=utf8',
        'Authorization': 'Bearer ' + token
    }
    var options = {
        url: process.env.PLENTY_URL + '/rest/items/variations?page=' + page,
        port: 443,
        path: '/rest/items/variations?page=' + page,
        method: 'GET',
        headers: header
    } 
    var itemsOptions = getOptions(process.env.PLENTY_URL + '/rest/items/variations?page=' + page, 443, '/rest/items/variations?page=' + page, 'GET', data, header);
    return httpRequest(options, false);
}

function getRequest(url) {
    var header = {
        'Content-Type': 'application/json; charset=utf8',
        'Authorization': 'Bearer ' + token
    }
    var variationOptions = {
        url: url,
        json: true,
        method: "GET",
        encoding: "utf-8",
        headers: header
    }
    return httpRequest(variationOptions, false);
}

function removeVariationRelationships(articles, relationships) {

    var tagVariationIds = relationships;
    for (let index = 0; index < tagVariationIds.length; index++) {
        const element = tagVariationIds[index];
        for (let varis = 0; varis < articles.length; varis++) {
            if (articles[varis].id == element) {
                let variationIndex = varis;
                articles.splice(variationIndex, 1);
            }
        }
    }
    return articles;
}


async function getSkusFromVariations(articles) {
    var Skus = [];
    var itemIDForSlack = "";
    var textForSlack = "";
    for (var i = 0; i < articles.length; i++) {
        if (articles[i].externalId) {
            if(articles[i].externalId.length) {
                Skus.push(articles[i].externalId);
            }
        }
        /*else {
            if(!articles[i].isMain) {
                if (itemIDForSlack !== articles[i].itemId && textForSlack.length > 0) {
                    await sendMessage("keine external_Id gefunden varianten_id: "+textForSlack + " --> ARTIKEL ID: " + itemIDForSlack);
                    textForSlack = "";
                }
                else {
                    textForSlack += articles[i].id+", ";
                }
                itemIDForSlack = articles[i].itemId;
            }
        }*/
    }
    return Skus;
}
function delay() {
    return new Promise(function (resolve) { setTimeout(resolve, 1500) })
}

function getGermanTimestamp() {
    // Aktuelles Datum und Uhrzeitobjekt erstellen
    var jetzt = new Date();

    // Datum formatieren (TT.MM.JJJJ)
    var tag = jetzt.getDate();
    var monat = jetzt.getMonth() + 1; // Monate sind nullbasiert, daher +1
    var jahr = jetzt.getFullYear();

    // Uhrzeit formatieren (HH:MM:SS)
    var stunden = jetzt.getHours();
    var minuten = jetzt.getMinutes();
    var sekunden = jetzt.getSeconds();

    // Führende Nullen hinzufügen, wenn nötig
    tag = (tag < 10) ? '0' + tag : tag;
    monat = (monat < 10) ? '0' + monat : monat;
    stunden = (stunden < 10) ? '0' + stunden : stunden;
    minuten = (minuten < 10) ? '0' + minuten : minuten;
    sekunden = (sekunden < 10) ? '0' + sekunden : sekunden;

    // Das Datum und die Uhrzeit im deutschen Format zusammenstellen
    return tag + '.' + monat + '.' + jahr + ' ' + stunden + ':' + minuten + ':' + sekunden;
}


async function synchronize() {
    try {
        //console.log("sendet plenty message...");
        await sendMessage(getGermanTimestamp() + " !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  starting plenty worker... ");
        await sendMessagestock(getGermanTimestamp() + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  starting plenty worker...");
        startTime = getGermanTimestamp();
        // 1. get token
        console.log("getting token...");
        var a = await getToken();
        var out = JSON.parse(a);
        if ("error" in out) {
            var msg = "Error in getToken"
            thechannel.sendToQueue(queue, Buffer.from(msg));
            throw new Error("Error in getToken");
        }
        console.log("done");

        token = out["access_token"];
        console.log(token);

        var articles = [];
        var variations = [];
        var warehouseIds;
        var blockedWarehouses = [];
        var urls = [];
        var prices = [];
        var relationships = [];
        var skus = [];
        var stocks = [];
        var stocksBundles = [];
        var bundles = [];
        var test_item = 8824;
        // 2. get items 
        console.log("getting items, checking pages");
        var b = await getItems();
        //var b = await getSpecificItem(test_item);
        console.log("done");
        out = JSON.parse(b);
        //console.log(out);
        var pages = out.lastPageNumber;
        // 3. get more than 100 items
        console.log("getting items from all pages...");
        console.log("numer of pages: " + pages);
        for (let index = 1; index <= pages; index++) {
        //for (let index = 7; index <= 10; index++) {
            var c = await getAllItems(index);
            await delay();
            //var c = await getAllItemsById(test_item, index)
            out = JSON.parse(c);
            try {
                for (let index = 0; index < out['entries'].length; index++) {
                    console.log(out['entries'][index].id);
                    const element = out['entries'][index];
                    
                    articles.push(element);
                    console.log("variationId: " + element.id);
                    
                    //console.log("from 3. " +out['entries'][index]);
                }
            } catch (error) {
                console.log(error);
            }
            
        }
        console.log("done");

        // 4. create url for each article
        for (var i = 0; i < articles.length; i++) {
            //console.log(i + ". generate url...")
            urls.push(process.env.PLENTY_URL + "/rest/items/" + articles[i].id + "/variations");
            //console.log("urls pushing articles[i].id = " + articles[i].id);
        }
        console.log("url length " + urls.length);

       

        // 6. get tagvariations
        console.log("getting relationhips...")
        var e = await getRequest(process.env.PLENTY_URL + '/rest/tags/relationships');
        for (let index = 0; index < e.length; index++) {
            const element = e[index]['relationshipValue'];
            relationships.push(element);
        }
        console.log("done");

       /*  // 7. removing relations
        articles = removeVariationRelationships(articles, relationships);
        console.log("variations without relations " + articles.length); */

        // 8. generating sku array
        console.log("getSKUSfromvariations");
        //skus = getSkusFromVariations(variations);
        skus = await getSkusFromVariations(articles);

        // 9. get blocked warehouseids
        console.log("setting up warehouseitem array");
        warehouseIds = await getRequest(process.env.SAGE_URL + '/warehouseitems');
        warehouseIds = warehouseIds.GetWarehouseItemsResult.Data;
        for (let index = 0; index < warehouseIds.length; index++) {
            const element = warehouseIds[index];
            if (element.IsStockBlocked) {
                //console.log(element.WarehouseId);
                blockedWarehouses.push(element.WarehouseId);
            }
        }

        // 10. get stocks from sage an save in array 
        console.log("getting stocks from sage...");
       
        for (let index = 0; index < skus.length; index++) {
            var element = skus[index];
            console.log(element);
            if (index % 5 == 0) {
                console.log(index + '...');
            }
            var f = await getRequest(process.env.SAGE_URL + "/pimitem/Sku/" + element);
            f = f.GetPimItemBySkuResult.Data;
            if(f.ProductItem.Active) {
                if (f.ProductItem.IsBundle) {

                    var productItem = f.ProductItem;
                    var tempArray = [];
                    for (let index = 0; index < productItem.BundleItems.length; index++) {
                        var otherArray = [];
                        otherArray['amount'] = productItem.BundleItems[index].Amount;
                        otherArray['bundleSku'] = element;
                        otherArray['sku'] = productItem.BundleItems[index].Sku;
                        tempArray.push(otherArray);
                    }
                    bundles.push(tempArray);
                    //stocks[element] = "bundle " +element;
                }
                else {
                    if (f.StockItems.length > 0 && f.StockItems != null) {
                        var tempStock = 0;
                        var stockValue = 0;
                        var actualStock = 0;
    
                        for (var b = 0; b < f.StockItems.length; b++) {
                            if (!(blockedWarehouses.includes(f.StockItems[b].WarehouseId))) {
                                //stockValue = parseInt(f.StockItems[b].StockAmount);
                                tempStock = parseInt(f.StockItems[b].StockAmount);
                                stockValue += tempStock;
                            }
                        }
                        if (stockValue <= minStock) {
                            stockValue = 0;
                        }
    
    
                        var valuesToPush = [];
                        valuesToPush['sku'] = element;
                        valuesToPush['stock'] = stockValue;
                        valuesToPush['actualStock'] = actualStock;
                        stocks.push(valuesToPush);
                    }
                    else {
                        var valuesToPush = [];
                        valuesToPush['sku'] = element;
                        valuesToPush['stock'] = 0;
                        valuesToPush['actualStock'] = 0;
                        stocks.push(valuesToPush);
                    }
                }
            }
            element = "";
        } 
        console.log("done");
        console.log('sku array: ' + skus.length);
        console.log('stock array: ' + stocks.length);
        console.log('bundles array: ' + bundles.length);
        //const ans = await askQuestion("Continue with enter...");

        // 11. set up bundle stock array
        console.log("setting up bundle stocks");
        try {
            for (let index = 0; index < bundles.length; index++) {
                var bundleSku = bundles[index][0]['bundleSku'];
                var bundleStock = 0;
                for (let schmindex = 0; schmindex < bundles[index].length; schmindex++) {
                    var partSkuFound = false;
                    var partSku = bundles[index][schmindex]['sku'];
                    var partAmount = bundles[index][schmindex]['amount'];
                    var actualAmount = 0;
                    var tempStock = 0;
                    var partStock = 0;
                    for (let a = 0; a < stocks.length; a++) {
                        if (stocks[a]['sku'] == partSku) {
                            actualAmount = stocks[a]['stock'];
                            partSkuFound = true;
                        }
                    }
                    if (!partSkuFound) {
                        //console.log("getting stock for " + partSku);
                        var out = await getRequest(process.env.SAGE_URL + "/pimitem/Sku/" + partSku)
                        out = out.GetPimItemBySkuResult.Data.StockItems;
                        for (var b = 0; b < out.length; b++) {
                            if (!(blockedWarehouses.includes(out[b].WarehouseId))) {
                                //stockValue = parseInt(f.StockItems[b].StockAmount);
                                partStock = parseInt(out[b].StockAmount);
                                actualAmount += partStock;
                            }
                        }
                        //console.log("nothing found for " + partSku)
                    }
                    tempStock = Math.floor(actualAmount / partAmount);
                    if (schmindex > 0) {
                        if(tempStock < bundleStock) {
                            bundleStock = tempStock;
                        }
                    }
                    else {
                        bundleStock = tempStock;
                    }
                }
                var tempBundleArray = [];
                tempBundleArray['sku'] = bundleSku;
                tempBundleArray['stock'] = bundleStock;
                stocksBundles.push(tempBundleArray);
                var valuesToPush = [];
                valuesToPush['sku'] = bundleSku;
                valuesToPush['stock'] = bundleStock;
                valuesToPush['actualStock'] = 0;
                stocks.push(valuesToPush);
            }
        } catch (error) {
            console.log(error);
        }
        console.log("bundles: " + stocksBundles.length);
        console.log("stock now: " + stocks.length);
        var articlesArray;
        articlesArray = await setArticleArray(articles, stocks);
        console.log("done. Lenght = " + articles.length);

        //11.5
       // console.log("getting stocks from plentymarket");
        //await checkPlentyStock(articlesArray);


        // 12. update stock 
        console.log("");
        console.log("updating stock now...");
        fs.stat('test.txt', function(err, stat) {
            if (err == null) {
                fs.unlink('test.txt', (err) => {
                    if (err) {
                      console.error(err);
                    } else {
                      console.log('File is deleted.');
                    }
                });
            } else if (err.code === 'ENOENT') {
              console.log("test.txt does not exist");
            } else {
              console.log('Some other error: ', err.code);
            }
        });
        
        //"rest/items/"+articleStocks[x][0]+"/variations/"+articleStocks[x][1]+"/stock/correction"
        try {
            for (let index = 0; index < articlesArray.length; index++) {
                var stock = 0;
                var element = articlesArray[index];
                var plenty_stock = await getPlentyStock(element['itemId'], element['id']);
                stock = element['stock'];
                if (typeof stock == 'undefined') {  
                    stock = 0;
                }
                if(plenty_stock !== element['stock']) {
                    console.log("********************");
                    console.log("item_id = " + element['itemId']);
                    console.log("variation_id = " + element['id']);
                    console.log("plenty stock = " + plenty_stock);
                    console.log("sage stock = "+ element['stock']);
                    console.log("-----------------------");
                    var header = {
                        "Content-Type": "application/json; charset=utf8",
                        "Authorization": "Bearer " + token
                    };
                    var body = {
                        "warehouseId": element['mainWarehouseId'],
                        "quantity": stock,
                        "reasonId": 301,
                        "storageLocationId": 0
                    };
                    var options = {
                        url: process.env.PLENTY_URL + "/rest/items/" + element['itemId'] + "/variations/" + element['id'] + "/stock/correction",
                        method: "PUT",
                        headers: header,
                        json: true,
                        encoding: "utf-8",
                        body: body
                    }
                    var forTextfile = "*********  item_id = " + element['itemId'] + " variation_id = " + element['id']+" in plenty: "+plenty_stock +" sage stock = "+ element['stock'] + "-----------------------"+'\r\n';
                    fs.appendFile("test.txt", forTextfile, function(err) {
                        if(err) {
                            return console.log(err);
                        }
                        console.log("The file was saved!");
                    }); 
                    //await sendMessage("Stock für variation_id "+element['id']+" in plenty: "+plenty_stock+" in sage "+element['stock']);
                    //await sendMessagestock("Stock für variation_id "+element['id']+" in plenty: "+plenty_stock+" in sage "+element['stock']);
                    var out = await httpRequest(options, true, 'PUT');
                    console.log('--');
                    console.log("item_id: " + element['itemId'])
                    console.log("id: " + element['id']);
                    console.log("sku: " + element['sku']);
                    console.log("stock: " + stock);
                    console.log('**');
                    //await delay();
                }
                else {
                    console.log("same stock for " +element['id']);
                }
            }
        } catch (error) {
            console.log(error);
        }
       


        // 13. get prices from sage by sku
        /* console.log("getting prices");
        for (let blup = 0; blup < articlesArray.length; blup++) {
            let element = articlesArray[blup];
            var sku = element['sku'];
            console.log("sku " + sku);
            if (sku) {
                var options = {
                    //http://sage100.hansgmbh.local:8080/SageClient/Rest/PriceItems/7958-01?priceListId=1
                    url: process.env.SAGE_URL+"/PriceItems/"+sku+"?priceListId=1",
                    //url: process.env.SAGE_URL + "/PriceItems/" + sku,
                    json: true,
                    method: "GET",
                    encoding: "utf-8",
                };
                var out = await httpRequest(options, true, 'GET');
                console.log(out);
                if ("error" in out) {
                    var msg = "Error in prices from sage by sku"
                    throw new Error("Error in prices from sage by sku");
                }
                if(out.GetPriceItemsBySkuResult !== undefined) {
                    if(out.GetPriceItemsBySkuResult.Data !== undefined) {
                        var data = out.GetPriceItemsBySkuResult.Data
                        if (data.length) {
                            for (var i = 0; i < data.length; i++) {
                                if (data[i].ValidFromAmount == 0) {
                                    var objectTest = {
                                        price: data[i].Price,
                                        id: element['id'],
                                        itemId: element['itemId'],
                                        pricelistId: data[i].PriceListId
                                    }
                                    prices.push(objectTest);
                                }
                                else {
                                    //console.log("Valid From Amount not 0 ");
                                }
                            }
                        }
                    }
                }
            }
        }
        console.log("done");
        console.log("preis array: " + prices.length);

        var priceRange = prices.length / 50;
        var pricesBulk = [];

        for (var e = 0; e < priceRange; e++) {
            var temp = [];
            if (e < 0) {
                var ende = (e + 1) * 50
                var start = e;
            }
            else {
                var ende = (e + 1) * 50
                var start = e * 50;
            }
            for (var f = start; f < ende; f++) {
                var element = prices[f];
                if (element) {
                    //console.log(element);
                    if (element.hasOwnProperty('pricelistId')) {
                        if (element['pricelistId'] == 1) {
                            var singlePrice = {
                                "variationId": element['id'],
                                "salesPriceId": 1,
                                "price": element['price']
                            }
                            temp.push(singlePrice);
                        }
                    }
                }
            }
            pricesBulk.push(temp);
        }


        
        // 15. updating prices
        console.log("updating prices");
        for (var g = 0; g < pricesBulk.length; g++) {
            var element = pricesBulk[g];
            console.log("+++++++++++++++ " + g + "+++++++++++++++");
            salesPriceID = 1;
            let options = {
                url: process.env.PLENTY_URL + "/rest/items/variations/variation_sales_prices",
                json: true,
                method: "PUT",
                encoding: "utf-8",
                body: element,
                headers: {
                    'Content-Type': 'application/json; charset=utf8',
                    'Authorization': 'Bearer ' + token,
                }
            };
            var out = await httpRequest(options, true, 'PUT');
            console.log(out);
            await delay();
        }
        ende = getGermanTimestamp();
        await sendMessagestock("Synch vom " + startTime+" beendet!"); */
        console.log("done!");
    } catch (e) {
        console.error(e);
    } finally {
        //console.log('We do cleanup here');
    }
}



async function httpRequest(params, postData, type = 'GET') {
    try {
        return new Promise(function (resolve, reject) {
            if (type === 'POST') {
                request.post(params, (err, res, body) => {
                    if (err) {
                        var msg = "Error in POST httpRequest " + err;
                        //thechannel.sendToQueue(queue, Buffer.from(msg));
                        console.log(msg);
                        return false;
                    }
                    if (body) {
                        resolve(body);
                    }
                });
            }
            else if (type === 'PUT') {
                //console.log("in put");
                request.put(params, (err, res, body) => {
                    if (err) {
                        var msg = "Error in PUT httpRequest " + err;
                        //thechannel.sendToQueue(queue, Buffer.from(msg));
                        return console.log(err);
                    }
                    if (body) {
                        //console.log(res);
                        //console.log(body);
                        resolve(body);
                        return body;
                        /* console.log("BODY: ");
                        console.log(body); */
                    }
                    //console.log(err);
                    //console.log(res);
                    //console.log(body);
                    /* if(err) {
                        return reject(new Error('statusCode=' + res));
                    }
                    if(body) {
                        resolve(body);
                    } */
                });
            }
            else if (type === 'DELETE') {
                //console.log("in put");
                request.delete(params, (err, res, body) => {
                    if (err) {
                        var msg = "Error in DELETE httpRequest " + err;
                        //thechannel.sendToQueue(queue, Buffer.from(msg));
                        return console.log(err);
                    }
                    if (body) {
                        //console.log(res);
                        //console.log(body);
                        resolve(body);
                        return body;
                        /* console.log("BODY: ");
                        console.log(body); */
                    }
                    //console.log(err);
                    //console.log(res);
                    //console.log(body);
                    /* if(err) {
                        return reject(new Error('statusCode=' + res));
                    }
                    if(body) {
                        resolve(body);
                    } */
                });
            }
            else if (type === 'GET') {
                request.get(params, (err, res, body) => {
                    if (err) {
                        var msg = "Error in GET httpRequest " + err;
                        //thechannel.sendToQueue(queue, Buffer.from(msg));
                        console.log(err);
                        return false;
                        //return reject(new Error(err + ' statusCode=' + res));
                    }
                    if (body) {
                        resolve(body);
                    }
                });
            }
        });
    }
    catch (e) {
        var msg = "Error in httpRequest " + e;
        thechannel.sendToQueue(queue, Buffer.from(msg));
        console.log(e);
    } finally {
        //console.log('We do cleanup here');
    }
}

function getStocksBySku(stocks, sku) {
    for (let index = 0; index < stocks.length; index++) {
        const element = stocks[index];
        if (element['sku'] == sku) {
            return element['stock'];
        }
    }
}

async function getPlentyStock(item_id, variation_id) {
    var header = {
        'Content-Type': 'application/json; charset=utf8',
        'Authorization': 'Bearer ' + token
    }
    var options = {
        url: process.env.PLENTY_URL + "/rest/items/"+item_id+"/variations/"+variation_id+"/stock",
        port: 443,
        method: 'GET',
        headers: header
    } 
    var out = await httpRequest(options, true, 'GET');
    //console.log(out);
    var plenty_stock = 0;
    if(isJsonString(out)) {
        out = JSON.parse(out);
        for (let index = 0; index < out.length; index++) {
            const element = out[index];
            plenty_stock += element.physicalStock;  
        }
    }
    await delay();
    return plenty_stock;
}

async function checkPlentyStock(articleArray) {
    var header = {
        'Content-Type': 'application/json; charset=utf8',
        'Authorization': 'Bearer ' + token
    }
    for (let index = 0; index < articleArray.length; index++) {
        const element = articleArray[index];
        var options = {
            url: process.env.PLENTY_URL + "/rest/items/"+element['itemId']+"/variations/"+element['id']+"/stock",
            port: 443,
            method: 'GET',
            headers: header
        } 
        var out = httpRequest(options, true, 'GET');
        var plenty_stock = 0;
        if(isJsonString(out)) {
            out = JSON.parse(out);
            for (let index = 0; index < out.length; index++) {
                const element = out[index];
                plenty_stock += element.physicalStock;  
            }
        }
        console.log("********************");
        console.log("item_id = " + element['itemId']);
        console.log("variation_id = " + element['id']);
        console.log("plenty stock = " + plenty_stock);
        console.log("sage stock = "+ element['stock']);
        console.log("-----------------------");
        if(plenty_stock === element['stock']) {
            element['skip'] = true;
            console.log("setting skip to true");
        }
        await delay();
    }
}

function isJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

async function setArticleArray(articles, stocks) {
    articleStocks = [];
    console.log("set Article Array");
    var size = articles.length;
    var item_id = 0;
    var slack_payload = '';
    for (var i = 0; i < size; i++) {
        //console.log(i);
        var arrayToPush = [];
        if (articles[i].externalId) {
            if(articles[i].externalId.length > 0) {
            var sku = articles[i].externalId;
            arrayToPush['sku'] = sku;
           // console.log("sku: " + sku);
            arrayToPush['itemId'] = articles[i].itemId;
            arrayToPush['id'] = articles[i].id;
            arrayToPush['mainVariationId'] = articles[i].mainVariationId;
            arrayToPush['mainWarehouseId'] = articles[i].mainWarehouseId;
            arrayToPush['skip'] = false;
            var stock = getStocksBySku(stocks, sku);
            if (typeof stock == 'undefined') {
                stock = 0;
            }
            arrayToPush['stock'] = stock;
            //console.log("stock: " + arrayToPush['stock']);
            articleStocks.push(arrayToPush);
            }
        }
        else {
            if(item_id === articles[i].itemId) {
                slack_payload += " und varianten_id: "+articles[i].id + "\n";
            }
            else {
                if(i > 0) {
                    //await sendMessage(slack_payload);
                }
                item_id = articles[i].itemId;
                slack_payload = " ------ ARTIKEL ID ------ " + articles[i].itemId + " keine sku gefunden varianten_id: "+articles[i].id + '\n';
            }
            var sku = articles[i].externalId;
            arrayToPush['sku'] = sku;
            arrayToPush['itemId'] = articles[i].itemId;
            arrayToPush['id'] = articles[i].id;
            arrayToPush['mainVariationId'] = articles[i].mainVariationId;
            arrayToPush['skip'] = false;
            arrayToPush['mainWarehouseId'] = articles[i].mainWarehouseId;
            /* console.log('----------------');
            console.log("keine sku: " + sku);
            console.log("item id: " + articles[i].itemId);
            console.log('id: ' + articles[i].id)
            console.log('**********'); */
            arrayToPush['stock'] = 0;
            articleStocks.push(arrayToPush);
        }
    }
    return articleStocks;
}

synchronize();